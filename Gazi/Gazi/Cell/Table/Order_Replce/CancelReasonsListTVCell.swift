//
//  CancelReasonsListTVCell.swift
//  Gazi
//
//  Created by Apple on 30/09/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class CancelReasonsListTVCell: UITableViewCell {

    @IBOutlet var lblReason: UILabel!
    @IBOutlet var btnRound: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }

}
