//
//  OrderedDetailListTVCell.swift
//  Gazi
//
//  Created by Apple on 10/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class OrderedDetailListTVCell: UITableViewCell {

    @IBOutlet weak var lblHead: UILabel!
    @IBOutlet weak var lblValue: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
