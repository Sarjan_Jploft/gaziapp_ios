//
//  ReplacementTVCell.swift
//  Gazi
//
//  Created by Apple on 30/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class ReplacementTVCell: UITableViewCell {
    
    @IBOutlet weak var lblSelectType: UILabel!
    @IBOutlet weak var btnSelectType: UIButton!
    
    
    @IBOutlet weak var lblSelectQty: UILabel!
    @IBOutlet weak var btnSelectQty: UIButton!
    
    @IBOutlet weak var btnAddMore: UIButton!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        btnAddMore.layer.cornerRadius = 4
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    
}
