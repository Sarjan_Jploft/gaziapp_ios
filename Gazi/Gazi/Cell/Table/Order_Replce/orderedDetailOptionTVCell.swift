//
//  orderedDetailOptionTVCell.swift
//  Gazi
//
//  Created by Apple on 10/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Cosmos

class orderedDetailOptionTVCell: UITableViewCell {

    
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnRateThisOrder: UIButton!
    @IBOutlet weak var btnTrackOrder: UIButton!
    
    @IBOutlet weak var viewCancel: UIView!
    @IBOutlet weak var viewRate: CosmosView!
    @IBOutlet weak var viewRateContnr: UIView!
    @IBOutlet weak var viewTrackOrder: UIView!
    
    @IBOutlet var lblTrackOrder: UILabel!
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        btnCancel.layer.cornerRadius = 3
        
        btnCancel.setTitle(Localization("Cancel"), for: .normal)
        btnRateThisOrder.setTitle(Localization("Rate this order"), for: .normal)
        lblTrackOrder.text = Localization("Track Order")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
