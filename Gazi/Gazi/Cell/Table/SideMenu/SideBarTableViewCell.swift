//
//  SideBarTableViewCell.swift
//  Infinite Cabs
//
//  Created by micro on 17/10/19.
//  Copyright © 2019 micro. All rights reserved.
//

import UIKit

class SideBarTableViewCell: UITableViewCell {
    
    // Outlets Head
    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblContact: UILabel!
    @IBOutlet weak var btnClose: UIButton!
    
    
    // Outlets cell
    
    @IBOutlet weak var lblOption: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
