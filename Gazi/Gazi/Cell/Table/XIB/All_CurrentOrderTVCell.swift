//
//  All_CurrentOrderTVCell.swift
//  Gazi
//
//  Created by Apple on 25/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class All_CurrentOrderTVCell: UITableViewCell {

    
    @IBOutlet weak var viewContainer: CardView!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnRateOrder: UIButton!
    @IBOutlet weak var btnTrackOrder: UIButton!
    @IBOutlet weak var btnOpenOrder: UIButton!
    @IBOutlet weak var btnInvoice: UIButton!
    
    @IBOutlet weak var lblOrderId: UILabel!
    @IBOutlet weak var lblOrderNumber: UILabel!
    
    
    @IBOutlet var lblOrderIdHead: UILabel!
    @IBOutlet var lblOrderNumberHead: UILabel!
    @IBOutlet var lblOpenOrderHead: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewContainer.cornerRadius = 8
        
        
        
        lblOrderIdHead.text = Localization("Order ID :")
        lblOrderNumberHead.text = Localization("Order Number :")
        lblOpenOrderHead.text = Localization("Open Order")
        
        btnCancel.setTitle(Localization("Cancel"), for: .normal)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
