//
//  All_canceledTVCell.swift
//  Gazi
//
//  Created by Apple on 25/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class All_canceledTVCell: UITableViewCell {

    @IBOutlet weak var lblOrderId: UILabel!
    @IBOutlet weak var lblOrderNumber: UILabel!
    @IBOutlet weak var btnOpenOrder: UIButton!
    @IBOutlet weak var btnInvoice: UIButton!
    @IBOutlet weak var viewContainer: CardView!
    
    
    
    @IBOutlet var lblOpenOrderHead: UILabel!
    @IBOutlet var lblOrderIdHead: UILabel!
    @IBOutlet var lblOrderNumberHead: UILabel!
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewContainer.cornerRadius = 8
        
        lblOrderIdHead.text = Localization("Order ID :")
        lblOrderNumberHead.text = Localization("Order Number :")
        lblOpenOrderHead.text = Localization("Open Order")
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
