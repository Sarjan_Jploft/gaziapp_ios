//
//  CanceledOrderedTVCell.swift
//  Gazi
//
//  Created by Apple on 18/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class CanceledOrderedTVCell: UITableViewCell {

    @IBOutlet weak var lblOrderId: UILabel!
    @IBOutlet weak var lblOrderNumber: UILabel!
    @IBOutlet weak var btnOpenOrder: UIButton!
    @IBOutlet weak var btnInvoice: UIButton!
    @IBOutlet weak var viewContainer: CardView!
    
    
    @IBOutlet var lblHeadOrderId: UILabel!
    @IBOutlet var lblHeadOrderNumber: UILabel!
    @IBOutlet var lblHeadOpenOrder: UILabel!
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewContainer.cornerRadius = 8
        
        lblHeadOrderId.text = Localization("Order ID :")
        lblHeadOrderNumber.text = Localization("Order Number :")
        lblHeadOpenOrder.text = Localization("Open Order")
               
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
