//
//  CurrentOrderListTVCell.swift
//  Gazi
//
//  Created by Apple on 11/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class CurrentOrderListTVCell: UITableViewCell {

    @IBOutlet weak var viewContainer: CardView!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnRateOrder: UIButton!
    @IBOutlet weak var btnTrackOrder: UIButton!
    @IBOutlet weak var btnOpenOrder: UIButton!
    
    
    @IBOutlet weak var lblOrderId: UILabel!
    @IBOutlet weak var lblOrderNumber: UILabel!
    
    @IBOutlet var lblOrderIdHead: UILabel!
    @IBOutlet weak var lblOrderNumberHead: UILabel!
    @IBOutlet var lblOpenOrderHead: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
        
        viewContainer.cornerRadius = 8
        btnCancel.layer.cornerRadius = 3
        
        lblOrderIdHead.text = Localization("Order ID :")
        lblOrderNumberHead.text = Localization("Order Number :")
        lblOpenOrderHead.text = Localization("Open Order")
        
        btnCancel.setTitle(Localization("Cancel"), for: .normal)
        
        layoutIfNeeded()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
