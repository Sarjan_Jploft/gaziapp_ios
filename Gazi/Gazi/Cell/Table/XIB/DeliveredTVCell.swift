//
//  DeliveredTVCell.swift
//  Gazi
//
//  Created by Apple on 18/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Cosmos

class DeliveredTVCell: UITableViewCell {

    @IBOutlet weak var lblOrderId: UILabel!
    @IBOutlet weak var lblOrderNumber: UILabel!
    @IBOutlet weak var btnOpenOrder: UIButton!
    @IBOutlet weak var btnInvoice: UIButton!
    @IBOutlet weak var btnRateOrder: UIButton!
    @IBOutlet weak var viewContainer: CardView!
    @IBOutlet weak var viewRating: CosmosView!
    
    
    @IBOutlet var lblOrderIdHead: UILabel!
    @IBOutlet var lblOrderNumberHead: UILabel!
    @IBOutlet var lblOpenOrderHead: UILabel!
    @IBOutlet var lblInvoiceHead: UILabel!
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewContainer.cornerRadius = 8
        
        lblOrderIdHead.text = Localization("Order ID :")
        lblOrderNumberHead.text = Localization("Order Number :")
        lblOpenOrderHead.text = Localization("Open Order")
        lblInvoiceHead.text = Localization("Invoice")
        
        btnRateOrder.setTitle(Localization("Rate this Order"), for: .normal)
               
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
