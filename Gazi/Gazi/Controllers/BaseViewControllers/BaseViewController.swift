//
//  BaseViewController.swift
//  Infinite Cabs
//
//  Created by Apple on 17/11/19.
//  Copyright © 2019 micro. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController,SlideMenuDelegate  {

    let lang = UserDefaults.standard.string(forKey: "Applanguage") ?? "en"
    
    override func viewDidLoad() {
        super.viewDidLoad()

       // navigationController?.navigationBar.barStyle = .black
        // Do any additional setup after loading the view.
        
        //   UIApplication.shared.statusBarUIView?.backgroundColor = UIColor(named: "ThemeColor")
    }
    
        
        func slideMenuItemSelected(at index: Int, withIdentifire identifire: [String: Any]) {
            
            if index > -1 && identifire.count > 0 {
                
                openViewControllerBased(onIdentifier: identifire["identifire"] as? String)
                UserDefaults.standard.set(index, forKey: "index")
            }
        }

       
        func openViewControllerBased(onIdentifier strIdentifier: String?) {
            
            var strStoryBoard: UIStoryboard?
            
            if strIdentifier == "HomeVC"{
                 strStoryBoard = staticClass.getStoryboard_Home()
            }else if strIdentifier == "EditProfileVC"{
                strStoryBoard = staticClass.getStoryboard_Profile()
            }else if strIdentifier == "TrackOrderVC" || strIdentifier == "OrderHistoryVC"{
                strStoryBoard = staticClass.getStoryboard_Order()
            }else{
                return
            }
            
//            else if strIdentifier == "MyRideViewController"{
////                strStoryBoard = staticClass.getStoryboard_MyRide()
//            }else if strIdentifier == "SettingsVC" || strIdentifier == "LegalTVController" || strIdentifier == "ContactVC" || strIdentifier == "CouponCodeViewController"{
////                strStoryBoard = staticClass.getStoryboard_Settings()
//            }
            
            if let aBoard = strStoryBoard {
                print("strStoryBoard is:-\(aBoard)")
            }
            if let anIdentifier = strStoryBoard?.instantiateViewController(withIdentifier: strIdentifier ?? "") {
                print("strStoryBoard is:-\(anIdentifier)")
            }
            let destViewController: UIViewController? = strStoryBoard?.instantiateViewController(withIdentifier: strIdentifier ?? "")
            if let aController = destViewController {
                
                navigationController?.pushViewController(aController, animated: true)
            }
        }

       
    
    
    func addSlideMenuButton(_view:UIView) {
            
            let btnShowMenu = UIButton(type: .system)
           
        btnShowMenu.frame = CGRect(x: 0 , y: 0, width: 45, height: 45)
            
            btnShowMenu.addTarget(self, action:#selector(onSlideMenuButtonPressed(_:)), for: .touchUpInside)
             _view.addSubview(btnShowMenu)
        
        
//            let customBarItem = UIBarButtonItem(customView: btnShowMenu)
//            navigationItem.leftBarButtonItem = customBarItem
        }

        
       
        @objc func onSlideMenuButtonPressed(_ sender: UIButton)
        {
            self.view.endEditing(true)

            if sender.tag == 10 {
                // To Hide Menu If it already there
                slideMenuItemSelected(at: -1, withIdentifire: [:])
                
                sender.tag = 0
                
                let viewMenuBack = view.subviews.last //as! UIView
                
                UIView.animate(withDuration: 0.3, animations: {() -> Void in
                    var frameMenu: CGRect? = viewMenuBack?.frame
                    frameMenu?.origin.x = -1 * UIScreen.main.bounds.size.width
                    viewMenuBack?.frame = frameMenu ?? CGRect.zero
                    viewMenuBack?.layoutIfNeeded()
                    
                    // change for color
                    viewMenuBack?.backgroundColor = UIColor.clear
                }, completion: {(_ finished: Bool) -> Void in
                    viewMenuBack?.removeFromSuperview()
                })
                return
            }
            
        
            
            sender.isEnabled = true
            sender.tag = 10
            
            let menuVC = UIStoryboard(name: "SideMenu", bundle: nil).instantiateViewController(withIdentifier: "SideViewController") as? SideViewController
            
            print(menuVC ?? "")
            
            menuVC?.btnMenu = sender
            menuVC?.delegate = self
            
            if let aView = menuVC?.view {
                self.view.addSubview(aView)
            }
            
            if let aVC = menuVC {
                addChild(aVC)
            }
            
            menuVC?.view.layoutIfNeeded()
            let xValue = lang == "ar" ? (UIScreen.main.bounds.size.width) : (-UIScreen.main.bounds.size.width)
            menuVC?.view.frame = CGRect(x: xValue, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
            UIView.animate(withDuration: 0.3, animations: {() -> Void in
                
                menuVC?.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
                
                sender.isEnabled = true
                
            }, completion: {(_ finished: Bool) -> Void in
        })
    }
       
}
