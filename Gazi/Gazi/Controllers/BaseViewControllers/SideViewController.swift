//
//  SideViewController.swift
//  Infinite Cabs
//
//  Created by micro on 17/10/19.
//  Copyright © 2019 micro. All rights reserved.
//

import UIKit
import SwiftyJSON
import SDWebImage


protocol SlideMenuDelegate {
    func slideMenuItemSelected(at index: Int, withIdentifire identifire: [String: Any])
}


class SideViewController: UIViewController {

    @IBOutlet weak var mytable: UITableView!
    @IBOutlet weak var viewSideMenu: UIView!
    
    
    
//    var arrOptions = ["Home","My Trips","My Cards" ,"Coupon Code","Settings", "Privacy Policy", "Terms & Conditions", "Contact", "Logout"]
    
    var btnCloseMenuOverlay: UIButton!
    
    var btnMenu: UIButton!
    var arrayMenuOptions = [[String:String]]()
    var strLogout = Localization("Logout")
     var delegate: SlideMenuDelegate!
    
    
    let lang = UserDefaults.standard.string(forKey: "Applanguage") ?? "en"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        set_sideviewProfile()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(func_HideSideMenu))
        viewSideMenu.addGestureRecognizer(tap)
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture(gesture:)))
            
        swipeLeft.direction = UISwipeGestureRecognizer.Direction.left
        self.view.addGestureRecognizer(swipeLeft)
        
        // Do any additional setup after loading the view.
    }
    
   @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {

        if let swipeGesture = gesture as? UISwipeGestureRecognizer {


            switch swipeGesture.direction {
            case UISwipeGestureRecognizer.Direction.right:
                    print("Swiped right")
                

            case UISwipeGestureRecognizer.Direction.down:
                    print("Swiped down")
            case UISwipeGestureRecognizer.Direction.left:
                    print("Swiped left")
                // close the side menu
                btnCloseMenuOverlay = UIButton(type: .custom)
                onCloseMenuClick(btnCloseMenuOverlay)
            case UISwipeGestureRecognizer.Direction.up:
                    print("Swiped up")
                default:
                    break
            }
        }
    }
    
    @objc func func_HideSideMenu() {
            btnCloseMenuOverlay = UIButton(type: .custom)
            onCloseMenuClick(btnCloseMenuOverlay)
        }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
           strLogout = Localization("Logout")
           setSideMenuData()
       }
    
    
    
    func setSideMenuData()
    {
        arrayMenuOptions =
            [
                ["title":Localization("Home"), "identifire": "HomeVC", "image": "SideMenu_home-second"],
                ["title":Localization("My Order"), "identifire": "OrderHistoryVC", "image": "SideMenu_MyDetail"],
                ["title":Localization("Cancel Order"), "identifire": "OrderHistoryVC", "image": "SideMenu_MyAvailability"],
                ["title":Localization("Invoice"), "identifire": "OrderHistoryVC", "image": "SideMenu_MyAvailability"],
                ["title": Localization("Review"), "identifire": "SettingsVC", "image": "SideMenu_MyPatient"],
//                ["title": "Privacy Policy", "identifire": "LegalVC", "image": "SideMenu_Transaction"],
//                ["title": "Terms & Conditions", "identifire": "LegalVC", "image": "SideMenu_Transaction"],
                ["title": Localization("Track Order"), "identifire": "OrderHistoryVC", "image": "SideMenu_Transaction"],
                ["title": Localization("Profile"), "identifire": "EditProfileVC", "image": "SideMenu_Transaction"],
                ["title": Localization("Logout"), "identifire": "PaymentMethodsVC", "image": "SideMenu_Transaction"]
        ]
        mytable.reloadData()
    }
    
    
    
    @IBAction func onCloseMenuClick(_ button: UIButton?)
      {
        
          print(button?.tag ?? "")
          
          btnMenu.tag = 0
          
        if(button?.tag == 0){
            delegate = nil
        }
        
          if delegate != nil {
              
              var index: Int = (button?.tag)!
              
              if button == btnCloseMenuOverlay {
                  index = -1
              }
              if index < 0 {
                  delegate.slideMenuItemSelected(at: index, withIdentifire: [:])
              } else {
                  delegate.slideMenuItemSelected(at: index, withIdentifire: arrayMenuOptions[index])
              }
          }
          
        let xValue = lang == "ar" ? (UIScreen.main.bounds.size.width) : (-UIScreen.main.bounds.size.width)
        
        
          UIView.animate(withDuration: 0.3, animations: {() -> Void in
              self.view.frame = CGRect(x: xValue, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
              self.view.layoutIfNeeded()
              self.view.backgroundColor = UIColor.clear
          }, completion: {(_ finished: Bool) -> Void in
              self.view.removeFromSuperview()
            self.removeFromParent()
          })
      }

      
    
    // function for side controller
    
    func set_sideviewProfile() {
        
        
    }
    
   
    
    
    
    
    
}


//MARK:- TableView Delegate and DataSource

extension SideViewController:UITableViewDelegate,UITableViewDataSource{
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayMenuOptions.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Head") as! SideBarTableViewCell
        guard let jsondata = global.shared.getJSON(Constants.UserDefult.SaveUserData.rawValue)else{return cell.contentView}
        let fullName = jsondata["fname"].stringValue + " " + jsondata["lname"].stringValue
        let email = jsondata["email"].stringValue
        let img = jsondata["image"].stringValue
        cell.lblName.text = fullName
        cell.lblContact.text = email
        cell.imgProfile.layer.cornerRadius = cell.imgProfile.frame.height/2
        
//        let img =  global.userProfileImage
        
        if let imageURL = URL(string: img) {
            cell.imgProfile.sd_setImage(with: imageURL, placeholderImage: UIImage(named: "userB"), options: SDWebImageOptions.progressiveLoad) { (image, error, sdimagecacheType, url) in

            }
        }
        
        return cell.contentView
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
//        let tblHeight = self.mytable.frame.height
//
//        return ((tblHeight - 120) / CGFloat(arrayMenuOptions.count))
        return 44
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! SideBarTableViewCell
        
        let dic = arrayMenuOptions[indexPath.row]
        cell.lblOption.text = dic["title"]
        
        return cell
        
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

       let dic = arrayMenuOptions[indexPath.row]
        print(dic)
//        if indexPath.row == 5 || indexPath.row == 6{
//
//            let strStoryBoard = staticClass.getStoryboard_Settings()
//            let vc = strStoryBoard?.instantiateViewController(withIdentifier: "LegalVC") as! LegalVC
//
//            if indexPath.row == 5{
//                vc.isPrivacy = true
//            }else{
//                vc.isTermsCond = true
//            }
//
//            self.navigationController?.pushViewController(vc, animated: true)
            

       //  }else
//        else if indexPath.row == 1{
//
//         let strbd = UIStoryboard(name: "MyRide", bundle: nil)
//         let vc = strbd.instantiateViewController(withIdentifier: "MyRideViewController") as! MyRideViewController
//         self.navigationController?.pushViewController(vc, animated: true)
//
//         }


        if (indexPath.row == 7)
        {
            DispatchQueue.main.async(execute: {() -> Void in
                let alertController1 = UIAlertController(title:Localization("Confirm") , message: Localization("Do you want to logout?"), preferredStyle: .alert)
                alertController1.addAction(UIAlertAction(title:Localization("Yes") , style: .default, handler: {(_ action1: UIAlertAction?) -> Void in

                    global.shared.removeDataFromUserDefult()

                    // Need facebook logout

                    let strbdMain = staticClass.getStoryboard_Main()
                    let initialVC = strbdMain?.instantiateViewController(withIdentifier: "LogInVC") as! LogInVC

                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    let nav = UINavigationController(rootViewController: initialVC)
                    appDelegate.window!.rootViewController = nav



                }))
                alertController1.addAction(UIAlertAction(title: Localization("No"), style: .default, handler: nil))
                self.present(alertController1, animated: true) {() -> Void in }
            })
        }else if (indexPath.row == 4){
            
           
            let url = URL(string: "itms-apps://itunes.apple.com/app/")!
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
                        
            
            
        }else
        {
            let btn = UIButton(type: .custom)
            btn.tag = indexPath.row
            onCloseMenuClick(btn)
        }
    }
    
    
}

