//
//  HomeVC.swift
//  Gazi
//
//  Created by Apple on 10/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import CoreLocation
import GoogleMaps

class HomeVC: BaseViewController {
    
    @IBOutlet weak var imgHome1: UIImageView!
    @IBOutlet weak var googleMap: GMSMapView!
    @IBOutlet weak var btnNewCylinder: UIButton!
    @IBOutlet weak var btnReplace: UIButton!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var viewHead: UIView!
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet var lblHome: UILabel!
    @IBOutlet var lblNewCylinder: UILabel!
    @IBOutlet var lblReplaceCylinder: UILabel!
    
    
    
    let locationManager = CLLocationManager()
    
    var jsonData = JSON()
    
    let strLat = UserDefaults.standard.string(forKey: "lat") ?? ""
    let strLng = UserDefaults.standard.string(forKey: "lng") ?? ""

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUp_UI()
        checkLocationPermission()
        
        
        let camera = GMSCameraPosition.camera(withLatitude: (Double(strLat) ?? 0.0), longitude: (Double(strLng) ?? 0.0), zoom: 15.0)
        googleMap.camera = camera
        
        //        let marker = GMSMarker()
        //        marker.position = CLLocationCoordinate2D(latitude: -33.86, longitude: 151.20)
        //        marker.title = "Sydney"
        //        marker.snippet = "Australia"
        //        marker.map = googleMap
        
        
        
        API_GetGasAgencies()
        
        
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    
    
    
    func setUp_UI(){
        
        changeStatusBarColor()
        addGreenGradientsAndBorder(to: btnSubmit)
        viewHead.applyGradient(colours: [Constants.headViewTopGradient, Constants.headViewBottomGradient])
        
        lblHome.text = Localization("HOME")
        lblNewCylinder.text = Localization("NEW CYLINDER")
        lblReplaceCylinder.text = Localization("Replace")
        
        btnSubmit.setTitle(Localization("Submit"), for: .normal)
        
    }
    
    func setMarker(){
        
        
        for data in jsonData{
            
            let newJson = data.1
            
            let marker = GMSMarker()
            
            // I have taken a pin image which is a custom image
            let markerImage = UIImage(named: "MarkerCarCylinder")//!.withRenderingMode(.alwaysTemplate)
            
            //creating a marker view
            let markerView = UIImageView(image: markerImage)
            
            marker.position = CLLocationCoordinate2D(latitude: CLLocationDegrees(exactly: newJson["lat"].doubleValue) ?? 0.0, longitude: CLLocationDegrees(exactly: newJson["lng"].doubleValue) ?? 0.0)
            
            marker.iconView = markerView
            marker.title = newJson["name"].stringValue
//            marker.snippet = "Gas Agency"
            marker.map = googleMap
            
        }
        
    }
    
    func checkLocationPermission(){
        
        let locStatus = CLLocationManager.authorizationStatus()
        switch locStatus {
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
            return
        case .denied, .restricted:
            showPermissionAlert()
            return
        case .authorizedAlways, .authorizedWhenInUse:
            break
        @unknown default:
            print("error")
        }
        
    }
    
    //MARK:- ACTION
    
    @IBAction func btnNewCylinderClicked(_ sender: UIButton) {
        
        btnNewCylinder.isSelected = !btnNewCylinder.isSelected
        
    }
    
    
    @IBAction func btnReplaceClicked(_ sender: UIButton) {
        
        btnReplace.isSelected = !btnReplace.isSelected
    }
    
    
    @IBAction func btnSubmitClicked(_ sender: UIButton) {
        
        if btnNewCylinder.isSelected || btnReplace.isSelected{
          
            let strbd = staticClass.getStoryboard_Order()
            let vc = strbd?.instantiateViewController(withIdentifier: "ReplacementVC") as! ReplacementVC
            vc.isReplaceNewBothSelected = btnNewCylinder.isSelected && btnReplace.isSelected
            vc.isReplaceOnly = btnReplace.isSelected && (!btnNewCylinder.isSelected)
            vc.isNewOnly = btnNewCylinder.isSelected && (!btnReplace.isSelected)
            self.navigationController?.pushViewController(vc, animated: true)
            
            
//            let vc = strbd?.instantiateViewController(withIdentifier: "ReplacementNewVC") as! ReplacementNewVC
//            self.navigationController?.pushViewController(vc, animated: true)
            
        }else{
            
            HelpingMethod.shared.presentAlertWithTitle(title: Localization("Error"), message: Localization("Please select booking type"), vc: self)
        }
        
    }
    
    @IBAction func btnSideMenuClicked(_ sender: UIButton) {
        onSlideMenuButtonPressed(sender)
    }
    
    
    //MARK:- API Call
    
    private func API_GetGasAgencies(){
        
//        let latt =  Double(locationManager.location?.coordinate.latitude ?? 0.0)
//        let longg =  Double(locationManager.location?.coordinate.longitude ?? 0.0)
        
        let strLat = UserDefaults.standard.string(forKey: "lat") ?? ""
        let strLng = UserDefaults.standard.string(forKey: "lng") ?? ""
        
        Hud.shared.showHudWithMsg()
        
        let userid = global.shared.getUserDataValueByKey(strKey: ApiKey.userId)
        
        let dictKeys = [
            
            "user_id" : userid,
            "lat":(strLat == "" ? 0.0 : strLat),
            "lng":(strLng == "" ? 0.0 : strLng)
            
            ] as [String : Any]
        
        let url = Constants.AppLinks.API_BASE_URL + "\(Constants.ApiUrl.GasAgency.rawValue)"
        
        
        Alamofire.request(url , method: .post, parameters: dictKeys, headers:nil).responseJSON(queue: .global(qos: .background))
        { (response) in
            print(response)
            Hud.shared.hideHud()
            
            do {
                let json: JSON = try JSON(data: response.data!)
                
                if (json["statusCode"].stringValue == "200"){
                    
                    self.jsonData = json["data"]
                    DispatchQueue.main.async {
                        self.setMarker()
                    }
                    
                }
                else{
                    // error
                    Hud.shared.show_ErrorHud(json["message"].stringValue)
                }
                
            } catch {
                // show a alert
                Hud.shared.show_ErrorHud(Localization("Please check network connection"))
            }
        }
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
