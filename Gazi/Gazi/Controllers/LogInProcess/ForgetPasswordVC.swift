//
//  ForgetPasswordVC.swift
//  Gazi
//
//  Created by Apple on 09/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ForgetPasswordVC: UIViewController {
    
    
    @IBOutlet weak var viewHead: UIView!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    @IBOutlet weak var txtOtp1: UITextField!
    @IBOutlet weak var txtOtp2: UITextField!
    @IBOutlet weak var txtOtp3: UITextField!
    @IBOutlet weak var txtOtp4: UITextField!
    @IBOutlet weak var txtOtp5: UITextField!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var ViewstackTxt: UIView!
    @IBOutlet var lblForgotPassword: UILabel!
    @IBOutlet var lblOtpDescription: UILabel!
    
    
    
    
    var userid = ""
    var strOtp = ""
    
    let lang = UserDefaults.standard.string(forKey: "Applanguage") ?? "en"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUp_UI()
        selfDelegate()
        popupTakingEmailOrPhone()
        
        // Do any additional setup after loading the view.
    }
    
    
    func setUp_UI(){
        
        changeStatusBarColor()
        addPaddingAndBorder(to: txtPassword)
        addPaddingAndBorder(to: txtConfirmPassword)
        addGreenGradientsAndBorder(to: btnSubmit)
        viewHead.applyGradient(colours: [Constants.headViewTopGradient, Constants.headViewBottomGradient])
        ViewstackTxt.layer.borderWidth = 1
        ViewstackTxt.layer.borderColor = UIColor(red: 212/255, green: 212/255, blue: 212/255, alpha: 1).cgColor
        ViewstackTxt.layer.cornerRadius = 4
        ViewstackTxt.clipsToBounds = true
        
        
        txtPassword.placeholder = Localization("Password")
        txtConfirmPassword.placeholder = Localization("Confirm Password")
        
        lblForgotPassword.text = Localization("FORGOT PASSWORD")
        lblOtpDescription.text = Localization("One Time Password (OTP) has been sent to your Mobile number")
        
        btnSubmit.setTitle(Localization("Submit"), for: .normal)
        
        if lang == "ar"{
            txtPassword.textAlignment = .right
            txtConfirmPassword.textAlignment = .right
            
        }else{
            txtPassword.textAlignment = .left
            txtConfirmPassword.textAlignment = .left
            
        }
        
    }
    
    func selfDelegate(){
        
        txtPassword.delegate = self
        txtConfirmPassword.delegate = self
        txtOtp1.delegate = self
        txtOtp2.delegate = self
        txtOtp3.delegate = self
        txtOtp4.delegate = self
        txtOtp5.delegate = self
    }
    
    func popupTakingEmailOrPhone(){
        
        let ac = UIAlertController(title: Localization("Enter your registered email id"), message: nil, preferredStyle: .alert)
        ac.addTextField { (emailTextfield) in
            emailTextfield.placeholder = Localization("Enter email id")
            emailTextfield.keyboardType = .emailAddress
            emailTextfield.textContentType = .emailAddress
        }
        
        let submitAction = UIAlertAction(title: Localization("Submit"), style: .default) { [unowned ac] _ in
            let answer = ac.textFields![0]
            
            let email = answer.text?.trimmingCharacters(in: .whitespacesAndNewlines)
            if email!.isEmpty || (HelpingMethod.shared.VaildateTxtEmail(strEmail: answer.text!) ==  false) {
                
                let alert = UIAlertController(title: Localization("Error"), message: Localization("Please enter a valid email id"), preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: Localization("ok"), style: .default, handler: { (action) in
                    self.popupTakingEmailOrPhone()
                }))
                self.present(alert, animated: true, completion: nil)
                
                
            }else{
                self.API_ForgotPassword(email: answer.text!)
            }
            
            // do something interesting with "answer" here
        }
        
        ac.addAction(submitAction)
        
        present(ac, animated: true)
        
    }
    
    func setUpTextFieldMoveNext()
    {
        
        txtOtp1.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        txtOtp2.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        txtOtp3.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        txtOtp4.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        txtOtp5.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        
    }
    
    
    @objc func textFieldDidChange(textField: UITextField)
    {
        let text = textField.text
        
        if (text?.utf16.count)! >= 1{
            switch textField{
            case txtOtp1:
                txtOtp2.becomeFirstResponder()
            case txtOtp2:
                txtOtp3.becomeFirstResponder()
            case txtOtp3:
                txtOtp4.becomeFirstResponder()
            case txtOtp4:
                txtOtp5.becomeFirstResponder()
            case txtOtp5:
                txtOtp5.resignFirstResponder()
            default:
                break
            }
        }else{
            
            if (text?.utf16.count)! == 0{
                switch textField{
                case txtOtp1:
                    txtOtp1.resignFirstResponder()
                case txtOtp2:
                    txtOtp1.becomeFirstResponder()
                case txtOtp3:
                    txtOtp2.becomeFirstResponder()
                case txtOtp4:
                    txtOtp3.becomeFirstResponder()
                case txtOtp5:
                    txtOtp4.becomeFirstResponder()
                    
                default:
                    break
                }
            }
        }
    }
    
    
    func txtFieldvalidation() -> Bool
    {
        txtOtp1.text = txtOtp1.text?.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
        txtOtp2.text = txtOtp2.text?.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
        txtOtp3.text = txtOtp3.text?.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
        txtOtp4.text = txtOtp4.text?.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
        txtOtp5.text = txtOtp5.text?.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
        txtPassword.text = txtPassword.text?.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
        txtConfirmPassword.text = txtConfirmPassword.text?.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
        
        if(txtOtp1.text?.isEmpty == true)
        {
            HelpingMethod.shared.presentAlertWithTitle(title: Localization("Error"), message: Localization(Constants.errorMessage.otpInvalid.rawValue), vc: self)
            return false
        }
        else if(txtOtp2.text?.isEmpty == true)
        {
            HelpingMethod.shared.presentAlertWithTitle(title: Localization("Error"), message: Localization(Constants.errorMessage.otpInvalid.rawValue), vc: self)
            
            return false
        }
        else if(txtOtp3.text?.isEmpty == true)
        {
            HelpingMethod.shared.presentAlertWithTitle(title: Localization("Error"), message: Localization(Constants.errorMessage.otpInvalid.rawValue), vc: self)
            
            return false
        }
        else if(txtOtp4.text?.isEmpty == true)
        {
            HelpingMethod.shared.presentAlertWithTitle(title: Localization("Error"), message: Localization(Constants.errorMessage.otpInvalid.rawValue), vc: self)
            
            return false
        }else if(txtOtp5.text?.isEmpty == true)
        {
            HelpingMethod.shared.presentAlertWithTitle(title: Localization("Error"), message: Localization(Constants.errorMessage.otpInvalid.rawValue), vc: self)
            
            return false
        }else if(txtPassword.text?.isEmpty == true)
        {
            HelpingMethod.shared.presentAlertWithTitle(title: Localization("Error"), message: Localization(Constants.errorMessage.PasswordEmpty.rawValue), vc: self)
            
            return false
        }
        else if(txtConfirmPassword.text?.isEmpty == true)
        {
            HelpingMethod.shared.presentAlertWithTitle(title: Localization("Error"), message: Localization(Constants.errorMessage.ConfirmPasswordEmpty.rawValue), vc: self)
            
            return false
        }else if txtPassword.text != txtConfirmPassword.text{
            
            HelpingMethod.shared.presentAlertWithTitle(title: Localization("Error"), message: Localization(Constants.errorMessage.ConfirmPasswordInvalid.rawValue), vc: self)
            
            return false
        }else if self.strOtp != getUserEnterOtp(){
            
            HelpingMethod.shared.presentAlertWithTitle(title: Localization("Error"), message: Localization(Constants.errorMessage.otpNotMatch.rawValue), vc: self)
            
            return false
            
        }
        
        return true
    }
    
    
    func getUserEnterOtp() -> String
    {
        let strOtp = txtOtp1.text! + txtOtp2.text! + txtOtp3.text! + txtOtp4.text! + txtOtp5.text!
        return strOtp
    }
    
    
    //MARK:- ACTION
    
    @IBAction func btnBackClicked(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btnSubmitClicked(_ sender: UIButton) {
        
        if (self.txtFieldvalidation()){
            self.API_ChangePassword()
        }
        
    }
    
    
    //MARK:- API Call
    private func API_ForgotPassword(email:String){
        
        
        Hud.shared.showHudWithMsg()
        
        let dictKeys = [
            
            "email" : email
            
            ] as [String : Any]
        
        let url = Constants.AppLinks.API_BASE_URL + "\(Constants.ApiUrl.ForgetPassword)"
        
        
        Alamofire.request(url , method: .post, parameters: dictKeys, headers:nil).responseJSON(queue: .global(qos: .background)) { (response) in
            
            Hud.shared.hideHud()
            
            do {
                let json: JSON = try JSON(data: response.data!)
                if (json["statusCode"].stringValue == "200"){
                    let jsondata = json["data"]
                    self.userid = jsondata["user_id"].stringValue
                    self.strOtp = jsondata["otp"].stringValue
                    Hud.shared.show_SuccessHud(Localization("OTP has been sent to your email id"))
                    
                }
                else{
                    // error
                    Hud.shared.show_ErrorHud(json["message"].stringValue)
                }
                
            } catch {
                // show a alert
                Hud.shared.show_ErrorHud(Localization("Please check network connection"))
            }
        }
    }
    
    
    private func API_ChangePassword(){
        
        self.view.endEditing(true)
        
        Hud.shared.showHudWithMsg()
        
        let dictKeys = [
            
            "user_id" : self.userid,
            "password" : txtPassword.text!,
            "otp" : self.strOtp
            
            ] as [String : Any]
        
        let url = Constants.AppLinks.API_BASE_URL + "\(Constants.ApiUrl.ChangePassword)"
        
        
        Alamofire.request(url , method: .post, parameters: dictKeys, headers:nil).responseJSON(queue: .global(qos: .background)) { (response) in
            
            Hud.shared.hideHud()
            
            do {
                let json: JSON = try JSON(data: response.data!)
                if (json["statusCode"].stringValue == "200"){
                    //                        self.data = json["data"]
                    Hud.shared.show_SuccessHud(Localization("Your password has been changed successfully"))
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                        
                        self.navigationController?.popViewControllerWithCurlDownAnimation()
                    }
                }
                else{
                    // error
                    Hud.shared.show_ErrorHud(json["message"].stringValue)
                }
                
            } catch {
                // show a alert
                Hud.shared.show_ErrorHud(Localization("Please check network connection"))
            }
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

//MARK:- Textfield Delegate
extension ForgetPasswordVC:UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        switch textField {
            
        case txtPassword:
            txtConfirmPassword.becomeFirstResponder()
        case txtConfirmPassword:
            txtConfirmPassword.resignFirstResponder()
            
        default:
            return true
        }
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField != txtPassword && textField != txtConfirmPassword{
            
            let strCount = textField.text?.count ?? 0
            
            setUpTextFieldMoveNext()
            
            if (strCount >= 1 && range.length == 0) {
                return false
            } else {
                return true
            }
            
        }
        return true
    }
}

