//
//  LaunchScreenVC.swift
//  Gazi
//
//  Created by Apple on 15/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import CoreLocation

class LaunchScreenVC: UIViewController,CLLocationManagerDelegate {
    
    
    let locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            // Code you want to be delayed
            self.showVC()
        }
        
        // Do any additional setup after loading the view.
    }
    
    
    func showVC(){
        
        let userId = global.shared.getUserDataValueByKey(strKey: ApiKey.userId)
        
        if userId != ""{
            methodRedirectToHome()
        }
        else{
            checkLocationPermission()
        }
    }
    
    
    func methodRedirectToHome(){
        
        let stbdHome = staticClass.getStoryboard_Home()
        let nextViewController = stbdHome?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        let navigationController = UINavigationController(rootViewController: nextViewController)
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        appdelegate.window?.rootViewController = navigationController
        
    }
    
    
    func methodRedirectToLocationPermissionVC(){
        
        let stbdMain = staticClass.getStoryboard_Main()
        let nextViewController = stbdMain?.instantiateViewController(withIdentifier: "LocationPermissionVC") as! LocationPermissionVC
        let navigationController = UINavigationController(rootViewController: nextViewController)
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        appdelegate.window?.rootViewController = navigationController
        
    }
    
    func methodRedirectToLogInVC(){
        
        let stbdMain = staticClass.getStoryboard_Main()
        let nextViewController = stbdMain?.instantiateViewController(withIdentifier: "LogInVC") as! LogInVC
        let navigationController = UINavigationController(rootViewController: nextViewController)
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        appdelegate.window?.rootViewController = navigationController
        
    }
    
    
    
    func checkLocationPermission(){
        
        let locStatus = CLLocationManager.authorizationStatus()
        switch locStatus {
        case .denied, .restricted, .notDetermined:
            methodRedirectToLocationPermissionVC()
            
        case .authorizedAlways, .authorizedWhenInUse:
            methodRedirectToLogInVC()
            
        @unknown default:
            print("error")
        }
    }
    
    
    
    //    func showVC(){
    //
    //        let strbd = staticClass.getStoryboard_Main()
    //        let vc = strbd?.instantiateViewController(withIdentifier: "LocationPermissionVC") as! LocationPermissionVC
    //        self.navigationController?.pushViewController(vc, animated: true)
    //
    //    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
