//
//  LocationPermissionVC.swift
//  Gazi
//
//  Created by Apple on 09/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import CoreLocation

class LocationPermissionVC: UIViewController,CLLocationManagerDelegate {

    //Outlets connections
    
    @IBOutlet weak var btnSure: UIButton!
    @IBOutlet var lblAllowLocation: UILabel!
    @IBOutlet var lblPermissionDescription: UILabel!
    @IBOutlet var btnNotNow: UIButton!
    
    
    var isSureClicked = false
    
    let locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager.delegate = self
        
        self.setUp_UI()
        
        
        // Do any additional setup after loading the view.
    }
   
    
    func setUp_UI(){
        addGreenGradientsAndBorder(to: btnSure)
        changeStatusBarColor()
        
        lblAllowLocation.text = Localization("Allow your location")
        lblPermissionDescription.text = Localization("We will need your location to give you better experience.")
        
        btnSure.setTitle(Localization("Sure, I'd Like that"), for: .normal)
        btnNotNow.setTitle(Localization("Not Now"), for: .normal)
        
    }
    
    override func viewDidLayoutSubviews() {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    
    //MARK:- ACTION
    
    @IBAction func btnSureClicked(_ sender: UIButton) {
        self.isSureClicked = true
        
        let locStatus = CLLocationManager.authorizationStatus()
        switch locStatus {
           case .notDetermined:
              locationManager.requestWhenInUseAuthorization()
           return
           case .denied, .restricted:
              showPermissionAlert()
           return
           case .authorizedAlways, .authorizedWhenInUse:
            print("do something")
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "LogInVC") as! LogInVC
                   self.navigationController?.pushViewController(vc, animated: true)
        
        @unknown default:
            print("error")
        }
        
    }
    
    
    @IBAction func btnNotNowClicked(_ sender: UIButton) {
    
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "LogInVC") as! LogInVC
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if self.isSureClicked && (status == .authorizedAlways || status == .authorizedWhenInUse){
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "LogInVC") as! LogInVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        print("allow clicked")
    }
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
