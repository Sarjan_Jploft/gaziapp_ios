//
//  LogInVC.swift
//  Gazi
//
//  Created by Apple on 09/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class LogInVC: UIViewController {
    
    //Outlets Connections
    
    @IBOutlet weak var btnSelectLanguage: UIButton!
    @IBOutlet weak var txtLogin: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var btnLogIn: UIButton!
    @IBOutlet weak var btnSignUp: UIButton!
    
    @IBOutlet var lblSelectLang: UILabel!
    @IBOutlet var lblDefaultLang: UILabel!
    @IBOutlet var lblPrivacyPolicy: UILabel!
    
    @IBOutlet var btnForgotPassword: UIButton!
    
//    var arrayLanguage = ["Select Language","English","عربى","اردو","हिन्दी"]
    var arrayLanguage = ["English","عربى","हिन्दी"]
//     var arrayLanguage = ["English","اردو"]
    
    var toolBar = UIToolbar()
    var picker  = UIPickerView()
    
    var validation = Validation()
    
    let lang = UserDefaults.standard.string(forKey: "Applanguage") ?? "en"
    
    let arrayLanguages = Localisator.sharedInstance.getArrayAvailableLanguages()
    
    var languageCode = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        setUp_UI()
        
        txtLogin.delegate = self
        txtPassword.delegate = self
    
        
    }
   
    
   
    override func viewDidLayoutSubviews() {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    
    func setUp_UI() {
        
//        changeStatusBarColor()
        addPaddingAndBorder(to: txtLogin)
        addPaddingAndBorder(to: txtPassword)
        addGreenGradientsAndBorder(to: btnLogIn)
        
        btnSignUp.layer.borderWidth = 1
        btnSignUp.layer.borderColor = UIColor(red: 14/255, green: 191/255, blue: 122/255, alpha: 1).cgColor
                
        txtLogin.placeholder = Localization("Email ID")
        txtPassword.placeholder = Localization("Password")
        lblDefaultLang.text = Localization("Default Language")
        lblSelectLang.text = Localization("Select Language")
        lblPrivacyPolicy.text = Localization("By continuing you agree to our Terms of services Privacy Policy Content Policy")
        btnLogIn.setTitle(
            Localization("Login"), for: .normal)
        btnForgotPassword.setTitle(
        Localization("Forgot Password"), for: .normal)
        btnSignUp.setTitle(
        Localization("Sign Up"), for: .normal)
        
        if lang == "ar"{
            txtLogin.textAlignment = .right
            txtPassword.textAlignment = .right
        }else{
            txtLogin.textAlignment = .left
            txtPassword.textAlignment = .left
        }
        
        if(lang == "ar"){
            self.btnSelectLanguage.setTitle("عربى" + " >", for: .normal)
        }else if(lang == "hi"){
            self.btnSelectLanguage.setTitle("हिन्दी" + " >", for: .normal)
        }else{
            self.btnSelectLanguage.setTitle("English" + " >", for: .normal)
        }
        
    }
    
    func flipController(){
        
        let appDelegate: AppDelegate? = UIApplication.shared.delegate as? AppDelegate
        let lngCode = appDelegate?.ChangeLayout()
        
        let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
        rootviewcontroller.rootViewController = self.storyboard?.instantiateViewController(withIdentifier: "LaunchScreenVC")
        let mainwindow = (UIApplication.shared.delegate?.window!)!
        mainwindow.backgroundColor = UIColor(hue: 0.6477, saturation: 0.6314, brightness: 0.6077, alpha: 0.8)
        UIView.transition(with: mainwindow, duration: 0.55001, options: .transitionFlipFromLeft, animations: { () -> Void in
        }) { (finished) -> Void in
        }
        
    }
    
    //MARK:- ACTION
    
    
    @IBAction func btnLoginClicked(_ sender: UIButton) {
        
        txtLogin.text = txtLogin.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        txtPassword.text = txtPassword.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        
        if txtLogin.text!.isEmpty{
            
            HelpingMethod.shared.presentAlertWithTitle(title: Localization("Error"), message: Localization(Constants.errorMessage.EmailEmpty.rawValue), vc: self)

        }else if !(HelpingMethod.shared.VaildateTxtEmail(strEmail: txtLogin.text!)){
           
             HelpingMethod.shared.presentAlertWithTitle(title: Localization("Error"), message: Localization(Constants.errorMessage.EmailInvalid.rawValue), vc: self)
            
        }else if txtPassword.text!.isEmpty{
            
            HelpingMethod.shared.presentAlertWithTitle(title: Localization("Error"), message: Localization(Constants.errorMessage.PasswordEmpty.rawValue), vc: self)

        }else{
            
            API_Login()
            
        }
        
        
//       methodToRedirectHome()
        
        
        
    }
    
    
    @IBAction func btnForgetPasswordClicked(_ sender: UIButton) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ForgetPasswordVC") as! ForgetPasswordVC
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    @IBAction func btnSignUpClicked(_ sender: UIButton) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignUpVC") as! SignUpVC
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func btnSelectLanguageClicked(_ sender: UIButton) {
        /*
        toolBar.removeFromSuperview()
        picker.removeFromSuperview()
        
        self.view.endEditing(true)
        
        picker = UIPickerView.init()
        picker.delegate = self
        picker.backgroundColor = UIColor.lightGray//UIColor(named: "ThemeColor")
        picker.setValue(UIColor.black, forKey: "textColor")
        picker.autoresizingMask = .flexibleWidth
        picker.contentMode = .center
        picker.frame = CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 300, width: UIScreen.main.bounds.size.width, height: 300)
        
        toolBar = UIToolbar.init(frame: CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 300, width: UIScreen.main.bounds.size.width, height: 50))
        toolBar.barStyle = .default
        toolBar.items = [
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
            UIBarButtonItem.init(title:Localization("Done") , style: .done, target: self, action: #selector(onDoneButtonTapped))
        ]
        
        self.view.addSubview(picker)
        self.view.addSubview(toolBar)
        // pickerView.isHidden = false
        */
        openActionSheetForLanguage()
        
    }
    
    func openActionSheetForLanguage(){
        
        // create an actionSheet
        let actionSheetController: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        // create an action
        let english: UIAlertAction = UIAlertAction(title: self.arrayLanguage[0], style: .default) { action -> Void in
            
            UserDefaults.standard.set("en", forKey: "Applanguage")
            self.btnSelectLanguage.setTitle(self.arrayLanguage[0] + " >", for: .normal)
            if SetLanguage(self.arrayLanguages[0]) {}
            self.flipController()

        }
        
        let arabic: UIAlertAction = UIAlertAction(title: self.arrayLanguage[1], style: .default) { action -> Void in
            
            UserDefaults.standard.set("ar", forKey: "Applanguage")
            self.btnSelectLanguage.setTitle(self.arrayLanguage[1] + " >", for: .normal)
            if SetLanguage(self.arrayLanguages[1]) {}
            self.flipController()
        }
        
        let hindi: UIAlertAction = UIAlertAction(title: self.arrayLanguage[2], style: .default) { action -> Void in
            
            self.btnSelectLanguage.setTitle(self.arrayLanguage[2] + " >", for: .normal)
            if SetLanguage(self.arrayLanguages[2]) {}
            UserDefaults.standard.set("hi", forKey: "Applanguage")
            self.flipController()
            
        }
        
        
        
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in }
        
        // add actions
        actionSheetController.addAction(english)
        actionSheetController.addAction(arabic)
        actionSheetController.addAction(hindi)
        actionSheetController.addAction(cancelAction)
        
        
        
        present(actionSheetController, animated: true) {
            print("option menu presented")
        }
    }
    
    @objc func onDoneButtonTapped() {
        toolBar.removeFromSuperview()
        picker.removeFromSuperview()
    }
    
    
    private func API_Login(){
           
           Hud.shared.showHudWithMsg()
           
           let dictKeys = [
               
            "email" : txtLogin.text!,
            "password":txtPassword.text!
               
               ] as [String : Any]
           
        let url = Constants.AppLinks.API_BASE_URL + "\(Constants.ApiUrl.Login.rawValue)"
       
             
            
           Alamofire.request(url , method: .post, parameters: dictKeys, headers:nil).responseJSON(queue: .global(qos: .background))
           { (response) in
               print(response)
               Hud.shared.hideHud()
               
               do {
                   let json: JSON = try JSON(data: response.data!)
                   if (json["statusCode"].stringValue == "200"){
                    let jsonData = json["data"]
//                    let dict = (jsonData.dictionaryValue) as! NSDictionary
                    let isOtpVarified = jsonData["isOtpVarified"].stringValue
                    
                    let lat = jsonData["lat"].stringValue
                    let lng = jsonData["lng"].stringValue
                    
                    if isOtpVarified == "1"{
                        global.shared.saveJSON(json: jsonData, key: Constants.UserDefult.SaveUserData.rawValue)
                        
                        UserDefaults.standard.set(lat, forKey: "lat")
                        UserDefaults.standard.set(lng, forKey: "lng")
                       
                        DispatchQueue.main.async {
                            self.methodToRedirectHome()
                        }
                    }else{
                        //NEED TO REDIRECT VERIFICATION
                        self.methodRedirectToOtpVerify(strOtp: jsonData["otp"].stringValue, id: jsonData["user_id"].stringValue)
                    }
                    
                   
                   }
                   else{
                       // error
                       Hud.shared.show_ErrorHud(json["message"].stringValue)
                   }
                   
               } catch {
                   // show a alert
                   Hud.shared.show_ErrorHud(Localization("Please check network connection"))
               }
           }
       }
    
    
    func methodToRedirectHome(){
        
        
        let strbdHome = staticClass.getStoryboard_Home()
        let vc = strbdHome?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let nav = UINavigationController(rootViewController: vc)
        nav.isNavigationBarHidden = true
        appDelegate.window!.rootViewController = nav
        appDelegate.window?.makeKeyAndVisible()

        
    }
    
    func methodRedirectToOtpVerify(strOtp: String,id:String)
    {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OtpVerifyVC") as! OtpVerifyVC
        vc.strOtp = strOtp
        vc.strUserId = id
        vc.isFromLogin = true
        self.navigationController?.pushViewController(vc, animated: true)
        
    }

    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}




//MARK:- Picker View Delegate And Datasource

extension LogInVC:UIPickerViewDataSource,UIPickerViewDelegate{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrayLanguage.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return arrayLanguage[row]
    }
    /*
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        let lang = row == 0 ? "English" : arrayLanguage[row]
        self.btnSelectLanguage.setTitle(lang + " >", for: .normal)
        // self.pickerView.isHidden = true
        
        if SetLanguage(arrayLanguages[row]) {
            
        }
//        bestOfferLbl.text! = Localization("BestOffers")
//        self.txtLogin.text = Localization("HelloWorld")
    }
    */
    
    
     func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
            
            let lang = row == 0 ? "English" : arrayLanguage[row]
            self.btnSelectLanguage.setTitle(lang + " >", for: .normal)
            // self.pickerView.isHidden = true
            if lang == "English"
            {
//                Language.language = Language.english
                if SetLanguage(arrayLanguages[0]) {

                }

                UserDefaults.standard.set("en", forKey: "Applanguage")
                
            }else if lang == "عربى"{
//                Language.language = Language.arabic
                if SetLanguage(arrayLanguages[1]) {

                }
//
                UserDefaults.standard.set("ar", forKey: "Applanguage")
            }
            else
            {
//                Language.language = Language.hindi
                if SetLanguage(arrayLanguages[2]) {

                }
//
                UserDefaults.standard.set("hi", forKey: "Applanguage")
            }
        
//        DispatchQueue.main.async {
//            let appDelegate: AppDelegate? = UIApplication.shared.delegate as? AppDelegate
//            appDelegate?.ChangeLayout()
//        }
        
        UIView.appearance().semanticContentAttribute = .forceRightToLeft
        
        
        
//        txtLogin.placeholder = Localization("Email ID")
        
    //        bestOfferLbl.text! = Localization("BestOffers")
//            self.txtLogin.text = Localization("HelloWorld")
        }
}


//MARK:- Textfield Delegate
extension LogInVC:UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        switch textField {
            
        case txtLogin:
            txtPassword.becomeFirstResponder()
        case txtPassword:
            txtPassword.resignFirstResponder()
        
        default:
            return true
        }
        
        return true
    }
    
}
