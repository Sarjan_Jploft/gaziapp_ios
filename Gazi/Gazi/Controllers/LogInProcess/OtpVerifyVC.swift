//
//  OtpVerifyVC.swift
//  Gazi
//
//  Created by Apple on 09/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class OtpVerifyVC: UIViewController {
    
    @IBOutlet weak var headView: UIView!
    @IBOutlet weak var txtOtp1: UITextField!
    @IBOutlet weak var txtOtp2: UITextField!
    @IBOutlet weak var txtOtp3: UITextField!
    @IBOutlet weak var txtOtp4: UITextField!
    @IBOutlet weak var txtOtp5: UITextField!
    @IBOutlet weak var btnResendOtp: UIButton!
    @IBOutlet weak var btnVerify: UIButton!
    @IBOutlet weak var ViewstackTxt: UIView!
    @IBOutlet var lblOtpVerificationHead: UILabel!
    
    @IBOutlet var lblOtpDescription: UILabel!
    
    
    var strOtp = ""
    var strUserId = ""
    var isFromLogin = false
    
    let lang = UserDefaults.standard.string(forKey: "Applanguage") ?? "en"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUp_UI()
        selfDelegate()
        //fillOtp()
        // Do any additional setup after loading the view.
    }
    
    
    func setUp_UI(){
        
        changeStatusBarColor()
        addGreenGradientsAndBorder(to: btnVerify)
        headView.applyGradient(colours: [Constants.headViewTopGradient, Constants.headViewBottomGradient])
        ViewstackTxt.layer.borderWidth = 1
        ViewstackTxt.layer.borderColor = UIColor(red: 212/255, green: 212/255, blue: 212/255, alpha: 1).cgColor
        ViewstackTxt.layer.cornerRadius = 4
        ViewstackTxt.clipsToBounds = true
        
        if isFromLogin {
            API_ResendOtp()
        }
        
        
        
        lblOtpVerificationHead.text = Localization("OTP VERIFICATION")
        lblOtpDescription.text = Localization("One Time Password (OTP) has been sent to your Mobile number")
        
        btnResendOtp.setTitle(Localization("Resend OTP"), for: .normal)
        btnVerify.setTitle(Localization("Verify"), for: .normal)
        
        
        
        
    }
    
    func selfDelegate(){
        
        txtOtp1.delegate = self
        txtOtp2.delegate = self
        txtOtp3.delegate = self
        txtOtp4.delegate = self
        txtOtp5.delegate = self
    }
    
    
    func fillOtp()
    {
        let str = strOtp
        var i = 0
        for char in str
        {
            i+=1
            print("char :",char)
            switch i {
            case 1:
                self.txtOtp1.text = "\(char)"
                break
            case 2:
                self.txtOtp2.text = "\(char)"
                break
                
            case 3:
                self.txtOtp3.text = "\(char)"
                break
                
            case 4:
                self.txtOtp4.text = "\(char)"
                break
                
            case 5:
                self.txtOtp5.text = "\(char)"
                break
                
            default:
                break
            }
        }
        
    }
    
    
    
    func setUpTextFieldMoveNext()
    {
        
        txtOtp1.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        txtOtp2.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        txtOtp3.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        txtOtp4.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        txtOtp5.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        
    }
    
    
    @objc func textFieldDidChange(textField: UITextField)
    {
        let text = textField.text
        
        if (text?.utf16.count)! >= 1{
            switch textField{
            case txtOtp1:
                txtOtp2.becomeFirstResponder()
            case txtOtp2:
                txtOtp3.becomeFirstResponder()
            case txtOtp3:
                txtOtp4.becomeFirstResponder()
            case txtOtp4:
                txtOtp5.becomeFirstResponder()
            case txtOtp5:
                txtOtp5.resignFirstResponder()
            default:
                break
            }
        }else{
            
            if (text?.utf16.count)! == 0{
                switch textField{
                case txtOtp1:
                    txtOtp1.resignFirstResponder()
                case txtOtp2:
                    txtOtp1.becomeFirstResponder()
                case txtOtp3:
                    txtOtp2.becomeFirstResponder()
                case txtOtp4:
                    txtOtp3.becomeFirstResponder()
                case txtOtp5:
                    txtOtp4.becomeFirstResponder()
                    
                default:
                    break
                }
            }
        }
    }
    
    func txtFieldvalidation() -> Bool
    {
        txtOtp1.text = txtOtp1.text?.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
        txtOtp2.text = txtOtp2.text?.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
        txtOtp3.text = txtOtp3.text?.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
        txtOtp4.text = txtOtp4.text?.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
        txtOtp5.text = txtOtp5.text?.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
        
        if(txtOtp1.text?.isEmpty == true)
        {
            HelpingMethod.shared.presentAlertWithTitle(title: Localization("Error"), message: Localization(Constants.errorMessage.otpInvalid.rawValue), vc: self)
            return false
        }
        else if(txtOtp2.text?.isEmpty == true)
        {
            HelpingMethod.shared.presentAlertWithTitle(title: Localization("Error"), message: Localization(Constants.errorMessage.otpInvalid.rawValue), vc: self)
            
            return false
        }
        else if(txtOtp3.text?.isEmpty == true)
        {
            HelpingMethod.shared.presentAlertWithTitle(title: Localization("Error"), message: Localization(Constants.errorMessage.otpInvalid.rawValue), vc: self)
            
            return false
        }
        else if(txtOtp4.text?.isEmpty == true)
        {
            HelpingMethod.shared.presentAlertWithTitle(title: Localization("Error"), message: Localization(Constants.errorMessage.otpInvalid.rawValue), vc: self)
            
            return false
        }else if(txtOtp5.text?.isEmpty == true)
        {
            HelpingMethod.shared.presentAlertWithTitle(title: Localization("Error"), message: Localization(Constants.errorMessage.otpInvalid.rawValue), vc: self)
            
            return false
        }else if strOtp != getUserEnterOtp(){
            
            HelpingMethod.shared.presentAlertWithTitle(title: Localization("Error"), message: Localization(Constants.errorMessage.otpNotMatch.rawValue), vc: self)
            
            return false
            
        }
        
        return true
    }
    
    
    
    func getUserEnterOtp() -> String
    {
        let strOtp = txtOtp1.text! + txtOtp2.text! + txtOtp3.text! + txtOtp4.text! + txtOtp5.text!
        return strOtp
    }
    
    
    //MARK:- ACTION
    
    @IBAction func btnVerifyClicked(_ sender: UIButton) {
        
        
        if(self.txtFieldvalidation())
        {
            self.API_OtpVerify()
            
        }
        //        let vc = self.storyboard?.instantiateViewController(withIdentifier: "LogInVC") as! LogInVC
        //        self.navigationController?.pushViewControllerWithFlipAnimation(viewController: vc)
        
    }
    
    
    @IBAction func btnResendClicked(_ sender: UIButton) {
        
        API_ResendOtp()
    }
    
    
    @IBAction func btnBackClicked(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- API Call
    private func API_OtpVerify(){
        
        Hud.shared.showHudWithMsg()
        
        let dictKeys = [
            
            "user_id" : strUserId,
            "otp":getUserEnterOtp()
            
            ] as [String : Any]
        
        let url = Constants.AppLinks.API_BASE_URL + "\(Constants.ApiUrl.OtpVerify.rawValue)"
        
        
        Alamofire.request(url , method: .post, parameters: dictKeys, headers:nil).responseJSON(queue: .global(qos: .background)) { (response) in
            
            Hud.shared.hideHud()
            
            do {
                let json: JSON = try JSON(data: response.data!)
                if (json["statusCode"].stringValue == "200"){
                        let jsonData = json["data"]
                    
                    Hud.shared.show_SuccessHud(Localization("Your account is verified successfully"))
//                    DispatchQueue.main.async {
//                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "LogInVC") as! LogInVC
//                        self.navigationController?.pushViewControllerWithFlipAnimation(viewController: vc)
//                    }
                    
                    
                    global.shared.saveJSON(json: jsonData, key: Constants.UserDefult.SaveUserData.rawValue)
                    
                    
                    DispatchQueue.main.async {
                        self.methodToRedirectHome()
                    }
                    
                    
                    
                }
                else{
                    // error
                    Hud.shared.show_ErrorHud(json["message"].stringValue)
                }
                
            } catch {
                // show a alert
                Hud.shared.show_ErrorHud(Localization("Please check network connection"))
            }
        }
    }
    
    
    
    private func API_ResendOtp(){
        
        Hud.shared.showHudWithMsg()
        
        let dictKeys = [
            
            "user_id" : strUserId
            
            ] as [String : Any]
        
        let url = Constants.AppLinks.API_BASE_URL + "\(Constants.ApiUrl.ResendOtp.rawValue)"
        
        
        Alamofire.request(url , method: .post, parameters: dictKeys, headers:nil).responseJSON(queue: .global(qos: .background)) { (response) in
            
            Hud.shared.hideHud()
            
            do {
                
                let json: JSON = try JSON(data: response.data!)
                
                if (json["statusCode"].stringValue == "200"){
                    let jsondata = json["data"]
                    self.strOtp = jsondata["otp"].stringValue
                    Hud.shared.show_SuccessHud(json["message"].stringValue)
                    
                }
                else{
                    // error
                    Hud.shared.show_ErrorHud(json["message"].stringValue)
                }
                
            } catch {
                // show a alert
                Hud.shared.show_ErrorHud(Localization("Please check network connection"))
            }
        }
    }
    
    
    
    func methodToRedirectHome(){
           
           
           let strbdHome = staticClass.getStoryboard_Home()
           let vc = strbdHome?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
           let appDelegate = UIApplication.shared.delegate as! AppDelegate
           let nav = UINavigationController(rootViewController: vc)
           nav.isNavigationBarHidden = true
           appDelegate.window!.rootViewController = nav
           appDelegate.window?.makeKeyAndVisible()

           
       }
       
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

//MARK:- Textfield Delegate
extension OtpVerifyVC: UITextFieldDelegate
{
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let strCount = textField.text?.count ?? 0
        
        setUpTextFieldMoveNext()
        
        if (strCount >= 1 && range.length == 0) {
            return false
        } else {
            return true
        }
        
    }
    
}
