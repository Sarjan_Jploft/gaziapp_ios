//
//  SignUpVC.swift
//  Gazi
//
//  Created by Apple on 09/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SKCountryPicker
import CoreLocation

class SignUpVC: UIViewController {
    
    
    @IBOutlet weak var viewHead: UIView!
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtConfirmPass: UITextField!
    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet weak var btnSignUp: UIButton!
    @IBOutlet weak var btnSelectLanguage: UIButton!
    @IBOutlet weak var btnSelectCountryCode: UIButton!
    
    @IBOutlet weak var lblCountryCode: UILabel!
    @IBOutlet weak var imgFlag: UIImageView!
    
    
    @IBOutlet var lblSignUpHead: UILabel!
    @IBOutlet var lblSelectLang: UILabel!
    @IBOutlet var lblDefaultLang: UILabel!
    @IBOutlet var lblAlreadyHaveAnAc: UILabel!
    @IBOutlet var btnSignIn: UIButton!
    
    
    
    
    //    var arrayLanguage = ["Select Language","English","عربى","اردو","हिन्दी"]
    var arrayLanguage = ["English","عربى","हिन्दी"]
    
    let arrayLanguages = Localisator.sharedInstance.getArrayAvailableLanguages()
    
    
    var toolBar = UIToolbar()
    var picker  = UIPickerView()
    
    var locationManager = CLLocationManager()
    var languageCode = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUp_UI()
        selfDelegate()
        
        txtPhone.inputAccessoryView = self.methodAddToolbar()
        
        //        guard let country = CountryManager.shared.currentCountry else {
        //
        ////            btnSelectCountryCode.setTitle("+91", for: .normal)
        //            lblCountryCode.text = "+971"
        //            imgFlag.image = UIImage(named: "UAE")
        //
        //            return
        //        }
        //        btnSelectCountryCode.setTitle(country.dialingCode, for: .normal)
        lblCountryCode.text = "+971"//country.dialingCode
        imgFlag.image = UIImage(named: "UAE")//country.flag
        
        
        API_ReverseGeoCoding()
        
    }
    
   
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        print("viewWillAppear function called")
        
        NotificationCenter.default.addObserver(self, selector: #selector(SignUpVC.receiveLanguageChangedNotification(notification:)), name: kNotificationLanguageChanged, object: nil)
        
        configureViewFromLocalisation()
        
    }
    
    @objc func receiveLanguageChangedNotification(notification:NSNotification) {
        if notification.name == kNotificationLanguageChanged {
            configureViewFromLocalisation()
        }
    }
    
    func configureViewFromLocalisation() {}
    
    
    func setUp_UI(){
        
        changeStatusBarColor()
        
        addPaddingAndBorder(to: txtFirstName)
        addPaddingAndBorder(to: txtLastName)
        addPaddingAndBorder(to: txtPhone)
        addPaddingAndBorder(to: txtEmail)
        addPaddingAndBorder(to: txtPassword)
        addPaddingAndBorder(to: txtConfirmPass)
        addPaddingAndBorder(to: txtAddress)
        
        addGreenGradientsAndBorder(to: btnSignUp)
        
        viewHead.applyGradient(colours: [Constants.headViewTopGradient, Constants.headViewBottomGradient])
    
        txtFirstName.placeholder = Localization("First Name")
        txtLastName.placeholder = Localization("Last Name")
        txtPhone.placeholder = Localization("Phone Number")
        txtEmail.placeholder = Localization("Email")
        txtPassword.placeholder = Localization("Password")
        txtConfirmPass.placeholder = Localization("Confirm Password")
        txtAddress.placeholder = Localization("Add Address")
        
        lblSignUpHead.text = Localization("SIGNUP")
        lblSelectLang.text = Localization("Select Language")
        lblDefaultLang.text = Localization("Default Language")
        lblAlreadyHaveAnAc.text = Localization("Already have an account?")
        
        btnSignIn.setTitle(Localization("Sign In"), for: .normal)
        btnSignUp.setTitle(Localization("Sign Up"), for: .normal)
        
        let appDelegate: AppDelegate? = UIApplication.shared.delegate as? AppDelegate
        let lngCode = appDelegate?.ChangeLayout()
        
        if lngCode == "en"{
           self.btnSelectLanguage.setTitle(self.arrayLanguage[0] + " >", for: .normal)
        }else if lngCode == "ar"{
           self.btnSelectLanguage.setTitle(self.arrayLanguage[1] + " >", for: .normal)
        }else{
           self.btnSelectLanguage.setTitle(self.arrayLanguage[2] + " >", for: .normal)
        }
        
        
        
        if lngCode == "ar"{
            txtFirstName.textAlignment = .right
            txtLastName.textAlignment = .right
            txtPhone.textAlignment = .right
            txtEmail.textAlignment = .right
            txtPassword.textAlignment = .right
            txtConfirmPass.textAlignment = .right
            txtAddress.textAlignment = .right
        }else{
            txtFirstName.textAlignment = .left
            txtLastName.textAlignment = .left
            txtPhone.textAlignment = .left
            txtEmail.textAlignment = .left
            txtPassword.textAlignment = .left
            txtConfirmPass.textAlignment = .left
            txtAddress.textAlignment = .left
        }
        
    }
    
    func selfDelegate(){
        
        txtFirstName.delegate = self
        txtLastName.delegate = self
        txtPhone.delegate = self
        txtEmail.delegate = self
        txtPassword.delegate = self
        txtConfirmPass.delegate = self
        txtAddress.delegate = self
        
    }
    
    func flipController(){
        
        let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
        rootviewcontroller.rootViewController = self.storyboard?.instantiateViewController(withIdentifier: "LaunchScreenVC")
        let mainwindow = (UIApplication.shared.delegate?.window!)!
        mainwindow.backgroundColor = UIColor(hue: 0.6477, saturation: 0.6314, brightness: 0.6077, alpha: 0.8)
        UIView.transition(with: mainwindow, duration: 0.55001, options: .transitionFlipFromLeft, animations: { () -> Void in
        }) { (finished) -> Void in
        }
        
    }
    
    
    //MARK:- ACTION
    
    @IBAction func btnSelectCountryCodeClicked(_ sender: UIButton) {
        
        let _ = CountryPickerController.presentController(on: self) { (country) in
            
            //            self.btnSelectCountryCode.setTitle(country.dialingCode, for: .normal)
            
            self.imgFlag.image = country.flag
            self.lblCountryCode.text = country.dialingCode
            //            self.countryName = country.countryName
        }
        
    }
    
    @IBAction func btnSignInClicked(_ sender: UIButton) {
        
        self.navigationController?.popViewControllerWithCurlDownAnimation()
    }
    
    
    
    @IBAction func btnSignUpClicked(_ sender: UIButton) {
        
        txtFirstName.text = txtFirstName.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        txtLastName.text = txtLastName.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        txtPhone.text = txtPhone.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        txtEmail.text = txtEmail.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        txtPassword.text = txtPassword.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        txtConfirmPass.text = txtConfirmPass.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        txtAddress.text = txtAddress.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        
        if txtFirstName.text!.isEmpty{
            HelpingMethod.shared.presentAlertWithTitle(title: Localization("Error"), message: Localization(Constants.errorMessage.FirstNameEmpty.rawValue), vc: self)
        }else if txtLastName.text!.isEmpty{
            HelpingMethod.shared.presentAlertWithTitle(title: Localization("Error"), message: Localization(Constants.errorMessage.LastNameEmpty.rawValue), vc: self)
        }else if txtPhone.text!.isEmpty{
            HelpingMethod.shared.presentAlertWithTitle(title: Localization("Error"), message: Localization(Constants.errorMessage.MobileNoEmpty.rawValue), vc: self)
        }else if txtPhone.text!.count <= 7 || txtPhone.text!.count > 16 {
            HelpingMethod.shared.presentAlertWithTitle(title: Localization("Error"), message: Localization(Constants.errorMessage.MobileNoInvalid.rawValue), vc: self)
        }else if txtEmail.text!.isEmpty{
            HelpingMethod.shared.presentAlertWithTitle(title: Localization("Error"), message: Localization(Constants.errorMessage.EmailEmpty.rawValue), vc: self)
        }else if(HelpingMethod.shared.VaildateTxtEmail(strEmail: txtEmail.text!) ==  false){
            HelpingMethod.shared.presentAlertWithTitle(title: Localization("Error"), message: Localization(Constants.errorMessage.EmailInvalid.rawValue), vc: self)
        }else if txtPassword.text!.isEmpty{
            HelpingMethod.shared.presentAlertWithTitle(title: Localization("Error"), message: Localization(Constants.errorMessage.PasswordEmpty.rawValue), vc: self)
        }else if txtConfirmPass.text!.isEmpty{
            HelpingMethod.shared.presentAlertWithTitle(title: Localization("Error"), message: Localization(Constants.errorMessage.ConfirmPasswordEmpty.rawValue), vc: self)
        }else if txtPassword.text != txtConfirmPass.text{
            HelpingMethod.shared.presentAlertWithTitle(title: Localization("Error"), message: Localization(Constants.errorMessage.ConfirmPasswordInvalid.rawValue), vc: self)
        }else if txtAddress.text!.isEmpty{
            HelpingMethod.shared.presentAlertWithTitle(title: Localization("Error"), message: Localization(Constants.errorMessage.AddressEmpty.rawValue), vc: self)
        }else{
            
            getLocation(from: txtAddress.text!) { (location) in
                
                if location == nil{
                    HelpingMethod.shared.presentAlertWithTitle(title: Localization("Error"), message: Localization("Unable to find Address"), vc: self)
                }else{
                    let strLat = String(location?.latitude ?? 0.0)
                    let strLng = String(location?.longitude ?? 0.0)
                    print(strLat)
                    print(strLng)
                    
                    self.API_SignUp(strLat, strLng)
                }
            }
            
            //API_SignUp()
        }
        
        
        //                let vc = self.storyboard?.instantiateViewController(withIdentifier: "OtpVerifyVC") as! OtpVerifyVC
        //                self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func btnSelectLanguageClicked(_ sender: UIButton) {
        /*
         toolBar.removeFromSuperview()
         picker.removeFromSuperview()
         
         self.view.endEditing(true)
         
         picker = UIPickerView.init()
         picker.delegate = self
         picker.backgroundColor = UIColor.lightGray//UIColor(named: "ThemeColor")
         picker.setValue(UIColor.black, forKey: "textColor")
         picker.autoresizingMask = .flexibleWidth
         picker.contentMode = .center
         picker.frame = CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 300, width: UIScreen.main.bounds.size.width, height: 300)
         
         toolBar = UIToolbar.init(frame: CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 300, width: UIScreen.main.bounds.size.width, height: 50))
         toolBar.barStyle = .default
         toolBar.items = [
         UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
         UIBarButtonItem.init(title: "Done", style: .done, target: self, action: #selector(onDoneButtonTapped))
         ]
         
         self.view.addSubview(picker)
         self.view.addSubview(toolBar)
         // pickerView.isHidden = false
         */
        openActionSheetForLanguage()
        
    }
    
    
    
    private func openActionSheetForLanguage(){
        
        
        
        // create an actionSheet
        let actionSheetController: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        // create an action
        let english: UIAlertAction = UIAlertAction(title: self.arrayLanguage[0], style: .default) { action -> Void in
            
            UserDefaults.standard.set("en", forKey: "Applanguage")
            self.btnSelectLanguage.setTitle(self.arrayLanguage[0] + " >", for: .normal)
            if SetLanguage(self.arrayLanguages[0]) {}
            self.flipController()
            
            
        }
        
        let arabic: UIAlertAction = UIAlertAction(title: self.arrayLanguage[1], style: .default) { action -> Void in
            
            UserDefaults.standard.set("ar", forKey: "Applanguage")
            self.btnSelectLanguage.setTitle(self.arrayLanguage[1] + " >", for: .normal)
            if SetLanguage(self.arrayLanguages[1]) {}
            self.flipController()
            
            
        }
        
        let hindi: UIAlertAction = UIAlertAction(title: self.arrayLanguage[2], style: .default) { action -> Void in
            
            self.btnSelectLanguage.setTitle(self.arrayLanguage[2] + " >", for: .normal)
            if SetLanguage(self.arrayLanguages[2]) {}
            UserDefaults.standard.set("hi", forKey: "Applanguage")
            self.flipController()
           
        }
        
        
        
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in }
        
        // add actions
        
        actionSheetController.addAction(english)
        actionSheetController.addAction(arabic)
        actionSheetController.addAction(hindi)
        actionSheetController.addAction(cancelAction)
        
        
        
        present(actionSheetController, animated: true) {
            print("option menu presented")
        }
        
          
    }
    
    
    
    @objc func onDoneButtonTapped() {
        toolBar.removeFromSuperview()
        picker.removeFromSuperview()
    }
    
    
    //MARK: Toolbar method
    func methodAddToolbar() -> UIToolbar
    {
        let toolbar = UIToolbar(frame:CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        toolbar.barStyle = .default
        toolbar.items = [
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
            UIBarButtonItem(title: Localization("Done"), style: .plain, target: self, action: #selector(clickOnDone))]
        toolbar.sizeToFit()
        
        return toolbar
        
    }
    
    
    @objc func clickOnDone()
    {
        self.view.endEditing(true)
    }
    
    
    //MARK:- API Call SignUp
    
    private func API_SignUp(_ lat:String, _ lng:String){
        
        Hud.shared.showHudWithMsg()
        
        let dictKeys = [
            
            "fname" : txtFirstName.text!,
            "lname" : txtLastName.text!,
            "mobile" : txtPhone.text!,
            "email" : txtEmail.text!,
            "password" : txtPassword.text!,
            "address": txtAddress.text!,
            "lat" : lat,
            "lng" : lng
            
            ] as [String : Any]
        
        let url = Constants.AppLinks.API_BASE_URL + "\(Constants.ApiUrl.SignUp.rawValue)"
        
        
        Alamofire.request(url , method: .post, parameters: dictKeys, headers:nil).responseJSON(queue: .global(qos: .background)) { (response) in
            
            Hud.shared.hideHud()
            
            do {
                
                let json: JSON = try JSON(data: response.data!)
                
                if (json["statusCode"].stringValue == "200"){
                    
                    UserDefaults.standard.set(lat, forKey: "lat")
                    UserDefaults.standard.set(lng, forKey: "lng")
                    
                    
                    let jsondata = json["data"]
                    let otp = jsondata["otp"].stringValue
                    let strid = jsondata["user_id"].stringValue
                    
                    DispatchQueue.main.async {
                        self.methodRedirectToOtpVerify(strOtp: otp, id: strid)
                    }
                    
                }
                else{
                    // error
                    Hud.shared.show_ErrorHud(json["message"].stringValue)
                }
                
            } catch {
                // show a alert
                Hud.shared.show_ErrorHud(Localization("Please check network connection"))
            }
        }
    }
    
    
    
    func methodRedirectToOtpVerify(strOtp: String,id:String)
    {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OtpVerifyVC") as! OtpVerifyVC
        vc.strOtp = strOtp
        vc.strUserId = id
        vc.isFromLogin = false
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    //MARK:- API Reverse Geo coding
    func API_ReverseGeoCoding(){
        if locationManager.location == nil{return}
        CLGeocoder().reverseGeocodeLocation(locationManager.location!, completionHandler: {(placemarks, error)-> Void in
            if (error != nil) {
                print("Reverse geocoder failed with error" + error!.localizedDescription)
                return
            }
            
            if placemarks!.count > 0 {
                let pm = placemarks![0] as CLPlacemark
                self.displayLocationInfo(placemark: pm)
            } else {
                print("Problem with the data received from geocoder")
            }
        })
        
    }
    
    
    func displayLocationInfo(placemark: CLPlacemark) {
        
        //stop updating location to save battery life
        locationManager.stopUpdatingLocation()
        //            print((placemark.locality != nil) ? placemark.locality : "")
        //            print((placemark.postalCode != nil) ? placemark.postalCode : "")
        //            print((placemark.administrativeArea != nil) ? placemark.administrativeArea : "")
        //            print((placemark.country != nil) ? placemark.country : "")
        //            print(placemark.subLocality)
        //            print(placemark.name)
        //            print(placemark.region)
        
        let name = placemark.name ?? ""
        let subLocality = placemark.subLocality ?? ""
        let locality = placemark.locality ?? ""
        let administrativeArea = placemark.administrativeArea ?? ""
        let country = placemark.country ?? ""
        let postalCode = placemark.postalCode ?? ""
        
        
        
        self.txtAddress.text = "\(name), \(subLocality), \(locality), \(administrativeArea), \(country), \(postalCode)"
        
    }
    
    
    
    //MARK:- Get Lat Long From Address
    func getLocation(from address: String, completion: @escaping (_ location: CLLocationCoordinate2D?)-> Void) {
        let geocoder = CLGeocoder()
        geocoder.geocodeAddressString(address) { (placemarks, error) in
            guard let placemarks = placemarks,
                let location = placemarks.first?.location?.coordinate else {
                    completion(nil)
                    return
            }
            completion(location)
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}


//MARK:- Picker View Delegate And Datasource

extension SignUpVC:UIPickerViewDataSource,UIPickerViewDelegate{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrayLanguage.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return arrayLanguage[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        let lang = row == 0 ? "English" : arrayLanguage[row]
        self.btnSelectLanguage.setTitle(lang + " >", for: .normal)
        
        if lang == "English"
        {
            //            Language.language = Language.english
            if SetLanguage(arrayLanguages[0]) {
                
            }
            UserDefaults.standard.set("en", forKey: "Applanguage")
        }else if lang == "عربى"{
            //            Language.language = Language.arabic
            if SetLanguage(arrayLanguages[1]) {
                
            }
            UserDefaults.standard.set("ar", forKey: "Applanguage")
        }
        else
        {
            //            Language.language = Language.hindi
            if SetLanguage(arrayLanguages[2]) {
                
            }
            UserDefaults.standard.set("hi", forKey: "Applanguage")
        }
        
        DispatchQueue.main.async {
            let appDelegate: AppDelegate? = UIApplication.shared.delegate as? AppDelegate
            appDelegate?.ChangeLayout()
        }
        
        // self.pickerView.isHidden = true
    }
    
}


//MARK:- Textfield Delegate
extension SignUpVC:UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        switch textField {
            
        case txtFirstName:
            txtLastName.becomeFirstResponder()
        case txtLastName:
            txtPhone.becomeFirstResponder()
        case txtPhone:
            txtEmail.becomeFirstResponder()
        case txtEmail:
            txtPassword.becomeFirstResponder()
        case txtPassword:
            txtConfirmPass.becomeFirstResponder()
        case txtConfirmPass:
            txtAddress.becomeFirstResponder()
        case txtAddress:
            txtAddress.resignFirstResponder()
        default:
            txtAddress.resignFirstResponder()
        }
        //        self.view.endEditing(true)
        return true
    }
    
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtPhone{
            
            let currentText = textField.text ?? ""
            guard let stringRange = Range(range, in: currentText) else { return false }
            let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
            
            return updatedText.count < 16
        }
        return true
    }
    
}
