//
//  CancelReasonVC.swift
//  Gazi
//
//  Created by Apple on 30/09/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


protocol cancelOrderDelegate {
    func btnSubmitTapped(reason:String)
}

class CancelReasonVC: UIViewController {
    
    @IBOutlet var tbl: UITableView!
    @IBOutlet var viewContainer: UIView!
    @IBOutlet var btnCancel: UIButton!
    @IBOutlet var btnSubmit: UIButton!
    @IBOutlet var lblChooseReason: UILabel!
    
    
    
    
    
    var selectedIndex:Int = 0
    var driverId = ""
    var delegate:cancelOrderDelegate?
    var jsonData:[JSON] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewContainer.layer.cornerRadius = 16
        btnCancel.layer.cornerRadius = 4
        btnSubmit.layer.cornerRadius = 4
        
        lblChooseReason.text = Localization("Please Choose a reason")
        btnCancel.setTitle(Localization("Back"), for: .normal)
        btnSubmit.setTitle(Localization("Submit"), for: .normal)
        
        API_CancelReasonList()
    }
    
    
    
    
    //MARK:- ACTIONS
    
    @IBAction func btnCancelTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func btnSubmitTapped(_ sender: UIButton) {
        self.dismiss(animated: true) {
            self.delegate?.btnSubmitTapped(reason: self.jsonData[sender.tag]["title"].stringValue)
        }
    }
    
    
    @objc
    func btnRoundTapped(_ sender:UIButton){
        self.selectedIndex = sender.tag
        self.tbl.reloadData()
    }
    
    
    
    //MARK:- API Call
    
    private func API_CancelReasonList(){
        
        Hud.shared.showHudWithMsg()
        
        let dictKeys = [
            "driverId" : driverId,
            ] as [String : Any]
        
        let url = Constants.AppLinks.API_BASE_URL + "\(Constants.ApiUrl.CancelReasons.rawValue)"
        
        print("param", dictKeys)
        print("url is",url)
        
        Alamofire.request(url , method: .post, parameters: dictKeys, headers:nil).responseJSON(queue: .global(qos: .background))
        { (response) in
            print(response)
            Hud.shared.hideHud()
            
            do {
                let json: JSON = try JSON(data: response.data!)
                
                if (json["statusCode"].stringValue == "200"){
                    
                    self.jsonData = json["data"].arrayValue
                    
                    DispatchQueue.main.async {
                        self.tbl.reloadData()
                    }
                    
                }
                else{
                    // error
                    Hud.shared.show_ErrorHud(json["message"].stringValue)
                }
                
            } catch {
                // show a alert
                Hud.shared.show_ErrorHud("Please check network connection")
            }
        }
    }
    
    
    
}


//MARK:- TableView Delegate And DataSource

extension CancelReasonVC:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return jsonData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CancelReasonsListTVCell") as! CancelReasonsListTVCell
        
        cell.lblReason.text = jsonData[indexPath.row]["title"].stringValue
        self.selectedIndex == indexPath.row ? (cell.btnRound.isSelected = true) : (cell.btnRound.isSelected = false)
        
        cell.btnRound.tag = indexPath.row
        cell.btnRound.addTarget(self, action: #selector(btnRoundTapped(_:)), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.selectedIndex = indexPath.row
        self.tbl.reloadData()
        
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return .leastNormalMagnitude
    }
    
}

