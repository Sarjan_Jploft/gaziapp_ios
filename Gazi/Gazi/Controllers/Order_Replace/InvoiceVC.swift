//
//  InvoiceVC.swift
//  Gazi
//
//  Created by Apple on 19/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import WebKit


class InvoiceVC: UIViewController {
    
    @IBOutlet var webkit: WKWebView!

    var urlInvoice = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let request = URLRequest(url: URL(string: urlInvoice)!)
        webkit.load(request)
       
    }
    


}
