//
//  OrderDetailVC.swift
//  Gazi
//
//  Created by Apple on 10/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class OrderDetailVC: UIViewController {
    
    @IBOutlet weak var viewHead: UIView!
    @IBOutlet weak var lblReplaceCylinderPrice: UILabel!
    @IBOutlet weak var lblNewCylinderPrice: UILabel!
    @IBOutlet weak var lblDeliveryCost: UILabel!
    @IBOutlet weak var lbltax: UILabel!
    @IBOutlet weak var lblTotalPrice: UILabel!
    @IBOutlet weak var txtPromoCode: UITextField!
    @IBOutlet weak var btnApply: UIButton!
    @IBOutlet weak var txtViewDeliveryNote: UITextView!
    @IBOutlet weak var lblDeliveryNotePlaceHolder: UILabel!
    @IBOutlet weak var btnCardSelect: UIButton!
    @IBOutlet weak var btnOrderNow: UIButton!
    @IBOutlet weak var viewTextView: UIView!
    @IBOutlet weak var lblPaymentMode:UILabel!
    @IBOutlet weak var lblFinalAmount: UILabel!
    @IBOutlet weak var lblPromoDiscount: UILabel!
    
    @IBOutlet weak var stackFinalAmount: UIStackView!
    @IBOutlet weak var stackPromoCodeDiscount: UIStackView!
    @IBOutlet weak var stackCylinderPriceNew: UIStackView!
    @IBOutlet weak var stackCylinderPriceReplace: UIStackView!
    
    @IBOutlet weak var lblHeadCylinderPriceReplace: UILabel!
    @IBOutlet weak var lblHeadCylinderPriceNew: UILabel!
    @IBOutlet weak var lblDeliveryCostReplace: UILabel!
    @IBOutlet var lblOrderNowHead: UILabel!
    @IBOutlet var lblPromoCode: UILabel!
    @IBOutlet var lblTaxText: UILabel!
    @IBOutlet var lblTotalText: UILabel!
    
    
    @IBOutlet var lblPaymentModeText: UILabel!
    
    
    
    var arrayPaymentMode = ["Select Payment Mode","Card","Cash on delivery"]
    
    let lang = UserDefaults.standard.string(forKey: "Applanguage") ?? "en"
    
    var toolBar = UIToolbar()
    var picker  = UIPickerView()
    
    
    var isReplaceNewBothSelected = false
    var isReplaceOnly = false
    var isNewOnly = false
    
    var newCylinderPrice = 0.0
    var newCylinderQnty = 0
    var replacementCylinderPrice = 0.0
    var replacementCylinderQty = 0
    var newCylinderId = ""
    var ReplacedCylinderId = ""
    
    var tax = 0.0
    var deliveryCost = 0.0
    var totalAmount = 0.0
    var promoCodeDiscount = 0.0
    var finalAmount = 0.0
    
    var strPayMode = ""
    var strBookingId = ""
    var strDriverId = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUp_UI()
        initialSetUp()
        
        
        
        // Do any additional setup after loading the view.
    }
    
    
    func setUp_UI(){
        
        btnApply.layer.cornerRadius = 4
        changeStatusBarColor()
        addPaddingAndBorder(to: txtPromoCode)
        addGreenGradientsAndBorder(to: btnOrderNow)
        viewTextView.layer.borderColor = Constants.textfieldBordercolor
        viewTextView.layer.borderWidth = 1
        viewTextView.layer.cornerRadius = Constants.textFiledCornerRadius
        viewHead.applyGradient(colours: [Constants.headViewTopGradient, Constants.headViewBottomGradient])
        
        if isReplaceOnly{
            self.stackCylinderPriceNew.isHidden = true
            self.stackCylinderPriceReplace.isHidden = false
        }else if isNewOnly{
            self.stackCylinderPriceNew.isHidden = false
            self.stackCylinderPriceReplace.isHidden = true
        }else if isReplaceNewBothSelected{
            self.stackCylinderPriceNew.isHidden = false
            self.stackCylinderPriceReplace.isHidden = false
        }
        
        stackPromoCodeDiscount.isHidden = true
        stackFinalAmount.isHidden = true
        
        
        lblOrderNowHead.text = Localization("ORDER NOW")
        lblHeadCylinderPriceNew.text = Localization("New Cylinder Price ")
        lblDeliveryNotePlaceHolder.text = Localization("Note for delivery guy")
        lblPaymentMode.text = Localization("Cash on delivery")
        lblHeadCylinderPriceReplace.text = Localization("Replacement Cylinder Price")
        lblDeliveryCostReplace.text = Localization("Delivery Cost")
        lblPromoCode.text = Localization("Promo Code Discount")
        lblTaxText.text = Localization("Tax")
        lblTotalText.text = Localization("Total")
        lblPaymentModeText.text = Localization("Payment Mode")
        
        txtPromoCode.placeholder = Localization("Promo Code")
        
        btnApply.setTitle(Localization("Apply"), for: .normal)
        btnOrderNow.setTitle(Localization("Order Now"), for: .normal)
        
        if lang == "ar"{
            txtPromoCode.textAlignment = .right
        }else{
            txtPromoCode.textAlignment = .left
        }
        
    }
    
    func initialSetUp(){
        
        txtViewDeliveryNote.delegate = self
        
        totalAmount = (newCylinderPrice * Double(newCylinderQnty)) + (replacementCylinderPrice * Double(replacementCylinderQty))
        
        lblNewCylinderPrice.text = (newCylinderPrice * Double(newCylinderQnty)).removeZerosFromEnd() + " AED"
        lblReplaceCylinderPrice.text = (replacementCylinderPrice * Double(replacementCylinderQty)).removeZerosFromEnd() + " AED"
        
        
        let taxAmount = (tax * totalAmount) / 100
        lbltax.text = taxAmount.removeZerosFromEnd() + " AED"
        lblDeliveryCost.text = deliveryCost.removeZerosFromEnd() + " AED"
        //        lblFinalAmount.text = String(totalAmount - promoCodeDiscount + taxAmount + deliveryCost)
        lblTotalPrice.text = (totalAmount + taxAmount + deliveryCost).removeZerosFromEnd() + " AED"
        self.finalAmount = totalAmount - promoCodeDiscount + taxAmount + deliveryCost
        
    }
    
    //MARK:- ACTION
    
    @IBAction func btnOrderNowClicked(_ sender: UIButton) {
        txtPromoCode.text = txtPromoCode.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        API_BookNow()
        
    }
    
    
    @IBAction func btnApplyclicked(_ sender: UIButton) {
        
        let coupancode = txtPromoCode.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        
        if coupancode!.isEmpty{
            
            HelpingMethod.shared.presentAlertWithTitle(title: Localization("Error"), message:Localization(Constants.errorMessage.Enterpromocode.rawValue), vc: self)
            
        }else{
            
            API_ApplyCoupanCode()
        }
        
    }
    
    
    @IBAction func btnPaymentModeClciked(_ sender: UIButton) {
        
        toolBar.removeFromSuperview()
        picker.removeFromSuperview()
        
        self.view.endEditing(true)
        
        picker = UIPickerView.init()
        picker.delegate = self
        picker.backgroundColor = UIColor.lightGray//UIColor(named: "ThemeColor")
        picker.setValue(UIColor.black, forKey: "textColor")
        picker.autoresizingMask = .flexibleWidth
        picker.contentMode = .center
        picker.frame = CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 300, width: UIScreen.main.bounds.size.width, height: 300)
        
        toolBar = UIToolbar.init(frame: CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 300, width: UIScreen.main.bounds.size.width, height: 50))
        toolBar.barStyle = .default
        toolBar.items = [
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
            UIBarButtonItem.init(title: Localization("Done"), style: .done, target: self, action: #selector(onDoneButtonTapped))
        ]
        
        self.view.addSubview(picker)
        self.view.addSubview(toolBar)
        // pickerView.isHidden = false
        
        
    }
    
    @objc func onDoneButtonTapped() {
        toolBar.removeFromSuperview()
        picker.removeFromSuperview()
    }
    
    
    
    
    @IBAction func btnBackClicked(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    //MARK:- Method To Redirect After Booking
    
    func redirectAfterBooking(){
        
        
        DispatchQueue.main.async {
            
            if self.lblPaymentMode.text == Localization("Card"){
                
                
                let strbd = staticClass.getStoryboard_Payment()
                let vc = strbd?.instantiateViewController(withIdentifier: "CardDetailVC") as! CardDetailVC
                self.navigationController?.pushViewController(vc, animated: true)
                
                
            }else{
                Hud.shared.show_SuccessHud(Localization("Cylinder(s) Booked Successfully"))
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                    
                    self.navigationController?.popToRootViewController(animated: true)
                }
                
            }
            
        }
        
    }
    
    
    
    //MARK:- API CALL
    
    private func API_BookNow(){
        
        Hud.shared.showHudWithMsg()
        
        let userid = global.shared.getUserDataValueByKey(strKey: ApiKey.userId)
        var dictKeys =  [String : Any]()
        
        let promocode =  self.btnApply.backgroundColor == UIColor(red: 11/255, green: 178/255, blue: 106/255, alpha: 1) ? txtPromoCode.text! : ""
            
        
        
        dictKeys = [
            
            "userId" : userid,
            "paymentMode" : "Cash on delivery",//self.lblPaymentMode.text!,
            "amount" : self.totalAmount,
            "netAmount" : self.finalAmount,
            "note":txtViewDeliveryNote.text!,
            "coupon":promocode
        
        ]
        
        if isNewOnly{
            
            dictKeys["pidnew"] = newCylinderId
            dictKeys["qtynew"] = self.newCylinderQnty
            dictKeys["type"] = "new"
            
        }else if isReplaceOnly{
            
            dictKeys["pidreplace"] = ReplacedCylinderId
            dictKeys["qtyreplace"] = self.replacementCylinderQty
            dictKeys["type"] = "replace"
            
        }else if isReplaceNewBothSelected{
            
            dictKeys["pidnew"] = newCylinderId
            dictKeys["qtynew"] = self.newCylinderQnty
            dictKeys["pidreplace"] = ReplacedCylinderId
            dictKeys["qtyreplace"] = self.replacementCylinderQty
            dictKeys["type"] = "both"
            
        }
        
        let url = Constants.AppLinks.API_BASE_URL + "\(Constants.ApiUrl.BookNow.rawValue)"
        
        Alamofire.request(url , method: .post, parameters: dictKeys, headers:nil).responseJSON(queue: .global(qos: .background))
        { (response) in
            print(response)
            
            Hud.shared.hideHud()
            
            do {
                
                let json: JSON = try JSON(data: response.data!)
                if (json["statusCode"].stringValue == "200"){
                    
                    
                    
                    self.strBookingId = json["orderId"].stringValue
                    self.searchDriverAPI()
                    
                    
                    
                }
                else{
                    // error
                    Hud.shared.show_ErrorHud(json["message"].stringValue)
                }
                
            } catch {
                // show a alert
                Hud.shared.show_ErrorHud(Localization("Please check network connection"))
            }
        }
    }
    
    
    
    private func API_ApplyCoupanCode(){
        
        Hud.shared.showHudWithMsg()
        
        let userid = global.shared.getUserDataValueByKey(strKey: ApiKey.userId)
        
        let dictKeys = [
            
            "user_id" : userid,
            "promoCode" : txtPromoCode.text!,
            "amount" : (newCylinderPrice * Double(newCylinderQnty)) + (replacementCylinderPrice * Double(replacementCylinderQty))
            
            ] as [String : Any]
        
        let url = Constants.AppLinks.API_BASE_URL + "\(Constants.ApiUrl.ApplyPromoCode.rawValue)"
        
        
        Alamofire.request(url , method: .post, parameters: dictKeys, headers:nil).responseJSON(queue: .global(qos: .background))
        { (response) in
            print(response)
            Hud.shared.hideHud()
            
            do {
                
                let json: JSON = try JSON(data: response.data!)
                if (json["statusCode"].stringValue == "200"){
                    
                    Hud.shared.show_SuccessHud(Localization("Congratulations!! Coupon code applied successfully"))
                    let newAmount = json["newamount"].doubleValue
                    let taxAmount = (newAmount * self.tax) / 100
                    self.finalAmount = newAmount + taxAmount + self.deliveryCost
                    let discount = self.totalAmount - newAmount
                    
                    DispatchQueue.main.async {
                        //                        self.lblFinalAmount.text = self.finalAmount.removeZerosFromEnd() + " AED"
                        self.lblTotalPrice.text = self.finalAmount.removeZerosFromEnd() + " AED"
                        self.lblPromoDiscount.text = discount.removeZerosFromEnd() + " AED"
                        self.lbltax.text = taxAmount.removeZerosFromEnd() + " AED"
                        
                        UIView.animate(withDuration: 0.5) {
                            
                            self.stackFinalAmount.isHidden = true
                            self.stackPromoCodeDiscount.isHidden = false
                            
                        }
                        
                        self.btnApply.setTitle(Localization("Applied"), for: .normal)
                        self.btnApply.backgroundColor = UIColor(red: 11/255, green: 178/255, blue: 106/255, alpha: 1)
                        self.txtPromoCode.isUserInteractionEnabled = false
                        self.btnApply.isUserInteractionEnabled = false
                        self.view.endEditing(true)
                    }
                    
                }
                else{
                    // error
                    Hud.shared.show_ErrorHud(json["message"].stringValue)
                }
                
            } catch {
                // show a alert
                Hud.shared.show_ErrorHud(Localization("Please check network connection"))
            }
        }
    }
    
    
    
    //MARK:- API Search Driver
    
    private func searchDriverAPI(){
        
        
        Hud.shared.showHudWithMsg()
        
        let userid = global.shared.getUserDataValueByKey(strKey: ApiKey.userId)
        
        //FIXME:
        let strLat = UserDefaults.standard.string(forKey: "lat") ?? ""
        let strLng = UserDefaults.standard.string(forKey: "lng") ?? ""

//        let strLat = "27.4136917"//UserDefaults.standard.string(forKey: "lat") ?? ""
//        let strLng = "77.3108998"//UserDefaults.standard.string(forKey: "lng") ?? ""
        
        let dictKeys = [
            
            "user_id" : userid,
            "booking_id" : strBookingId,
            "user_latitude" : strLat,
            "user_longlatitude" : strLng
            
            ] as [String : Any]
        
        
        let url = Constants.AppLinks.API_BASE_URL + "\(Constants.ApiUrl.SearchDriver.rawValue)"
        
        print("param",dictKeys)
        print("url",url)
        
        Alamofire.request(url , method: .post, parameters: dictKeys, headers:nil).responseJSON(completionHandler: { (response) in
            
            //            Hud.shared.hideHud()
            print(response)
            do {
                
                let json: JSON = try JSON(data: response.data!)
                print("json is",json)
                print("code is",json["statusCode"].stringValue )
                
                if (json["statusCode"].stringValue == "200"){
                    
                    let jsonDic = json["bookingData"].dictionaryValue
                    self.strDriverId = jsonDic["driverId"]?.stringValue ?? ""
                    
                    self.API_GetUserDriverLatLng()
                    
                }
                else if (json["statusCode"].stringValue == "201"){
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 10.0) {
                        DispatchQueue.global(qos: .background).async {
                            self.searchDriverAPI()
                        }
                    }
                    
                }
                    
                else if (json["statusCode"].stringValue == "202"){
                    
                    //  driver not found
                   
                    Hud.shared.show_ErrorHud(json["message"].stringValue)
                } else{
                    
                    // error
                    
                    Hud.shared.show_ErrorHud(json["message"].stringValue)
                }
                
            } catch {
                Hud.shared.show_ErrorHud(Localization("Please check network connection"))
            }
        })
    }
    
    
    //getDriverCurrentData
    private func API_GetUserDriverLatLng(){
        
        Hud.shared.showHudWithMsg()
        
        let userid = global.shared.getUserDataValueByKey(strKey: ApiKey.userId)
        
        let dictKeys = [
            
            "user_id" : userid,
            "booking_id" : strBookingId,
            "driver_id" : self.strDriverId
            
            ] as [String : Any]
        
        let url = Constants.AppLinks.API_BASE_URL + "\(Constants.ApiUrl.GetDriverLatLng.rawValue)"
        
        print("param",dictKeys)
        print("url",url)
        
        Alamofire.request(url , method: .post, parameters: dictKeys, headers:nil).responseJSON(queue: .global(qos: .background))
        { (response) in
            print(response)
//            Hud.shared.hideHud()
            
            do {
                
                let json: JSON = try JSON(data: response.data!)
                
                print("json ",json)
                
                if (json["statusCode"].stringValue == "200"){
                    
                    let jsonDict = json["data"].dictionaryValue
                    
                    let strStatus = jsonDict["status"]?.stringValue
                    
                    
                    switch strStatus {
                        
                    case "New":
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 10.0) {
                            DispatchQueue.global(qos: .background).async {
                                self.API_GetUserDriverLatLng()
                            }
                        }
                        
                        
                    case "Completed":
                        Hud.shared.hideHud()
                        print("Nothing to do")
                        
                    case "Driver_Declined":
                        Hud.shared.hideHud()
                        DispatchQueue.main.asyncAfter(deadline: .now() + 10.0) {
                            DispatchQueue.global(qos: .background).async {
                                self.searchDriverAPI()
                            }
                        }
                        
                        
                    case "Accepted":
                        Hud.shared.hideHud()
                        self.redirectAfterBooking()
                        
                        
                    default:
                        print("Default")
                    }
                    
                    
                }
                else{
                    // error
                    Hud.shared.show_ErrorHud(json["message"].stringValue)
                }
                
            } catch {
                // show a alert
                Hud.shared.show_ErrorHud(Localization("Please check network connection"))
            }
        }
    }
    
    
    
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}


//MARK:- TEXTVIEW DELEGATE
extension OrderDetailVC:UITextViewDelegate{
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if !lblDeliveryNotePlaceHolder.isHidden{
            lblDeliveryNotePlaceHolder.isHidden = true
            
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.text.isEmpty{
            lblDeliveryNotePlaceHolder.isHidden = false
        }
    }
    
}



//MARK:- Picker View Delegate And Datasource

extension OrderDetailVC:UIPickerViewDataSource,UIPickerViewDelegate{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrayPaymentMode.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return arrayPaymentMode[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        let mode = row == 0 ? Localization("Card") : arrayPaymentMode[row]
        self.lblPaymentMode.text = mode
        // self.pickerView.isHidden = true
    }
    
}
