//
//  CurrentOrderVC.swift
//  Gazi
//
//  Created by Apple on 12/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit


class CurrentOrderVC: UIViewController {
    
    @IBOutlet weak var mytable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nib = UINib(nibName: "CurrentOrderListTVCell", bundle: nil)
        mytable.register(nib, forCellReuseIdentifier: "CurrentOrderListTVCell")
        
        
        // Do any additional setup after loading the view.
    }
    
    
    @objc func btnRateClicked(_ sender:UIButton){
        
        let strbd = staticClass.getStoryboard_Order()
        let vc = strbd?.instantiateViewController(withIdentifier: "RatingOrderVC") as! RatingOrderVC
        vc.view.backgroundColor = UIColor(white: 0, alpha: 0.5)
        self.definesPresentationContext = true
        self.providesPresentationContextTransitionStyle = true
        vc.modalPresentationStyle = .overCurrentContext
        self.navigationController?.present(vc, animated: true, completion: nil)
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension CurrentOrderVC:UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CurrentOrderListTVCell") as! CurrentOrderListTVCell
        cell.btnRateOrder.tag = indexPath.row
        cell.btnRateOrder.addTarget(self, action: #selector(btnRateClicked(_:)), for: .touchUpInside)
        return cell
    }
}
