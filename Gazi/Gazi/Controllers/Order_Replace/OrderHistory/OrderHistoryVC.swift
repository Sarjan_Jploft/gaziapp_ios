//
//  OrderHistoryVC.swift
//  Gazi
//
//  Created by Apple on 12/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import SafariServices
import Alamofire
import SwiftyJSON
//import CarbonKit

class OrderHistoryVC: UIViewController {
    
    
    @IBOutlet weak var MyScrollView: UIScrollView!
    @IBOutlet weak var viewHead: UIView!
    @IBOutlet weak var btnCurrentOrder: UIButton!
    @IBOutlet weak var btnCanceledOrder: UIButton!
    @IBOutlet weak var btnAllOrder: UIButton!
    @IBOutlet weak var btnDelivered: UIButton!
    @IBOutlet weak var tbl: UITableView!
    
    @IBOutlet var lblOrderHistory: UILabel!
    
    
    let lang = UserDefaults.standard.string(forKey: "Applanguage") ?? "en"
    
    let btnBackColor = UIColor(red: 252/255, green: 109/255, blue: 0/255, alpha: 1)
    
    var jsonData = JSON()
    var cancelOrderId:String = ""
    var cancelReason:String = ""
    
    //    var countCurrent = Int()
    //    var countCanceled = Int()
    //    var countAll = Int()
    //    var countDelivered = Int()
    
    //    var controllersNames = ["Current","Cancel"]
    //    var carbonTapSwipeNavigation = CarbonTabSwipeNavigation()
    
    override func viewDidLoad() {
        
        initialSetUp()
        
    }
    
    
    func setUpUI(){
        
        viewHead.applyGradient(colours: [Constants.headViewTopGradient, Constants.headViewBottomGradient])
        
        changeStatusBarColor()
        
        btnCurrentOrder.layer.cornerRadius = 13
        btnCanceledOrder.layer.cornerRadius = 13
        btnAllOrder.layer.cornerRadius = 13
        btnDelivered.layer.cornerRadius = 13
        
        
        let index = UserDefaults.standard.integer(forKey: "index")
        
        if index == 2{
            
            btnCurrentOrder.isSelected = false
            btnCanceledOrder.isSelected = true
            btnAllOrder.isSelected = false
            btnDelivered.isSelected = false
            
            btnCurrentOrder.backgroundColor = .clear
            btnCanceledOrder.backgroundColor = btnBackColor
            btnAllOrder.backgroundColor = .clear
            btnDelivered.backgroundColor = .clear
            
            btnCurrentOrder.setTitleColor(.black, for: .normal)
            btnCanceledOrder.setTitleColor(.white, for: .normal)
            btnAllOrder.setTitleColor(.black, for: .normal)
            btnDelivered.setTitleColor(.black, for: .normal)
            
            if lang == "ar"{
                DispatchQueue.main.async {
                    let RightOffset = CGPoint(x: self.MyScrollView.contentSize.width - self.MyScrollView.bounds.size.width, y: 0)
                    self.MyScrollView.setContentOffset(RightOffset, animated: true)
                }
            }
            
            
            
        }else if index == 3{
            
            btnCurrentOrder.isSelected = false
            btnCanceledOrder.isSelected = false
            btnAllOrder.isSelected = false
            btnDelivered.isSelected = true
            
            
            btnCurrentOrder.backgroundColor = .clear
            btnCanceledOrder.backgroundColor = .clear
            btnAllOrder.backgroundColor = .clear
            btnDelivered.backgroundColor = btnBackColor
            
            btnCurrentOrder.setTitleColor(.black, for: .normal)
            btnCanceledOrder.setTitleColor(.black, for: .normal)
            btnAllOrder.setTitleColor(.black, for: .normal)
            btnDelivered.setTitleColor(.white, for: .normal)
            
            if lang == "en" || lang == "hi"{
                DispatchQueue.main.async {
                    let RightOffset = CGPoint(x: self.MyScrollView.contentSize.width - self.MyScrollView.bounds.size.width, y: 0)
                    self.MyScrollView.setContentOffset(RightOffset, animated: true)
                }
            }
            
            self.view.layoutIfNeeded()
            
        }else{
            
            btnCurrentOrder.backgroundColor = btnBackColor
            btnCanceledOrder.backgroundColor = .clear
            btnAllOrder.backgroundColor = .clear
            btnDelivered.backgroundColor = .clear
            
            btnCurrentOrder.setTitleColor(.white, for: .normal)
            btnCanceledOrder.setTitleColor(.black, for: .normal)
            btnAllOrder.setTitleColor(.black, for: .normal)
            btnDelivered.setTitleColor(.black, for: .normal)
            
            btnCurrentOrder.isSelected = true
            btnCanceledOrder.isSelected = false
            btnAllOrder.isSelected = false
            btnDelivered.isSelected = false
            
            if lang == "ar"{
                DispatchQueue.main.async {
                    let RightOffset = CGPoint(x: self.MyScrollView.contentSize.width - self.MyScrollView.bounds.size.width, y: 0)
                    self.MyScrollView.setContentOffset(RightOffset, animated: true)
                }
            }
            
            
        }
        
        
        lblOrderHistory.text = Localization("ORDERS HISTORY")
        btnCurrentOrder.setTitle(Localization("Current Order"), for: .normal)
        btnCanceledOrder.setTitle(Localization("Canceled Order"), for: .normal)
        btnAllOrder.setTitle(Localization("All Orders"), for: .normal)
        btnDelivered.setTitle(Localization("Delivered"), for: .normal)
        
        self.view.layoutIfNeeded()
        
    }
    
    func initialSetUp(){
        
        setUpUI()
        configureRefreshControl()
        
        let nib1 = UINib(nibName: "CurrentOrderListTVCell", bundle: nil)
        tbl.register(nib1, forCellReuseIdentifier: "CurrentOrderListTVCell")
        
        let nib2 = UINib(nibName: "CanceledOrderedTVCell", bundle: nil)
        tbl.register(nib2, forCellReuseIdentifier: "CanceledOrderedTVCell")
        
        let nib3 = UINib(nibName: "DeliveredTVCell", bundle: nil)
        tbl.register(nib3, forCellReuseIdentifier: "DeliveredTVCell")
        
        let nib4 = UINib(nibName: "All_CurrentOrderTVCell", bundle: nil)
        tbl.register(nib4, forCellReuseIdentifier: "All_CurrentOrderTVCell")
        
        let nib5 = UINib(nibName: "All_canceledTVCell", bundle: nil)
        tbl.register(nib5, forCellReuseIdentifier: "All_canceledTVCell")
        
        let nib6 = UINib(nibName: "All_DeliveredTVCell", bundle: nil)
        tbl.register(nib6, forCellReuseIdentifier: "All_DeliveredTVCell")
        
        
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(handleRLGesture))
        swipeLeft.direction = .left
        tbl.addGestureRecognizer(swipeLeft)
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(handleRLGesture))
        swipeRight.direction = .right
        tbl.addGestureRecognizer(swipeRight)
        
        
        API_GetMyOrderList()
        
    }
    
    
    func configureRefreshControl () {
        
        tbl.refreshControl = UIRefreshControl()
        tbl.refreshControl?.addTarget(self, action:
            #selector(handleRefreshControl),
                                      for: .valueChanged)
    }
    
    
    
    @objc func handleRefreshControl() {
        
        API_GetMyOrderList()
        DispatchQueue.main.async {
            self.tbl.refreshControl?.endRefreshing()
        }
    }
    
    
    @objc func handleRLGesture(gesture: UISwipeGestureRecognizer) -> Void{
        
        if gesture.direction == UISwipeGestureRecognizer.Direction.left{
            print("swipe left")
            
            if self.btnCurrentOrder.isSelected{
                
                btnCanceledOrederClicked(nil)
                
            }else if self.btnCanceledOrder.isSelected{
                
                btnAllOrderClicked(nil)
                
            }else if self.btnAllOrder.isSelected{
                
                btnDeliveredClicked(nil)
                
            }else{
                
                
                
            }
            
            
        }else if gesture.direction == UISwipeGestureRecognizer.Direction.right{
            print("swipe right")
            
            if self.btnCurrentOrder.isSelected{
                
                
            }else if self.btnCanceledOrder.isSelected{
                
                btnCurrentOrderClicked(nil)
                
            }else if self.btnAllOrder.isSelected{
                
                btnCanceledOrederClicked(nil)
                
            }
            else{
                
                btnAllOrderClicked(nil)
                
            }
            
        }
        
    }
    
    
    
    
    
    
    
    
    //    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
    //
    //        guard let storyboard = staticClass.getStoryboard_Order() else{
    //            return UIViewController()
    //        }
    //        if index == 0{
    //            return storyboard.instantiateViewController(withIdentifier: "CurrentOrderVC") as! CurrentOrderVC
    //        }else{
    //            return storyboard.instantiateViewController(withIdentifier: "CanceledOrderVC") as! CanceledOrderVC
    //        }
    //    }
    
    
    //    private func setupView() {
    //
    //        setupSegmentedControl()
    //        updateView()
    //
    //    }
    //
    
    //    private func setupSegmentedControl() {
    //        // Configure Segmented Control
    //        segmentedControl.removeAllSegments()
    //        segmentedControl.insertSegment(withTitle: "Summary", at: 0, animated: false)
    //        segmentedControl.insertSegment(withTitle: "Sessions", at: 1, animated: false)
    //        segmentedControl.addTarget(self, action: #selector(selectionDidChange(_:)), for: .valueChanged)
    //
    //        // Select First Segment
    //        segmentedControl.selectedSegmentIndex = 0
    //    }
    //
    
    //    func selectionDidChange(_ sender: UISegmentedControl) {
    //        updateView()
    //    }
    //
    //    //Adding a Child View Controller
    //    private lazy var summaryViewController: SummaryViewController = {
    //        // Load Storyboard
    //        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
    //
    //        // Instantiate View Controller
    //        var viewController = storyboard.instantiateViewController(withIdentifier: "SummaryViewController") as! SummaryViewController
    //
    //        // Add View Controller as Child View Controller
    //        self.add(asChildViewController: viewController)
    //
    //        return viewController
    //    }()
    //
    //
    //    private lazy var sessionsViewController: SessionsViewController = {
    //        // Load Storyboard
    //        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
    //
    //        // Instantiate View Controller
    //        var viewController = storyboard.instantiateViewController(withIdentifier: "SessionsViewController") as! SessionsViewController
    //
    //        // Add View Controller as Child View Controller
    //        self.add(asChildViewController: viewController)
    //
    //        return viewController
    //    }()
    //
    //
    //    private func add(asChildViewController viewController: UIViewController) {
    //        // Add Child View Controller
    //        addChild(viewController)
    //
    //        // Add Child View as Subview
    //        view.addSubview(viewController.view)
    //
    //        // Configure Child View
    //        viewController.view.frame = view.bounds
    //        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    //
    //        // Notify Child View Controller
    //        viewController.didMove(toParent: self)
    //    }
    //
    //
    //    //Removing a Child View Controller
    //    private func remove(asChildViewController viewController: UIViewController) {
    //        // Notify Child View Controller
    //        viewController.willMove(toParent: nil)
    //
    //        // Remove Child View From Superview
    //        viewController.view.removeFromSuperview()
    //
    //        // Notify Child View Controller
    //        viewController.removeFromParent()
    //    }
    //
    //    private func updateView() {
    //
    //        if btnCurrentOrder.isSelected{
    //
    //            remove(asChildViewController: sessionsViewController)
    //            add(asChildViewController: summaryViewController)
    //
    //        }else if btnAllOrder.isSelected{
    //
    //            remove(asChildViewController: sessionsViewController)
    //            add(asChildViewController: summaryViewController)
    //
    //        }else if btnCanceledOrder.isSelected{
    //
    //            remove(asChildViewController: sessionsViewController)
    //            add(asChildViewController: summaryViewController)
    //
    //        }
    //
    //    }
    //
    //
    //
    //    fileprivate func add(asChildViewController viewController: UIViewController) {
    //        // Add Child View as Subview
    //        view.addSubview(viewController.view)
    //
    //        // Configure Child View
    //        viewController.view.frame = view.bounds
    //        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    //    }
    //
    //
    
    
    //MARK:- ACTION
    
    @IBAction func btnBackClicked(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func btnCurrentOrderClicked(_ sender: UIButton?) {
        
        btnCurrentOrder.isSelected = true
        btnCanceledOrder.isSelected = false
        btnAllOrder.isSelected = false
        btnDelivered.isSelected = false
        
        btnCurrentOrder.backgroundColor = btnBackColor
        btnCanceledOrder.backgroundColor = .clear
        btnAllOrder.backgroundColor = .clear
        btnDelivered.backgroundColor = .clear
        
        btnCurrentOrder.setTitleColor(.white, for: .normal)
        btnCanceledOrder.setTitleColor(.black, for: .normal)
        btnAllOrder.setTitleColor(.black, for: .normal)
        btnDelivered.setTitleColor(.black, for: .normal)
        
        let RightOffset = CGPoint(x: 0, y: 0)
        MyScrollView.setContentOffset(RightOffset, animated: true)
        
        self.tbl.reloadData()
    }
    
    
    @IBAction func btnCanceledOrederClicked(_ sender: UIButton?) {
        
        btnCurrentOrder.isSelected = false
        btnCanceledOrder.isSelected = true
        btnAllOrder.isSelected = false
        btnDelivered.isSelected = false
        
        
        btnCurrentOrder.backgroundColor = .clear
        btnCanceledOrder.backgroundColor = btnBackColor
        btnAllOrder.backgroundColor = .clear
        btnDelivered.backgroundColor = .clear
        
        btnCurrentOrder.setTitleColor(.black, for: .normal)
        btnCanceledOrder.setTitleColor(.white, for: .normal)
        btnAllOrder.setTitleColor(.black, for: .normal)
        btnDelivered.setTitleColor(.black, for: .normal)
        
        let RightOffset = CGPoint(x: 0, y: 0)
        MyScrollView.setContentOffset(RightOffset, animated: true)
        
        self.tbl.reloadData()
    }
    
    
    @IBAction func btnAllOrderClicked(_ sender: UIButton?) {
        
        btnCurrentOrder.isSelected = false
        btnCanceledOrder.isSelected = false
        btnAllOrder.isSelected = true
        btnDelivered.isSelected = false
        
        btnCurrentOrder.backgroundColor = .clear
        btnCanceledOrder.backgroundColor = .clear
        btnAllOrder.backgroundColor = btnBackColor
        btnDelivered.backgroundColor = .clear
        
        btnCurrentOrder.setTitleColor(.black, for: .normal)
        btnCanceledOrder.setTitleColor(.black, for: .normal)
        btnAllOrder.setTitleColor(.white, for: .normal)
        btnDelivered.setTitleColor(.black, for: .normal)
        
        let RightOffset = CGPoint(x: 0, y: 0)
        MyScrollView.setContentOffset(RightOffset, animated: true)
        
        self.tbl.reloadData()
        
    }
    
    @IBAction func btnDeliveredClicked(_ sender: UIButton?) {
        
        btnCurrentOrder.isSelected = false
        btnCanceledOrder.isSelected = false
        btnAllOrder.isSelected = false
        btnDelivered.isSelected = true
        
        btnCurrentOrder.backgroundColor = .clear
        btnCanceledOrder.backgroundColor = .clear
        btnAllOrder.backgroundColor = .clear
        btnDelivered.backgroundColor = btnBackColor
        
        btnCurrentOrder.setTitleColor(.black, for: .normal)
        btnCanceledOrder.setTitleColor(.black, for: .normal)
        btnAllOrder.setTitleColor(.black, for: .normal)
        btnDelivered.setTitleColor(.white, for: .normal)
        
        let RightOffset = CGPoint(x: MyScrollView.contentSize.width - MyScrollView.bounds.size.width, y: 0)
        MyScrollView.setContentOffset(RightOffset, animated: true)
        
        self.tbl.reloadData()
        
    }
    
    @objc func btnRateDeliveredClicked(_ sender:UIButton){
        
        let strbd = staticClass.getStoryboard_Order()
        let vc = strbd?.instantiateViewController(withIdentifier: "RatingOrderVC") as! RatingOrderVC
        vc.view.backgroundColor = UIColor(white: 0, alpha: 0.5)
        vc.orderId = jsonData["delivered"][sender.tag]["orderId"].stringValue
        vc.delegate = self
        self.definesPresentationContext = true
        self.providesPresentationContextTransitionStyle = true
        vc.modalPresentationStyle = .overCurrentContext
        self.navigationController?.present(vc, animated: true, completion: nil)
        
    }
    
    @objc func btnRateInAllDeleiveredClicked(_ sender:UIButton){
        
        let strbd = staticClass.getStoryboard_Order()
        let vc = strbd?.instantiateViewController(withIdentifier: "RatingOrderVC") as! RatingOrderVC
        vc.view.backgroundColor = UIColor(white: 0, alpha: 0.5)
        vc.orderId = jsonData["All"][sender.tag]["orderId"].stringValue
        vc.delegate = self
        self.definesPresentationContext = true
        self.providesPresentationContextTransitionStyle = true
        vc.modalPresentationStyle = .overCurrentContext
        self.navigationController?.present(vc, animated: true, completion: nil)
        
    }
    
    @objc func btnTrackCurrentOrder(_ sender:UIButton){
        
        let strbd = staticClass.getStoryboard_Order()
        let vc = strbd?.instantiateViewController(withIdentifier: "TrackOrderVC") as! TrackOrderVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func btnTrackCurrentInAllOrder(_ sender:UIButton){
        
        let strbd = staticClass.getStoryboard_Order()
        let vc = strbd?.instantiateViewController(withIdentifier: "TrackOrderVC") as! TrackOrderVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func btnCancelCurrentOrderClicked(sender:UIButton){
        
        self.cancelOrderId = self.jsonData["current"][sender.tag]["orderId"].stringValue
        
        
        let alert = UIAlertController(title: Localization("Alert"), message:Localization("Are you sure want to cancel this order?") , preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: Localization("Yes"), style: .default, handler: { (action) in
            
            self.openCancelReasonVC(driverId: self.jsonData["current"][sender.tag]["driverId"].stringValue)
            //            self.API_CancelOrder(self.jsonData["current"][sender.tag]["orderId"].stringValue)
        }))
        alert.addAction(UIAlertAction(title: Localization("No"), style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
        
    }
    @objc func btnCancelInAllOrderClicked(sender:UIButton){
        
        self.cancelOrderId = self.jsonData["All"][sender.tag]["orderId"].stringValue
        
        
        
        let alert = UIAlertController(title: Localization("Alert"), message: Localization("Are you sure want to cancel this order?"), preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: Localization("Yes"), style: .default, handler: { (action) in
            //            self.API_CancelOrder(self.jsonData["All"][sender.tag]["orderId"].stringValue)
            self.openCancelReasonVC(driverId: self.jsonData["All"][sender.tag]["driverId"].stringValue)
        }))
        alert.addAction(UIAlertAction(title: Localization("No"), style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    
    
    @objc func btnOpenCurrentOrderClicked(sender:UIButton){
        
        methodToRedirectOrderDetail(jsonData["current"][sender.tag]["orderId"].stringValue, jsonData["current"][sender.tag]["id"].stringValue)
    }
    
    @objc func btnOpenCanceledOrderClicked(sender:UIButton){
        methodToRedirectOrderDetail(jsonData["cancled"][sender.tag]["orderId"].stringValue, jsonData["cancled"][sender.tag]["id"].stringValue)
        
    }
    
    @objc func btnOpenAllOrderClicked(sender:UIButton){
        methodToRedirectOrderDetail(jsonData["All"][sender.tag]["orderId"].stringValue, jsonData["All"][sender.tag]["id"].stringValue)
    }
    
    @objc func btnOpenDeliveredOrderClicked(sender:UIButton){
        methodToRedirectOrderDetail(jsonData["delivered"][sender.tag]["orderId"].stringValue, jsonData["delivered"][sender.tag]["id"].stringValue)
    }
    
    
    //    @objc func btnInvoiceCurrentOrderClicked(sender:UIButton){
    ////        methodToRedirectInvoice()
    //    }
    //    @objc func btnInvoiceCanceledOrderClicked(sender:UIButton){
    //        methodToRedirectInvoice()
    //    }
    @objc func btnInvoiceAllOrderClicked(sender:UIButton){
        methodToRedirectInvoice(jsonData["All"][sender.tag]["pdf"].stringValue)
    }
    @objc func btnInvoiceDeliveredOrderClicked(sender:UIButton){
        methodToRedirectInvoice(jsonData["delivered"][sender.tag]["pdf"].stringValue)
    }
    
    
    func methodToRedirectOrderDetail(_ orderId:String, _ productId:String){
        
        let strbd = staticClass.getStoryboard_Order()
        let vc = strbd?.instantiateViewController(withIdentifier: "OrderedDetailListVC") as! OrderedDetailListVC
        vc.strOrderId = orderId
        vc.strProductId = productId
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func methodToRedirectInvoice(_ urlString:String){
        
        // let strbd = staticClass.getStoryboard_Order()
        //let vc = strbd?.instantiateViewController(withIdentifier: "OrderedDetailListVC") as! OrderedDetailListVC
        //self.navigationController?.pushViewController(vc, animated: true)
        if let url = URL(string: urlString) {
            let config = SFSafariViewController.Configuration()
            config.entersReaderIfAvailable = true
            
            let vc = SFSafariViewController(url: url, configuration: config)
            present(vc, animated: true)
        }
    }
    
    
    func openCancelReasonVC(driverId:String){
        
        let strbd = staticClass.getStoryboard_Order()
        let vc = strbd?.instantiateViewController(withIdentifier: "CancelReasonVC") as! CancelReasonVC
        vc.driverId = driverId
        vc.delegate = self
        vc.view.backgroundColor = UIColor(white: 0, alpha: 0.5)
        self.definesPresentationContext = true
        self.providesPresentationContextTransitionStyle = true
        vc.modalPresentationStyle = .overCurrentContext
        self.navigationController?.present(vc, animated: true, completion: nil)
        
    }
    
    
    //MARK:- API Call
    
    private func API_CancelOrder(_ id:String){
        
        Hud.shared.showHudWithMsg()
        
        let userid = global.shared.getUserDataValueByKey(strKey: ApiKey.userId)
        
        let dictKeys = [
            
            "user_id" : userid,
            "OrderID" : id,
            "rid": self.cancelReason
            
            ] as [String : Any]
        
        let url = Constants.AppLinks.API_BASE_URL + "\(Constants.ApiUrl.CancalOrder.rawValue)"
        
        
        Alamofire.request(url , method: .post, parameters: dictKeys, headers:nil).responseJSON(queue: .global(qos: .background))
        { (response) in
            print(response)
            
            Hud.shared.hideHud()
            
            do {
                let json: JSON = try JSON(data: response.data!)
                
                if (json["statusCode"].stringValue == "200"){
                    
                    //                    self.jsonArray = json["data"].arrayValue
                    Hud.shared.show_SuccessHud(Localization("Order canceled successfully"))
                    self.API_GetMyOrderList()
                    
                }
                else{
                    // error
                    Hud.shared.show_ErrorHud(json["message"].stringValue)
                }
                
            } catch {
                // show a alert
                Hud.shared.show_ErrorHud(Localization("Please check network connection"))
            }
        }
    }
    
    
    
    
    
    
    private func API_GetMyOrderList(){
        
        Hud.shared.showHudWithMsg()
        
        let userid = global.shared.getUserDataValueByKey(strKey: ApiKey.userId)
        
        let dictKeys = [
            
            "user_id" : userid
            
            ] as [String : Any]
        
        let url = Constants.AppLinks.API_BASE_URL + "\(Constants.ApiUrl.MyOrders.rawValue)"
        
        
        Alamofire.request(url , method: .post, parameters: dictKeys, headers:nil).responseJSON(queue: .global(qos: .background))
        { (response) in
            print(response)
            Hud.shared.hideHud()
            
            do {
                let json: JSON = try JSON(data: response.data!)
                
                if (json["statusCode"].stringValue == "200"){
                    
                    self.jsonData = json["data"]
                    /*
                     self.countCurrent = self.jsonData["current"].count
                     self.countCanceled = self.jsonData["cancled"].count
                     self.countAll = self.jsonData["All"].count
                     self.countDelivered = self.jsonData["delivered"].count
                     */
                    DispatchQueue.main.async {
                        self.tbl.reloadData()
                    }
                    
                }
                else{
                    // error
                    Hud.shared.show_ErrorHud(json["message"].stringValue)
                }
                
            } catch {
                // show a alert
                Hud.shared.show_ErrorHud(Localization("Please check network connection"))
            }
        }
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}


extension OrderHistoryVC:UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if btnCurrentOrder.isSelected{
            tbl.restore()
            print("current lines are",jsonData["current"].count )
            return jsonData["current"].count
        }else if btnCanceledOrder.isSelected{
            tbl.restore()
            return jsonData["cancled"].count
        }else if btnDelivered.isSelected{
            tbl.restore()
            return jsonData["delivered"].count
        }else{
            return jsonData["All"].count
            //tbl.setEmptyView(title: "No Data Found", message: "")
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if btnCurrentOrder.isSelected{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "CurrentOrderListTVCell") as! CurrentOrderListTVCell
            
            
            //            cell.btnTrackOrder.tag = countCurrent - (indexPath.row + 1)
            cell.btnTrackOrder.tag = indexPath.row
            cell.btnTrackOrder.addTarget(self, action: #selector(btnTrackCurrentOrder(_:)), for: .touchUpInside)
            
            //            cell.btnCancel.tag = countCurrent - (indexPath.row + 1)
            cell.btnCancel.tag = indexPath.row
            cell.btnCancel.addTarget(self, action: #selector(btnCancelCurrentOrderClicked(sender:)), for: .touchUpInside)
            
            //            cell.btnOpenOrder.tag = countCurrent - (indexPath.row + 1)
            cell.btnOpenOrder.tag = indexPath.row
            cell.btnOpenOrder.addTarget(self, action: #selector(btnOpenCurrentOrderClicked(sender:)), for: .touchUpInside)
            
            //            cell.btnInvoice.tag = countCurrent - (indexPath.row + 1)
            //            cell.btnInvoice.tag = indexPath.row
            //            cell.btnInvoice.addTarget(self, action: #selector(btnInvoiceCurrentOrderClicked(sender:)), for: .touchUpInside)
            
            
            
            //            cell.lblOrderId.text = jsonData["current"][countCurrent - (indexPath.row + 1)]["orderId"].stringValue
            //            cell.lblOrderNumber.text = jsonData["current"][countCurrent - (indexPath.row + 1)]["orderNUmber"].stringValue
            
            
            cell.lblOrderId.text = jsonData["current"][indexPath.row]["orderId"].stringValue
            cell.lblOrderNumber.text = jsonData["current"][indexPath.row]["orderNUmber"].stringValue
            
            
            
            return cell
            
            
            
        }else if btnCanceledOrder.isSelected{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "CanceledOrderedTVCell") as! CanceledOrderedTVCell
            
            
            cell.btnOpenOrder.tag = indexPath.row
            cell.btnOpenOrder.addTarget(self, action: #selector(btnOpenCanceledOrderClicked(sender:)), for: .touchUpInside)
            
            //            cell.btnInvoice.tag = indexPath.row
            //            cell.btnInvoice.addTarget(self, action: #selector(btnInvoiceCanceledOrderClicked(sender:)), for: .touchUpInside)
            
            cell.lblOrderId.text = jsonData["cancled"][indexPath.row]["orderId"].stringValue
            cell.lblOrderNumber.text = jsonData["cancled"][indexPath.row]["orderNUmber"].stringValue
            
            
            return cell
            
        }else if btnDelivered.isSelected{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "DeliveredTVCell") as! DeliveredTVCell
            
            
            cell.btnOpenOrder.tag = indexPath.row
            cell.btnOpenOrder.addTarget(self, action: #selector(btnOpenDeliveredOrderClicked(sender:)), for: .touchUpInside)
            
            cell.btnInvoice.tag = indexPath.row
            cell.btnInvoice.addTarget(self, action: #selector(btnInvoiceDeliveredOrderClicked(sender:)), for: .touchUpInside)
            
            cell.btnRateOrder.tag = indexPath.row
            cell.btnRateOrder.addTarget(self, action: #selector(btnRateDeliveredClicked(_:)), for: .touchUpInside)
            
            cell.lblOrderId.text = jsonData["delivered"][indexPath.row]["orderId"].stringValue
            cell.lblOrderNumber.text = jsonData["delivered"][indexPath.row]["orderNUmber"].stringValue
            
            if jsonData["delivered"][indexPath.row]["isRated"].stringValue == "0"{
                cell.btnRateOrder.isHidden = false
                cell.viewRating.isHidden = true
            }else{
                cell.btnRateOrder.isHidden = true
                cell.viewRating.isHidden = false
                cell.viewRating.rating = jsonData["delivered"][indexPath.row]["rating"].doubleValue
            }
            
            return cell
            
        }else{
            
            // delievered 1 , canceled  3,  current 2
            
            let status = jsonData["All"][indexPath.row]["status"].stringValue
            
            if status == "2"{
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "All_CurrentOrderTVCell") as! All_CurrentOrderTVCell
                
                cell.btnTrackOrder.tag = indexPath.row
                cell.btnTrackOrder.addTarget(self, action: #selector(btnTrackCurrentInAllOrder(_:)), for: .touchUpInside)
                
                cell.btnCancel.tag = indexPath.row
                cell.btnCancel.addTarget(self, action: #selector(btnCancelInAllOrderClicked(sender:)), for: .touchUpInside)
                
                cell.btnOpenOrder.tag = indexPath.row
                cell.btnOpenOrder.addTarget(self, action: #selector(btnOpenAllOrderClicked(sender:)), for: .touchUpInside)
                
                //                cell.btnInvoice.tag = indexPath.row
                //                cell.btnInvoice.addTarget(self, action: #selector(btnInvoiceAllOrderClicked(sender:)), for: .touchUpInside)
                
                cell.lblOrderId.text = jsonData["All"][indexPath.row]["orderId"].stringValue
                cell.lblOrderNumber.text = jsonData["All"][indexPath.row]["orderNUmber"].stringValue
                
                
                return cell
                
            }else if status == "1"{
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "All_DeliveredTVCell") as! All_DeliveredTVCell
                
                cell.btnOpenOrder.tag = indexPath.row
                cell.btnOpenOrder.addTarget(self, action: #selector(btnOpenAllOrderClicked(sender:)), for: .touchUpInside)
                
                cell.btnInvoice.tag = indexPath.row
                cell.btnInvoice.addTarget(self, action: #selector(btnInvoiceAllOrderClicked(sender:)), for: .touchUpInside)
                
                cell.btnRateOrder.tag = indexPath.row
                cell.btnRateOrder.addTarget(self, action: #selector(btnRateInAllDeleiveredClicked(_:)), for: .touchUpInside)
                
                cell.lblOrderId.text = jsonData["All"][indexPath.row]["orderId"].stringValue
                cell.lblOrderNumber.text = jsonData["All"][indexPath.row]["orderNUmber"].stringValue
                
                
                if jsonData["All"][indexPath.row]["isRated"].stringValue == "0"{
                    cell.btnRateOrder.isHidden = false
                    cell.viewRating.isHidden = true
                }else{
                    cell.btnRateOrder.isHidden = true
                    cell.viewRating.isHidden = false
                    cell.viewRating.rating = jsonData["delivered"][indexPath.row]["rating"].doubleValue
                }
                
                return cell
                
            }else{
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "All_canceledTVCell") as! All_canceledTVCell
                
                cell.btnOpenOrder.tag = indexPath.row
                cell.btnOpenOrder.addTarget(self, action: #selector(btnOpenAllOrderClicked(sender:)), for: .touchUpInside)
                
                //                cell.btnInvoice.tag = indexPath.row
                //                cell.btnInvoice.addTarget(self, action: #selector(btnInvoiceAllOrderClicked(sender:)), for: .touchUpInside)
                
                cell.lblOrderId.text = jsonData["All"][indexPath.row]["orderId"].stringValue
                cell.lblOrderNumber.text = jsonData["All"][indexPath.row]["orderNUmber"].stringValue
                
                return cell
            }
            
            
        }
        
        
        
    }
    
    //    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    //        let strbd = staticClass.getStoryboard_Order()
    //        let vc = strbd?.instantiateViewController(withIdentifier: "OrderedDetailListVC") as! OrderedDetailListVC
    //        self.navigationController?.pushViewController(vc, animated: true)
    //    }
    //
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if btnCanceledOrder.isSelected{
            return 100
        }else if btnAllOrder.isSelected{
            // delievered 1 , canceled  3,  current 2
            let status = jsonData["All"][indexPath.row]["status"].stringValue
            
            if status == "2"{
                return 150
            }else if status == "1"{
                return 150
            }else{
                return 100
            }
        }
        return 150
        //        return UITableView.automaticDimension
    }
    
}


//MARK:- Update Rating
extension OrderHistoryVC:updateRating{
    
    
    func updateRating(update rating: Bool) {
        
        if rating{
            API_GetMyOrderList()
        }else{
            print("Nothing to update")
        }
    }
    
    
}


//MARK:- OrderListUpdationDelegate
extension OrderHistoryVC:OrderDetailListVCDelegate{
    
    func isUpdate(do update: Bool) {
        if update{
            self.API_GetMyOrderList()
        }else{
            print("Nothing to update")
        }
    }
    
    
}


//MARK:- Cancel Order Delegate

extension OrderHistoryVC:cancelOrderDelegate{
    
    func btnSubmitTapped(reason: String) {
        self.cancelReason = reason
        self.API_CancelOrder(self.cancelOrderId)
    }
    
    
}
