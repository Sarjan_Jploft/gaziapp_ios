//
//  OrderedDetailListVC.swift
//  Gazi
//
//  Created by Apple on 10/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

protocol OrderDetailListVCDelegate{
    func isUpdate(do update:Bool)
}


class OrderedDetailListVC: UIViewController {
    
    @IBOutlet weak var mytable: UITableView!
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet var lblOrderDetails: UILabel!
    
    
    
    var arrHead = [Localization("Order ID"),Localization("Order No."),Localization("Price"),Localization("Tax"),Localization("Delivery Fee"),Localization("Total"),Localization("Name"),Localization("Phone"),Localization("Address")]
    
    var arrValue = ["GAS2566","65847","100 AED","10 AED","10 AED","140 AED","James Smith","+971-8986563187","124, abc, city, xyx"]
    
    var strOrderId = ""
    var strProductId = ""
    var orderStatus = 0
    var isRated = 0
    var strDriverId = ""
    var cancelReason:String = ""
    
    var delegate:OrderDetailListVCDelegate?
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mytable.delegate = self
        mytable.dataSource = self
        
        viewHeader.applyGradient(colours: [Constants.headViewTopGradient, Constants.headViewBottomGradient])
        
        changeStatusBarColor()
        
        lblOrderDetails.text = Localization("ORDER DETAILS")
        
        
        
        
        API_OrderDetail()
        
        
        // Do any additional setup after loading the view.
    }
    
    
   
    
    //MARK:- ACTION
    
    @objc func btnRateThisOrderClicked(_ sender:UIButton){
        
        let strbd = staticClass.getStoryboard_Order()
        
        let vc = strbd?.instantiateViewController(withIdentifier: "RatingOrderVC") as! RatingOrderVC
        vc.view.backgroundColor = UIColor(white: 0, alpha: 0.5)
        vc.orderId = self.strOrderId
        vc.delegate = self
        self.definesPresentationContext = true
        self.providesPresentationContextTransitionStyle = true
        vc.modalPresentationStyle = .overCurrentContext
        
        self.navigationController?.present(vc, animated: true, completion: nil)
        
    }
    
    
    @objc func btnCancelOrderClicked(_ sender:UIButton){
        
//        API_CancelOrder()
        openCancelReasonVC()
    }
    
    @objc func btnTrackOrderClicked(_ sender:UIButton){
        
        let strbd = staticClass.getStoryboard_Order()
        let vc = strbd?.instantiateViewController(withIdentifier: "TrackOrderVC") as! TrackOrderVC
        vc.strBookingId = self.arrValue[0]
        vc.strDriverId = self.strDriverId
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    @IBAction func btnBackClicked(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    func openCancelReasonVC(){
        
        let strbd = staticClass.getStoryboard_Order()
        let vc = strbd?.instantiateViewController(withIdentifier: "CancelReasonVC") as! CancelReasonVC
        vc.driverId = strDriverId
        vc.delegate = self
        vc.view.backgroundColor = UIColor(white: 0, alpha: 0.5)
        self.definesPresentationContext = true
        self.providesPresentationContextTransitionStyle = true
        vc.modalPresentationStyle = .overCurrentContext
        self.navigationController?.present(vc, animated: true, completion: nil)
        
    }
    
    
    
    
    //MARK:- API CALL
    
    private func API_OrderDetail(){
        
        Hud.shared.showHudWithMsg()
        
        let userid = global.shared.getUserDataValueByKey(strKey: ApiKey.userId)
        
        let dictKeys = [
            
            "user_id" : userid,
            "orderID" : strOrderId
            
            ] as [String : Any]
        
        let url = Constants.AppLinks.API_BASE_URL + "\(Constants.ApiUrl.OrderDetail.rawValue)"
        
        
        Alamofire.request(url , method: .post, parameters: dictKeys, headers:nil).responseJSON(queue: .global(qos: .background))
        { (response) in
            print(response)
            Hud.shared.hideHud()
            
            do {
                let json: JSON = try JSON(data: response.data!)
                
                if (json["statusCode"].stringValue == "200"){
                    
                    let jsonData = json["data"]
                    
                    self.arrValue.removeAll()
                    
                    
                    self.arrValue.append(jsonData["orderId"].stringValue)
                    self.arrValue.append(jsonData["orderNUmber"].stringValue)
                    self.arrValue.append(jsonData["price"].stringValue + " AED")
                    if jsonData["coupon"].stringValue != ""
                    {
                        //self.arrHead.remove(at: 3)
                        if self.arrHead.contains(Localization("Promo Code Discount")){
                           self.arrHead.remove(at: 3)
                        }
                        self.arrValue.append(jsonData["discount"].stringValue + " AED")
                        self.arrHead.insert(Localization("Promo Code Discount"), at: 3)
                    }
                    let taxPercentage = jsonData["tax"].doubleValue
                    let taxAmount = ((taxPercentage * jsonData["price"].doubleValue) / 100)
                    self.arrValue.append(taxAmount.removeZerosFromEnd() + " AED")
                    self.arrValue.append(jsonData["deliveryfee"].stringValue + " AED")
                    self.arrValue.append(jsonData["netamount"].stringValue + " AED")
                    self.arrValue.append(jsonData["name"].stringValue.capitalized)
                    self.arrValue.append(jsonData["phone"].stringValue)
                    self.arrValue.append(jsonData["userAddress"].stringValue)
                    self.orderStatus = jsonData["status"].intValue
                    self.isRated = jsonData["isRated"].intValue
                    self.strDriverId = jsonData["DriverId"].stringValue
                    
                    DispatchQueue.main.async {
                        self.mytable.reloadData()
                    }
                    
                }
                else{
                    // error
                    Hud.shared.show_ErrorHud(json["message"].stringValue)
                }
                
            } catch {
                // show a alert
                Hud.shared.show_ErrorHud(Localization("Please check network connection"))
            }
        }
    }
    
    
    
    private func API_CancelOrder(){
        
        Hud.shared.showHudWithMsg()
        
        let userid = global.shared.getUserDataValueByKey(strKey: ApiKey.userId)
        
        let dictKeys = [
            
            "user_id" : userid,
            "OrderID" : strOrderId,
            "rid": self.cancelReason
            
            ] as [String : Any]
        
        let url = Constants.AppLinks.API_BASE_URL + "\(Constants.ApiUrl.CancalOrder.rawValue)"
        
        
        Alamofire.request(url , method: .post, parameters: dictKeys, headers:nil).responseJSON(queue: .global(qos: .background))
        { (response) in
            print(response)
            Hud.shared.hideHud()
            
            do {
                let json: JSON = try JSON(data: response.data!)
                
                if (json["statusCode"].stringValue == "200"){
                    
                    //                    self.jsonArray = json["data"].arrayValue
                    Hud.shared.show_SuccessHud(Localization("Order canceled successfully"))
                    self.delegate?.isUpdate(do: true)
                    self.API_OrderDetail()
                    
                }
                else{
                    // error
                    Hud.shared.show_ErrorHud(json["message"].stringValue)
                }
                
            } catch {
                // show a alert
                Hud.shared.show_ErrorHud(Localization("Please check network connection"))
            }
        }
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}


//MARK:- TABLEVIEW DELEGATE AND DATASOURCE

extension OrderedDetailListVC:UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0{
            return self.arrHead.count
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 0{
            
            return 40
        }
            //        else if indexPath.section == 2{
            //            return 152
            //        }
        else{
            if orderStatus == 3{
                return 0
            }
            return 100
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "OrderedDetailListTVCell") as! OrderedDetailListTVCell
            
            cell.lblHead.text = self.arrHead[indexPath.row]
            cell.lblValue.text = self.arrValue[indexPath.row]
            
            return cell
            
        }
            //        else if indexPath.section == 2{
            //
            //            let cell = tableView.dequeueReusableCell(withIdentifier: "CurrentOrderListTVCell") as! CurrentOrderListTVCell
            //
            //
            //            return cell
            //
            //        }
        else{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "orderedDetailOptionTVCell") as! orderedDetailOptionTVCell
            
            cell.btnRateThisOrder.tag = indexPath.row
            cell.btnRateThisOrder.addTarget(self, action: #selector(btnRateThisOrderClicked(_:)), for: .touchUpInside)
            
            cell.btnTrackOrder.tag = indexPath.row
            cell.btnTrackOrder.addTarget(self, action: #selector(btnTrackOrderClicked(_:)), for: .touchUpInside)
            
            cell.btnCancel.tag = indexPath.row
            cell.btnCancel.addTarget(self, action: #selector(btnCancelOrderClicked(_:)), for: .touchUpInside)
            
            // delievered 1 , canceled  3,  current 2
            
            if self.orderStatus == 1 || self.orderStatus == 3 {
                cell.viewCancel.isHidden = true
                cell.viewTrackOrder.isHidden = true
            }else if self.orderStatus == 2{
                cell.viewCancel.isHidden = false
                cell.viewTrackOrder.isHidden = false
            }
            
            if (self.orderStatus == 1) && (isRated == 0){
                cell.viewRate.isHidden = true
                cell.btnRateThisOrder.isHidden = false
                cell.viewRateContnr.isHidden = false
            }else if (self.orderStatus == 1) && (isRated == 1){
                cell.viewRate.isHidden = false
                cell.btnRateThisOrder.isHidden = true
                cell.viewRateContnr.isHidden = false
            }else if self.orderStatus != 1 {
                cell.viewRateContnr.isHidden = true
            }
            
            return cell
        }
        
    }
    
}



//MARK:- Update Rating
extension OrderedDetailListVC:updateRating{
    
    func updateRating(update rating: Bool) {
        if rating{
            API_OrderDetail()
            self.delegate?.isUpdate(do: true)
        }else{
            print("Nothing to update")
        }
    }
    
}


//MARK:- Cancel Order Delegate

extension OrderedDetailListVC:cancelOrderDelegate{
    
    func btnSubmitTapped(reason: String) {
        self.cancelReason = reason
        self.API_CancelOrder()
    }
    
    
}
