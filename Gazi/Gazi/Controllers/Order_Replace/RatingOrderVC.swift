//
//  RatingOrderVC.swift
//  Gazi
//
//  Created by Apple on 10/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Cosmos
import SwiftyJSON
import Alamofire


protocol updateRating {
    func updateRating(update rating: Bool)
}


class RatingOrderVC: UIViewController {
    
    @IBOutlet weak var viewCosmos: CosmosView!
    @IBOutlet weak var viewTextView: UIView!
    @IBOutlet weak var txtViewComment: UITextView!
    @IBOutlet weak var lblCommentPlaceHolder: UILabel!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var viewPopUp: UIView!
    @IBOutlet var lblRateThisOrder: UILabel!
    
    
    let lang = UserDefaults.standard.string(forKey: "Applanguage") ?? "en"
    
    var orderId = ""
    
    var delegate:updateRating?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUp_UI()
        txtViewComment.delegate = self
        // Do any additional setup after loading the view.
    }
    
    func setUp_UI(){
        
        DispatchQueue.main.async {
            
            self.changeStatusBarColor()
            self.addGreenGradientsAndBorder(to: self.btnSubmit)
            self.viewPopUp.layer.cornerRadius = 10
            self.viewPopUp.clipsToBounds = true
            self.viewTextView.layer.borderWidth = 1
            self.viewTextView.layer.borderColor = Constants.textfieldBordercolor
            
            
        }
        
        
        lblRateThisOrder.text = Localization("Rate this Order")
        lblCommentPlaceHolder.text = Localization("Comment...")
        btnSubmit.setTitle(Localization("Submit"), for: .normal)
        
        if lang == "ar"{
            txtViewComment.textAlignment = .right
        }else{
            txtViewComment.textAlignment = .left
        }
        
    }
    
    
    
    //MARK:- ACTION
    
    @IBAction func btnCrossClciked(_ sender: UIButton) {
        delegate?.updateRating(update: false)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnSubmitClicked(_ sender: UIButton) {
        
        API_RateOrder()
        
//        self.removeChld()
        
    }
    
    //MARK:- API Call
    
    private func API_RateOrder(){
        
        Hud.shared.showHudWithMsg()
        
        let userid = global.shared.getUserDataValueByKey(strKey: ApiKey.userId)
        
        txtViewComment.text = txtViewComment.text.trimmingCharacters(in: .whitespacesAndNewlines)
        
        let dictKeys = [
            
            "userId" : userid,
            "star" : viewCosmos.rating,
            "text" : txtViewComment.text!,
            "orderId" : orderId,
            
            ] as [String : Any]
        
        let url = Constants.AppLinks.API_BASE_URL + "\(Constants.ApiUrl.Rating.rawValue)"
        
        
        Alamofire.request(url , method: .post, parameters: dictKeys, headers:nil).responseJSON(queue: .global(qos: .background))
        { (response) in
            print(response)
            Hud.shared.hideHud()
            
            do {
                let json: JSON = try JSON(data: response.data!)
                if (json["statusCode"].stringValue == "200"){
//                    let jsonData = json["data"]
                    
                    Hud.shared.show_SuccessHud(Localization("Product rated successfully"))
                    self.delegate?.updateRating(update: true)
                    DispatchQueue.main.async {
                        self.dismiss(animated: true, completion: nil)
                    }
                    
                    
                }
                else{
                    // error
                    Hud.shared.show_ErrorHud(json["message"].stringValue)
                    
                }
                
            } catch {
                // show a alert
                Hud.shared.show_ErrorHud(Localization("Please check network connection"))
            }
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}


//MARK:- TEXTVIEW DELEGATE
extension RatingOrderVC:UITextViewDelegate{
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if !lblCommentPlaceHolder.isHidden{
            lblCommentPlaceHolder.isHidden = true
            
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.text.isEmpty{
            lblCommentPlaceHolder.isHidden = false
        }
    }
    
}
