//
//  ReplacementVC.swift
//  Gazi
//
//  Created by Apple on 10/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ReplacementVC: UIViewController {
    
    @IBOutlet weak var lblHead: UILabel!
    @IBOutlet weak var viewHead: UIView!
    @IBOutlet weak var btnBookNow: UIButton!
    @IBOutlet weak var lblWeightCategoryReplace: UILabel!
    @IBOutlet weak var lblQuantityReplace: UILabel!
    @IBOutlet weak var btnSelectWeightReplace: UIButton!
    @IBOutlet weak var btnSelectQntyReplace: UIButton!
    @IBOutlet weak var lblWeightCategoryNew: UILabel!
    @IBOutlet weak var lblQuantityNew: UILabel!
    @IBOutlet weak var btnSelectWeightNew: UIButton!
    @IBOutlet weak var btnSelectQntyNew: UIButton!
    @IBOutlet weak var txtAddress: UITextField!
    
    @IBOutlet weak var stackReplace: UIStackView!
    @IBOutlet weak var stackNew: UIStackView!
    @IBOutlet weak var lblReplace: UILabel!
    @IBOutlet weak var lblNew: UILabel!
    @IBOutlet var lblSelectCylinderType: UILabel!
    @IBOutlet var lblSelectQuantity: UILabel!
    @IBOutlet var lblSelectCylinderTypeNew: UILabel!
    @IBOutlet var lblSelectQuantityNew: UILabel!
    
    
    
    
    
    
    var arrayWeight = [Localization("Select Size"),"11 " + Localization("Kg"),"25 " +  Localization("Kg"),"50 " + Localization("Kg")]
    var selectedWeightIdReplace = String()
    var selectedWeightIdNew = String()
    
    var arrayQnty = [Localization("Select Quantity")]
    
    var isReplaceNewBothSelected = false
    var isReplaceOnly = false
    var isNewOnly = false
    
    var isWeightSelected = false
    var isQuantitySelected = false
    
    var isReplace = false
    var isNew = false
    
    var tax = 0.0
    var deliveryCost = 0.0
    var newCylinderPrice = 0.0
    var replacementCylinderPrice = 0.0
    
    
    var toolBar = UIToolbar()
    var picker  = UIPickerView()
    
    var jsonArray = [JSON]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUp_UI()
        API_GetCylinder()
        initialSetUp()
        // Do any additional setup after loading the view.
    }
    
    func setUp_UI(){
        
        viewHead.applyGradient(colours: [Constants.headViewTopGradient, Constants.headViewBottomGradient])
        addGreenGradientsAndBorder(to: btnBookNow)
        changeStatusBarColor()
         
        
        //UI Set as per cylinder selection on Home
        if isReplaceNewBothSelected{
            
            stackNew.isHidden = false
            stackReplace.isHidden = false
            lblReplace.isHidden = false
            lblNew.isHidden = false
            lblHead.text = Localization("REPLACEMENT/NEW")
            
        }else if isNewOnly{
            
            stackNew.isHidden = false
            lblNew.isHidden = false
            stackReplace.isHidden = true
            lblReplace.isHidden = true
            lblHead.text = Localization("NEW CYLINDER")
            
        }else if isReplaceOnly{
            
            stackNew.isHidden = true
            lblNew.isHidden = true
            stackReplace.isHidden = false
            lblReplace.isHidden = false
            lblHead.text = Localization("REPLACEMENT")
            
        }
        
        btnSelectWeightReplace.tag = 1
        btnSelectQntyReplace.tag = 1
        
        btnSelectWeightNew.tag = 2
        btnSelectQntyNew.tag = 2
        
        txtAddress.delegate = self
        addPaddingAndBorder(to: txtAddress)
        
        
        
        lblReplace.text = Localization("REPLACEMENT")
        lblSelectCylinderType.text = Localization("Select Cylinder Type")
        lblSelectQuantity.text = Localization("Select Quantity")
        lblWeightCategoryReplace.text = Localization("Select")
        lblQuantityReplace.text = Localization("Select")
        lblNew.text = Localization("NEW CYLINDER")
        lblSelectCylinderTypeNew.text = Localization("NEW CYLINDER")
        lblSelectQuantityNew.text = Localization("Select Quantity")
        
        lblWeightCategoryNew.text = Localization("Select")
        lblQuantityNew.text = Localization("Select")
        
        btnBookNow.setTitle(Localization("Book Now"), for: .normal)
        
        print("here arabic of kg", arrayWeight)
    }
    
    func initialSetUp(){
        let jsondataOptional = global.shared.getJSON(Constants.UserDefult.SaveUserData.rawValue)
        guard let jsonData = jsondataOptional else {
            return
        }
        let address = jsonData["address"].stringValue
        self.txtAddress.text = address
        
        for i in 1...100 {
            arrayQnty.append(String(i))
        }
    }
    
    //MARK:- ACTION
    
    
    @IBAction func btnBookNowClicked(_ sender: UIButton) {
        
        
        if (isReplaceOnly && lblWeightCategoryReplace.text == Localization("Select")) || (isReplaceNewBothSelected && lblWeightCategoryReplace.text == Localization("Select")){
            
            HelpingMethod.shared.presentAlertWithTitle(title: Localization("Error"), message: Localization(Constants.errorMessage.SelectCategoryReplace.rawValue), vc: self)
            
        }else if (isReplaceOnly && lblQuantityReplace.text == Localization("Select")) || (isReplaceNewBothSelected && lblQuantityReplace.text == Localization("Select")){
            
            HelpingMethod.shared.presentAlertWithTitle(title: Localization("Error"), message: Localization(Constants.errorMessage.SelectQtyReplace.rawValue), vc: self)
            
        }else if (isNewOnly && lblWeightCategoryNew.text == Localization("Select")) || (isReplaceNewBothSelected && lblWeightCategoryNew.text == Localization("Select")){
            
            HelpingMethod.shared.presentAlertWithTitle(title: Localization("Error"), message: Localization(Constants.errorMessage.SelectCategoryNew.rawValue), vc: self)
            
        }else if (isNewOnly && lblQuantityNew.text == Localization("Select")) || (isReplaceNewBothSelected && lblQuantityNew.text == Localization("Select")){
            
            HelpingMethod.shared.presentAlertWithTitle(title: Localization("Error"), message: Localization(Constants.errorMessage.SelectQtyNew.rawValue), vc: self)
            
        }else{
            
            let strbd = staticClass.getStoryboard_Order()
            let vc = strbd?.instantiateViewController(withIdentifier: "OrderDetailVC") as! OrderDetailVC
            
            vc.isNewOnly = self.isNewOnly
            vc.isReplaceOnly = self.isReplaceOnly
            vc.isReplaceNewBothSelected = self.isReplaceNewBothSelected
            vc.tax = self.tax
            vc.deliveryCost = self.deliveryCost
            vc.newCylinderPrice = self.newCylinderPrice
            vc.replacementCylinderPrice = self.replacementCylinderPrice
            vc.newCylinderQnty = (lblQuantityNew.text == Localization("Select")) ? 0 : (Int(lblQuantityNew.text ?? "0") ?? 0)
            vc.replacementCylinderQty = (lblQuantityReplace.text == Localization("Select")) ? 0 : (Int(lblQuantityReplace.text ?? "0") ?? 0)
            vc.newCylinderId = selectedWeightIdNew
            vc.ReplacedCylinderId = selectedWeightIdReplace
            
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        
        
    }
    
    @IBAction func btnBackClicked(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btnSelectWeightClicked(_ sender: UIButton) {
        
        if sender.tag == 1{
            self.isReplace = true
            self.isNew = false
        }else{
            self.isReplace = false
            self.isNew = true
        }
        
        self.isWeightSelected = true
        self.isQuantitySelected = false
        openPicker()
    }
    
    @IBAction func btnSelectQntyClicked(_ sender: UIButton) {
        
        if sender.tag == 1{
            self.isReplace = true
            self.isNew = false
        }else{
            self.isReplace = false
            self.isNew = true
        }
        
        
        self.isQuantitySelected = true
        self.isWeightSelected = false
        openPicker()
    }
    
    func openPicker(){
        toolBar.removeFromSuperview()
        picker.removeFromSuperview()
        
        self.view.endEditing(true)
        
        picker = UIPickerView.init()
        picker.delegate = self
        picker.backgroundColor = UIColor.lightGray//UIColor(named: "ThemeColor")
        picker.setValue(UIColor.black, forKey: "textColor")
        picker.autoresizingMask = .flexibleWidth
        picker.contentMode = .center
        picker.frame = CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 300, width: UIScreen.main.bounds.size.width, height: 300)
        
        toolBar = UIToolbar.init(frame: CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 300, width: UIScreen.main.bounds.size.width, height: 50))
        toolBar.barStyle = .default
        toolBar.items = [
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
            UIBarButtonItem.init(title: Localization("Done"), style: .done, target: self, action: #selector(onDoneButtonTapped))
        ]
        
        self.view.addSubview(picker)
        self.view.addSubview(toolBar)
    }
    
    
    @objc func onDoneButtonTapped() {
        toolBar.removeFromSuperview()
        picker.removeFromSuperview()
    }
    
    
    
    
    //MARK:- API Call
    
    private func API_GetCylinder(){
        
        Hud.shared.showHudWithMsg()
        
        let userid = global.shared.getUserDataValueByKey(strKey: ApiKey.userId)
        
        let dictKeys = [
            
            "user_id" : userid
            
            ] as [String : Any]
        
        let url = Constants.AppLinks.API_BASE_URL + "\(Constants.ApiUrl.CylinderCategories.rawValue)"
        
        
        Alamofire.request(url , method: .post, parameters: dictKeys, headers:nil).responseJSON(queue: .global(qos: .background))
        { (response) in
            print(response)
            Hud.shared.hideHud()
            
            do {
                let json: JSON = try JSON(data: response.data!)
                
                if (json["statusCode"].stringValue == "200"){
                    
                    self.jsonArray = json["data"].arrayValue
                    
                    self.arrayWeight.removeAll()
//                    self.arrWeightId.removeAll()
                    
                    self.arrayWeight.append(Localization("Select Size"))
                    
                    for arr in self.jsonArray{
//                        let id = arr["id"].stringValue
                        let title = arr["title"].stringValue
                        self.arrayWeight.append(title)
//                        self.arrWeightId.append(id)
                    }
                    
                    
                    self.tax = json["tax"].doubleValue
                    self.deliveryCost = json["deliveryFee"].doubleValue
//                    self.replacementCylinderPrice = json["NewCylinderPrice"].doubleValue
//                    self.newCylinderPrice = json["ReplaceCylinderPrice"].doubleValue
                    
                    
                }
                else{
                    // error
                    Hud.shared.show_ErrorHud(json["message"].stringValue)
                }
                
            } catch {
                // show a alert
                Hud.shared.show_ErrorHud(Localization("Please check network connection"))
            }
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}


//MARK:- Picker View Delegate And Datasource

extension ReplacementVC:UIPickerViewDataSource,UIPickerViewDelegate{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if self.isWeightSelected{
            return self.arrayWeight.count
        }
        return arrayQnty.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if self.isWeightSelected{
            return self.arrayWeight[row]
        }
        return arrayQnty[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if self.isReplace && self.isWeightSelected{
            let wght = row == 0 ? Localization("Select") : arrayWeight[row]
            self.lblWeightCategoryReplace.text = wght
            let id = row == 0 ? "" : self.jsonArray[row - 1]["id"].stringValue
            self.selectedWeightIdReplace = id
            let price = row == 0 ? 0.0 : self.jsonArray[row - 1]["newprice"].doubleValue
            self.replacementCylinderPrice = price
        }else if self.isReplace && self.isQuantitySelected{
            let qty = row == 0 ? Localization("Select") : arrayQnty[row]
            self.lblQuantityReplace.text = qty
        }else if self.isNew && self.isWeightSelected{
            let wght = row == 0 ? Localization("Select") : arrayWeight[row]
            self.lblWeightCategoryNew.text = wght
            let id = row == 0 ? "" : self.jsonArray[row - 1]["id"].stringValue
            self.selectedWeightIdNew = id
            let price = row == 0 ? 0.0 : self.jsonArray[row - 1]["newprice"].doubleValue
            self.newCylinderPrice = price
        }else if self.isNew && self.isQuantitySelected{
            let qty = row == 0 ? Localization("Select") : arrayQnty[row]
            self.lblQuantityNew.text = qty
        }
        
    }
    
}


extension ReplacementVC:UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
}
