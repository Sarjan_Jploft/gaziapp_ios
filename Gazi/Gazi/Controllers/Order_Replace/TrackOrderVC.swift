//
//  TrackOrderVC.swift
//  Gazi
//
//  Created by Apple on 12/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire
import GoogleMaps
import SwiftyJSON

class TrackOrderVC: UIViewController {

    @IBOutlet weak var viewHead: UIView!
    @IBOutlet weak var googleMap: GMSMapView!
    @IBOutlet var lblTrackOrder: UILabel!
    
    
    
    
    var strBookingId = ""
    var strDriverId = ""
    
    var driverCurrentLat = 0.0
    var driverCurrentLng = 0.0
    
    var dropLat = 0.0
    var dropLng = 0.0
    
    var isDriverPathCreated = false
    
    
    var polyline = GMSPolyline()
    var polylineForDriver = GMSPolyline()
    
    var driverPathCoordinatesArray = [CLLocationCoordinate2D]()
    var testPathCoordinatesArray = [CLLocationCoordinate2D]()
    
    let driverStartLocationMarker = GMSMarker()
    let driverCurrentLocationMarker = GMSMarker()
    let driverEndLocationMarker = GMSMarker()
    
    var selectedSourceLocation = CLLocationCoordinate2D()
    var selectedDestinationLocation = CLLocationCoordinate2D()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setUp_UI()
        mapSetUp()
        
        API_GetUserDriverLatLng()
        
        // Do any additional setup after loading the view.
    }
    

    func setUp_UI(){
        changeStatusBarColor()
        viewHead.applyGradient(colours: [Constants.headViewTopGradient, Constants.headViewBottomGradient])
        
        
        lblTrackOrder.text = Localization("TRACK ORDER")
    }
    
    
    func mapSetUp(){
       
        let camera = GMSCameraPosition.camera(withLatitude: 27.432782, longitude: 77.289719, zoom: 15.0)
        googleMap.camera = camera
        setMarker()
    }
    
    
    func setMarker(){
        
        
        
        // I have taken a pin image which is a custom image
        let markerImage = UIImage(named: "home-address")//!.withRenderingMode(.alwaysTemplate)
        
        //creating a marker view
        let markerView = UIImageView(image: markerImage)
        
//        marker.position = CLLocationCoordinate2D(latitude: 26.904260, longitude: 75.785670)
        
        driverEndLocationMarker.iconView = markerView
//        marker.title = "My Home"
//        marker.snippet = "Gas Agency"
        driverEndLocationMarker.map = googleMap
        
        
        //comment this line if you don't wish to put a callout bubble
        //        googleMap.selectedMarker = marker
        
        
        
        
        
        
        let markerImage1 = UIImage(named: "MarkerCarCylinder")//!.withRenderingMode(.alwaysTemplate)
        
        //creating a marker view
        let markerView1 = UIImageView(image: markerImage1)
        
//        driverCurrentLocationMarker.position = CLLocationCoordinate2D(latitude: 26.902250, longitude: 75.779130)
        driverCurrentLocationMarker.iconView = markerView1
        driverCurrentLocationMarker.map = googleMap
        
        
    }
    
    
    
    //MARK:- ACTION
    
    @IBAction func btnBackClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    //getDriverCurrentData
    private func API_GetUserDriverLatLng(){
        
//        Hud.shared.showHudWithMsg()
        
        let userid = global.shared.getUserDataValueByKey(strKey: ApiKey.userId)
        
        let dictKeys = [
            
            "user_id" : userid,
            "booking_id" : self.strBookingId,
            "driver_id" : self.strDriverId
            
            ] as [String : Any]
        
        let url = Constants.AppLinks.API_BASE_URL + "\(Constants.ApiUrl.GetDriverLatLng.rawValue)"
        
        
        Alamofire.request(url , method: .post, parameters: dictKeys, headers:nil).responseJSON(queue: .global(qos: .background))
        { (response) in
            print(response)
//            Hud.shared.hideHud()
            
            do {
                
                let json: JSON = try JSON(data: response.data!)
                
                if (json["statusCode"].stringValue == "200"){
                    
                    let jsonData = json["data"].dictionaryValue
//                    self.manageRider()
                    
                    let status = jsonData["status"]?.stringValue
                    
                    switch status {
                        
                    case "Completed":
                        print("Order has been Completed")
                    case "Accepted", "Start":
                        
                        
                        self.driverCurrentLat = jsonData["latitude"]?.doubleValue ?? 0.0
                        self.driverCurrentLng = jsonData["longlatitude"]?.doubleValue ?? 0.0
                        
                        self.dropLat = 27.432782//jsonData["droplatitude"]?.doubleValue ?? 0.0
                        self.dropLng = 77.289719//jsonData["droplonglatitude"]?.doubleValue ?? 0.0
                        
                        DispatchQueue.main.async {
                            self.manageDriverMapElements()
                        }
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 5.0) {
                            DispatchQueue.global(qos: .background).async {
                                self.API_GetUserDriverLatLng()
                            }
                        }
                        
                    default:
                        print("Default")
                        
                    }
                    
                    
                   
                }
                else{
                    // error
                    Hud.shared.show_ErrorHud(json["message"].stringValue)
                }
                
            } catch {
                // show a alert
                Hud.shared.show_ErrorHud("Please check network connection")
            }
        }
    }
    
    

    //MARK:- Create Marker On Map
    
    
    func createMarkerOnMap(imageName : String , markerName :  GMSMarker , isHide : Bool, width:Int = 50, height:Int = 50) {
        
        let markerImage = UIImage(named: imageName)!.withRenderingMode(.alwaysOriginal)
        let markerView = UIImageView(image: markerImage)
        markerView.frame = CGRect(x: 0, y: 0, width: width, height: height)
        markerName.iconView = markerView
        markerName.map = googleMap
        markerName.map?.isHidden = isHide
        googleMap.selectedMarker = markerName
    }
    
    
    
    /*
    private func getDriverCurrentData(){
        
        //        Hud.shared.showHud()
        let userID = global.userId
        
        
        let dictKeys = [ "user_id" : userID,
                         "driver_id" : AppData["driverId"] ?? "" ,//selectedDriverID,
            "booking_id" :  AppData["bookingId"] ?? ""
            ] as [String : Any]
        
        
        let url = Constants.ApiUrl.get_user_driver_latlong.rawValue
        
        Alamofire.request(url , method: .post, parameters: dictKeys, headers:nil).responseJSON(completionHandler: { (response) in
            //            Hud.shared.hideHud()
            do {
                let json: JSON = try JSON(data: response.data!)
                
                if (json["statusCode"].stringValue == "200"){
                    
                    driverCurrentData = json["data"]
                    self.manageRider()
                    
                }
                else{
                    // error
                    
                    self.showAlert(title: "Error", message: json["message"].stringValue)
                }
                
            } catch {
                // show a alert
                //               Hud.shared.hideHud()
                self.showAlert(title: "Error", message: "Please check network connection")
                
                DispatchQueue.global(qos: .background).asyncAfter(deadline: .now() + self.callBackTime * 3) {
                    self.getDriverCurrentData()
                }
            }
        })
    }
    */
    
    
    
    func manageDriverMapElements(){
        
        
        
        let lastlocation = driverCurrentLocationMarker.position
        
        let driverLat = self.driverCurrentLat
        let driverLong = self.driverCurrentLng
        
        let driverCurretLocation = CLLocationCoordinate2DMake(driverLat, driverLong)
        

        let homeLat = self.dropLat
        let homeLong = self.dropLng
        
        self.selectedSourceLocation = CLLocationCoordinate2D(latitude: homeLat, longitude: homeLong)
        driverEndLocationMarker.position = self.selectedSourceLocation
        
        
        
        
        carAnimactionAToBCordinates(a:lastlocation , b: driverCurretLocation )
        
        print("driverCurretLocation ==================================")
        print(driverCurretLocation)
        print("driver last location ==================================")
        print(lastlocation)


        let lat =  (driverLat * 10000).rounded()/10000
        let long =  (driverLong * 10000).rounded()/10000
        
        let deiverCurrenTrimedLocation = CLLocationCoordinate2DMake(lat, long)

        
            // dericer ride
            // driver go to her currinent location to user currient location
            
            // create driver Path
            if(isDriverPathCreated == false){
                
                // hide user data
                // hide user path source to destancinaction
                
                polyline.map = nil
                
                // hide user markers all markers
               
                
                // show driver markers
                driverCurrentLocationMarker.map = googleMap
                driverStartLocationMarker.map = googleMap//?.isHidden = false
                
                
                
                driverStartLocationMarker.position = driverCurretLocation
                driverEndLocationMarker.position = selectedSourceLocation
                driverCurrentLocationMarker.position = driverCurretLocation
                
                
                // create driver path
                fetchRoute(from: driverCurretLocation , to: selectedSourceLocation, isPathForUser: false)
                
            }
            
            if(driverPathCoordinatesArray.count == 0){return}
            
            var isOnPath = false
            var index = 0
            
            for i in 0..<driverPathCoordinatesArray.count{
                
                if(driverPathCoordinatesArray[i].latitude == deiverCurrenTrimedLocation.latitude && driverPathCoordinatesArray[i].longitude == deiverCurrenTrimedLocation.longitude ){
                    
                    isOnPath = true
                    index = i
                    break
                }
            }
            
            if(isOnPath){
                print("driver on path")
                
                //Creating new path from the current location to the destination
                
                let newPath = GMSMutablePath()
                
                for i in index..<Int(testPathCoordinatesArray.count){
                    newPath.add(testPathCoordinatesArray[i])
                }
                
                polylineForDriver.map = nil
                polylineForDriver = GMSPolyline(path: newPath)
                polylineForDriver.strokeColor = UIColor.red
                polylineForDriver.strokeWidth = 3.0
                polylineForDriver.map = self.googleMap
                
                
            }
            else{
                fetchRoute(from: driverCurretLocation , to: selectedSourceLocation, isPathForUser: false)
            }
        
        
    }
    
    
    //MARK:- Get New Path From Google Api
    
    
    // get path from direction api(google map api)
    func fetchRoute(from source: CLLocationCoordinate2D, to destination: CLLocationCoordinate2D , isPathForUser : Bool) {
        
        // let test: CLLocationCoordinate2D = CLLocationCoordinate2DMake(100, 100)
        
        let session = URLSession.shared
        
        let url = URL(string: "https://maps.googleapis.com/maps/api/directions/json?origin=\(source.latitude),\(source.longitude)&destination=\(destination.latitude),\(destination.longitude)&sensor=true&mode=driving&key=\(Constants.GoogleApiKey)")!
        
        
        let task = session.dataTask(with: url, completionHandler: {
            (data, response, error) in
            
            guard error == nil else {
                print(error!.localizedDescription)
                return
            }
            
            guard let jsonResult = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any] else {
                print("error in JSONSerialization")
                return
            }
            
            
            
            guard let routes = jsonResult["routes"] as? [Any] else {
                return
            }
            
            if(routes.count <= 0){
                return
            }
            
            guard let route = routes[0] as? [String: Any] else {
                return
            }
            
            guard let overview_polyline = route["overview_polyline"] as? [String: Any] else {
                return
            }
            
            guard let polyLineString = overview_polyline["points"] as? String else {
                return
            }
            
            //Call this method to draw path on map
            self.drawPath(from: polyLineString, a: source, b: destination , isPathForUser: isPathForUser)
            
        })
        task.resume()
    }
    
    
    
    //MARK:- Draw Path
    
    
    //draw path function
    func drawPath(from polyStr: String , a : CLLocationCoordinate2D,  b : CLLocationCoordinate2D , isPathForUser : Bool){
        
        DispatchQueue.main.async()
            {
                let path = GMSPath(fromEncodedPath: polyStr)
                
                self.driverStartLocationMarker.map = nil
                
                self.polyline.map = nil
                self.polylineForDriver.map = nil
                
                self.driverStartLocationMarker.position = a
                
                self.polylineForDriver = GMSPolyline(path: path)
                self.polylineForDriver.strokeWidth = 3.0
                self.polylineForDriver.strokeColor = #colorLiteral(red: 0.1764705926, green: 0.4980392158, blue: 0.7568627596, alpha: 1)
                self.polylineForDriver.map = self.googleMap
                self.isDriverPathCreated = true
                
                self.getAllPathLocationCoordinates(forUser: false)
                
        }
    }
    
    
    
    //MARK:- Get All Path Location Coordinates
    
    func getAllPathLocationCoordinates(forUser : Bool){
        
        self.testPathCoordinatesArray.removeAll()
        
        var index : UInt = 0
        
        self.driverPathCoordinatesArray.removeAll()
        
        while (  self.polylineForDriver.path?.coordinate(at: index) != nil && (self.polylineForDriver.path?.coordinate(at: index).latitude != -180.0 || self.polylineForDriver.path?.coordinate(at: index).longitude != -180.0) ) {
            
            if let coordinate = self.polylineForDriver.path?.coordinate(at: index){
                
                self.testPathCoordinatesArray.append(coordinate)
                
                let lat =  (coordinate.latitude * 10000).rounded()/10000
                let long =  (coordinate.longitude * 10000).rounded()/10000
                
                let tempC  = CLLocationCoordinate2DMake(lat, long)
                self.driverPathCoordinatesArray.append(tempC)
                index = index + 1
            }
        }
        
        
    }
    
    
    
   func carAnimactionAToBCordinates(a :CLLocationCoordinate2D , b : CLLocationCoordinate2D ){
        
        driverCurrentLocationMarker.rotation = CLLocationDegrees(getHeadingForDirection(fromCoordinate: a, toCoordinate: b))
        driverCurrentLocationMarker.map = googleMap
        
        CATransaction.begin()
        CATransaction.setAnimationDuration(10.0)
        driverCurrentLocationMarker.position = b
        CATransaction.commit()
        
        
    }
    
    func getHeadingForDirection(fromCoordinate fromLoc: CLLocationCoordinate2D, toCoordinate toLoc: CLLocationCoordinate2D) -> Float {
           
           let fLat: Float = Float((fromLoc.latitude).degreesToRadians)
           let fLng: Float = Float((fromLoc.longitude).degreesToRadians)
           let tLat: Float = Float((toLoc.latitude).degreesToRadians)
           let tLng: Float = Float((toLoc.longitude).degreesToRadians)
           let degree: Float = (atan2(sin(tLng - fLng) * cos(tLat), cos(fLat) * sin(tLat) - sin(fLat) * cos(tLat) * cos(tLng - fLng))).radiansToDegrees
           if degree >= 0 {
               return degree
           }
           else {
               return 360 + degree
           }
       }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension Int {
    var degreesToRadians: Double { return Double(self) * .pi / 180 }
}
extension FloatingPoint {
    var degreesToRadians: Self { return self * .pi / 180 }
    var radiansToDegrees: Self { return self * 180 / .pi }
}

