//
//  CardDetailVC.swift
//  Gazi
//
//  Created by Apple on 10/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class CardDetailVC: UIViewController {
    
    @IBOutlet weak var viewHead: UIView!
    @IBOutlet weak var txtCardNumber: UITextField!
    @IBOutlet weak var txtCardName: UITextField!
    @IBOutlet weak var txtCvv: UITextField!
    @IBOutlet weak var btnPay: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUp_UI()
        
        txtCardNumber.delegate = self
        txtCvv.delegate = self
        // Do any additional setup after loading the view.
    }
    
    func setUp_UI(){
        
        changeStatusBarColor()
        addGreenGradientsAndBorder(to: btnPay)
        viewHead.applyGradient(colours: [Constants.headViewTopGradient, Constants.headViewBottomGradient])
        
    }
    
    
    //MARK:- ACTION
    
    @IBAction func btnPayClicked(_ sender: UIButton) {
        
        txtCardNumber.text = txtCardNumber.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        txtCardName.text = txtCardName.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        txtCvv.text = txtCvv.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        
        
        if(validateName(txtCardName.text ?? "" ) && validateCreditCardNumber(txtCardNumber.text ?? "") && validateCVV(txtCvv.text ?? "")){
            
            Hud.shared.show_ErrorHud("Payment set up pending...")
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                
                self.navigationController?.popToRootViewController(animated: true)
            }
            
            
        }
        
        
    /*    if txtCardName.text!.isEmpty{
            HelpingMethod.shared.presentAlertWithTitle(title: "Error", message: Constants.errorMessage.CardNumberEmpty.rawValue, vc: self)
        }else if txtCardName.text!.isEmpty{
            HelpingMethod.shared.presentAlertWithTitle(title: "Error", message: Constants.errorMessage.CardNameEmpty.rawValue, vc: self)
        }else if txtCardName.text!.isEmpty{
            HelpingMethod.shared.presentAlertWithTitle(title: "Error", message: Constants.errorMessage.CVVEmpty.rawValue, vc: self)
        }else{
            
            Hud.shared.show_ErrorHud("Payment set up pending...")
            self.navigationController?.popToRootViewController(animated: true)
            
        }
        */
        
    }
    
    @IBAction func btnBackClicked(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}



//MARK:- TextField Delegate

extension CardDetailVC:UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        if(textField == txtCardNumber){
            
            if(textField.text?.count == 3 ||  textField.text?.count == 8 || textField.text?.count == 13 ){
                
                if(string == ""){
                    return true
                }
                else{
                    txtCardNumber.text = (textField.text ?? "") + string + "-"
                    return false
                }
                
            }
            
            if(textField == txtCardNumber){
                if(string == ""){
                    return true
                }
                
                if (19 <= textField.text?.count ?? 0) {
                    return false
                }
                
            }
        }else if textField == txtCvv{
            // get the current text, or use an empty string if that failed
            let currentText = textField.text ?? ""

            // attempt to read the range they are trying to change, or exit if we can't
            guard let stringRange = Range(range, in: currentText) else { return false }

            // add their new text to the existing text
            let updatedText = currentText.replacingCharacters(in: stringRange, with: string)

            // make sure the result is under 3 characters
            return updatedText.count <= 4
        }
        
        return true
    }
}
