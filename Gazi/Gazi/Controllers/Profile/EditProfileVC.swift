//
//  EditProfileVC.swift
//  Gazi
//
//  Created by Apple on 11/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SDWebImage
import CoreLocation

class EditProfileVC: UIViewController {
    
    @IBOutlet weak var viewHead: UIView!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var btnChangeProfile: UIButton!
    @IBOutlet weak var txtCurrentPassword: UITextField!
    @IBOutlet weak var txtNewPassword: UITextField!
    @IBOutlet weak var btnLogOut: UIButton!
    @IBOutlet weak var txtMobile: UITextField!
    @IBOutlet weak var txtaddress: UITextField!
    @IBOutlet var lblEditProfileHead: UILabel!
    @IBOutlet var lblLogOut: UILabel!
    @IBOutlet var lblUpdate: UILabel!
    @IBOutlet var lblChangePassword: UILabel!
    @IBOutlet var btnUpdate: UIButton!
    
    
    
    var imgPicker:ImagePicker!
    var oldPassword:String = ""
    
    
    let lang = UserDefaults.standard.string(forKey: "Applanguage") ?? "en"
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUp_UI()
        initialSetUp()
        
        // Do any additional setup after loading the view.
    }
    
    func setUp_UI(){
        
        changeStatusBarColor()
        imgProfile.layer.cornerRadius = imgProfile.frame.height/2
        viewHead.applyGradient(colours: [Constants.headViewTopGradient, Constants.headViewBottomGradient])
        
        lblEditProfileHead.text = Localization("EDIT PROFILE")
        lblChangePassword.text = Localization("Change Password")
        lblLogOut.text = Localization("Logout")
        lblUpdate.text = Localization("Update")
        btnChangeProfile.setTitle(Localization("Change Profile Picture"),for:.normal)
        
        txtMobile.placeholder = Localization("Mobile Number")
        txtaddress.placeholder = Localization("Address")
        txtCurrentPassword.placeholder = Localization("Current Password")
        txtNewPassword.placeholder = Localization("New Password")
        
        if lang == "ar"{
            
            txtMobile.textAlignment = .right
            txtaddress.textAlignment = .right
            txtCurrentPassword.textAlignment = .right
            txtNewPassword.textAlignment = .right
            
        }else{
            
            txtMobile.textAlignment = .left
            txtaddress.textAlignment = .left
            txtCurrentPassword.textAlignment = .left
            txtNewPassword.textAlignment = .left
        }
        
    }
    
    func initialSetUp(){
        
        let jsondataOptional = global.shared.getJSON(Constants.UserDefult.SaveUserData.rawValue)
        guard let jsonData = jsondataOptional else {
            return
        }
        let address = jsonData["address"].stringValue
        self.txtaddress.text = address
        
        let mobile = jsonData["mobile"].stringValue
        self.txtMobile.text = mobile
        
        imgPicker = ImagePicker(presentationController: self, delegate: self)
        
        let img = jsonData["image"].stringValue
        if let imageURL = URL(string: img) {
            imgProfile.sd_setImage(with: imageURL, placeholderImage: UIImage(named: "userB"), options: SDWebImageOptions.progressiveLoad) { (image, error, sdimagecacheType, url) in
                
            }
        }
        
        oldPassword = jsonData["temp_password"].stringValue
    }
    
    
    //MARK:- ACTION
    
    @IBAction func btnLogOutClicked(_ sender: UIButton) {
        
        let alertController1 = UIAlertController(title: Localization("Confirm"), message: Localization("Do you want to logout?"), preferredStyle: .alert)
        alertController1.addAction(UIAlertAction(title: Localization("Yes"), style: .default, handler: {(_ action1: UIAlertAction?) -> Void in
            
            global.shared.removeDataFromUserDefult()
            
            
            let strbdMain = staticClass.getStoryboard_Main()
            let initialVC = strbdMain?.instantiateViewController(withIdentifier: "LogInVC") as! LogInVC
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let nav = UINavigationController(rootViewController: initialVC)
            appDelegate.window!.rootViewController = nav
            
        }))
        alertController1.addAction(UIAlertAction(title: Localization("No"), style: .default, handler: nil))
        self.present(alertController1, animated: true) {() -> Void in }
        
    }
    
    
    @IBAction func btnChangeProfileClicked(_ sender: UIButton) {
        
        
        self.imgPicker.present(from: sender)
        
    }
    
    @IBAction func btnBackClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btnUpdateClicked(_ sender: UIButton) {
        
        txtaddress.text = txtaddress.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        txtMobile.text = txtMobile.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        txtCurrentPassword.text = txtCurrentPassword.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        txtNewPassword.text = txtNewPassword.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        
        if txtMobile.text!.isEmpty{
            HelpingMethod.shared.presentAlertWithTitle(title: Localization("Error"), message:Localization(Constants.errorMessage.MobileNoEmpty.rawValue), vc: self)
        }else if txtaddress.text!.isEmpty{
            HelpingMethod.shared.presentAlertWithTitle(title: Localization("Error"), message:Localization(Constants.errorMessage.AddressEmpty.rawValue), vc: self)
        }else if (txtNewPassword.text!.isEmpty) && !(txtCurrentPassword.text!.isEmpty){
            HelpingMethod.shared.presentAlertWithTitle(title: Localization("Error"), message:Localization(Constants.errorMessage.newPasswordInvalid.rawValue), vc: self)
        }else if !(txtNewPassword.text!.isEmpty) && (txtCurrentPassword.text!.isEmpty){
            HelpingMethod.shared.presentAlertWithTitle(title: Localization("Error"), message:Localization(Constants.errorMessage.CurrentPasswordEmpty.rawValue), vc: self)
        }else if !(txtNewPassword.text!.isEmpty) && (txtCurrentPassword.text != oldPassword){
            HelpingMethod.shared.presentAlertWithTitle(title: Localization("Error"), message:Localization(Constants.errorMessage.CurrentPasswordInvalid.rawValue), vc: self)
        }else if !(txtNewPassword.text!.isEmpty) && (txtNewPassword.text!.isEmpty){
            HelpingMethod.shared.presentAlertWithTitle(title: Localization("Error"), message:Localization(Constants.errorMessage.newPasswordInvalid.rawValue), vc: self)
        }else{
            
            getLocation(from: txtaddress.text!) { (location) in
             
                if location == nil{
                    HelpingMethod.shared.presentAlertWithTitle(title: Localization("Error"), message: Localization("Unable to find Address"), vc: self)
                }else{
                    let lat = location?.latitude ?? 0.0
                    let lng = location?.longitude ?? 0.0
                    print(lat)
                    print(lng)
                    self.API_EditProfile(lat, lng)
                }
           }
            
            
            //API_EditProfile()
            
        }
        
    }
    
    
    
    
    
    //MARK:- API CALL
    
    private func API_EditProfile(_ lat:CLLocationDegrees, _ lng:CLLocationDegrees){
        
        Hud.shared.showHudWithMsg()
        
        let userid = global.shared.getUserDataValueByKey(strKey: ApiKey.userId)
        
        let dictKeys = [
            
            "user_id" : userid,
            "address":txtaddress.text!,
            "mobile":txtMobile.text!,
            "password":txtNewPassword.text!,
            "lat":lat,
            "lng":lng
            
            ] as [String : Any]
        
        //fname, lname
        
        
        let imgData = imgProfile.image?.pngData()
        
        
        ApiLibrary.shared.imageUpload(postDictionary: dictKeys, strApiUrl: "profileEdit", video: nil, image: imgData, imageKey: "image", document: nil, docKey: "") { (resonse, msg, success) in
            
            Hud.shared.hideHud()
            
            if resonse == nil{
                Hud.shared.show_ErrorHud(Localization("Please check network connection"))
                return
            }
            
            let dicData = resonse as! NSDictionary
            let jsonData = try! JSONSerialization.data(withJSONObject: dicData)
            
            do {
                let json: JSON = try JSON(data:jsonData)
                
                
                if (json["statusCode"].stringValue == "200"){
                    
                    let jData = json["data"]
                    
                    Hud.shared.show_SuccessHud(Localization("Profile updated successfully"))
                    DispatchQueue.main.async {
                        
                        global.shared.saveJSON(json: jData, key: Constants.UserDefult.SaveUserData.rawValue)
                        UserDefaults.standard.set(lat, forKey: "lat")
                        UserDefaults.standard.set(lng, forKey: "lng")
                        

                    }
                    
                }
                else{
                    // error
                    Hud.shared.show_ErrorHud(json["message"].stringValue)
                }
            } catch{
                Hud.shared.hideHud()
            }
            
            
        }
        
    }
    
    
    //MARK:- Get Lat Long From Address
    func getLocation(from address: String, completion: @escaping (_ location: CLLocationCoordinate2D?)-> Void) {
        let geocoder = CLGeocoder()
        geocoder.geocodeAddressString(address) { (placemarks, error) in
            guard let placemarks = placemarks,
            let location = placemarks.first?.location?.coordinate else {
                completion(nil)
                return
            }
            completion(location)
        }
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

//MARK:- Image picker Delegate
extension EditProfileVC:ImagePickerDelegate{
    
    func didSelect(image: UIImage?) {
        guard let img = image else {return}
        self.imgProfile.image = img.resizedToLessThan1MB()
    }
    
}
