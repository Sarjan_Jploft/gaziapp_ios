//
//  Constant.swift
//  SocialHub
//
//  Created by Mac on 12/2/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import Foundation
import UIKit
                                            
struct Constants
{
   
    //Buttons Config
    
    static let buttonBordercolor = UIColor(red: 1/255, green: 87/255, blue: 41/255, alpha: 1).cgColor
    static let gradientGreen1 = UIColor(red: 16/255, green: 214/255, blue: 95/255, alpha: 1)
    static let gradientGreen2 = UIColor(red: 13/255, green: 167/255, blue: 151/255, alpha: 1)
    static let buttonCornerRadius = CGFloat(4)
    
    
    //Texfiled Config
    
    static let textfieldBordercolor = UIColor(red: 212/255, green: 212/255, blue: 212/255, alpha: 1).cgColor
    static let textFiledCornerRadius = CGFloat(4)
 
    static let headViewTopGradient = UIColor(red: 237/255, green: 104/255, blue: 47/255, alpha: 1)
    static let headViewBottomGradient = UIColor(red: 247/255, green: 142/255, blue: 53/255, alpha: 1)
    
    // View shadow color
    
    static let viewShadowColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.16)
    
    
//    static let GoogleApiKey = "AIzaSyDKw2pf_yEMNtptH9wlVw4fr1LmeoH5Qu8" //Temp
    
//    static let GoogleApiKey = "AIzaSyA7CG_Xd_BVR-SAkW-O_iG8cu2N9GUUuvU"
    
    static let GoogleApiKey = "AIzaSyDzAOwnlEUhl7IyXdcyGUn_e6uoES6spwc"
    
    
    struct AppLinks {
//        static let APP_MODE = "navigation_mode"
//        static let APP_MODE = "user_managment_mode"//"navigation_mode"
        static let APP_NAME = "Gazi"
//        static let API_BASE_URL = "http://sample.jploft.com/social-hub/api/"
        static let API_BASE_URL = "http://stageofproject.com/gas/Webservice/"
        
        //Live
//        static let API_BASE_URL = "http://13.233.161.115/socialhub/api/"
        
        
    }

    enum AppMode : String {
        case Navigation = "navigation_mode"
        case UserManagment = "user_managment_mode"
    }
    
    enum ApiUrl : String {
        
        case SignUp = "registration"
        case Login = "login"
        case OtpVerify = "otpverify"
        case ResendOtp = "resendotp"
        case ForgetPassword = "forgetpassword"
        case BookNow = "BookNow"
        case ApplyPromoCode = "ApplyCode"
        case Rating = "RateProduct"
        case SearchDriver = "searchDriver"
        case GetDriverLatLng = "get_user_driver_latlong"
        case ChangePassword = "changepassword"
        case CylinderCategories = "cylinders"
        case CancalOrder = "CancalOrder"
        case OrderDetail = "OrderDetails"
        case MyOrders = "myOrders"
        case EditProfile = "profileEdit"
        case GasAgency = "gasagency"
        case CancelReasons = "reasonList"
        
        
        case GetProfile = "get-profile"
        
        
        
        case HomeData = "homepage"
        case GetFeed = "postList"
        case BlockList = "blockList"
        case BlockUser = "blockUser"
        case AddFav = "addFav"
        case SendRequest = "sendRequest"
        case vibes = "vibes"
        case SendVibes = "sendvibes"
        case AddPost = "addPost"
        case Follow = "addfollowers"
        case NotificationList = "notificationlist"
        case HidePost = "hidepost"
        case DeletePost = "deletepost"
        case AddRespect = "addrespect"
        case PreviousPlace = "previousplace"
        case VideoList = "videoList"
        case followList = "getfollowerlist"
        case getActivity = "activity"
        case getFriendList = "getFriendList"
        case addActivity = "addactivity"
        case uploadVideo = "uploadVideo"
        case ReportUser = "reportuser"
        case AcceptFollower = "acceptfollower"
        case RejectInvite = "rejectinvite"
        case NotificationSeen = "notificationseen"
        case StoreLatLong = "storeletlong"
        case AddVibesCategories = "addvibescategories"
        case Addviewvideo = "addviewvideo"
        case SearchUser = "usersearch"
        case CheckBlockUser = "checkblockuser"
        case ViewAndFav = "viewerandfav"
        case BlockUserFirebase = "blockUserfirbase"
        case UpdateVibe = "updatevibe"
        case PostDetail = "postdetail"
        case AddComment = "addcomment"
        
    }
    
    
    enum UserDefult : String {
        case SaveUserData = "save-user-data-in-defult"
    }
    
    enum errorMessage: String {
        
        case LogInId = "Please enter login id"
        case FirstNameEmpty = "Please Enter First Name"
        case LastNameEmpty = "Please Enter Last Name"
        case DobEmpty = "Please Enter DOB"
        case GenderEmpty = "Please Enter Gender"
        case CountryCodeEmpty = "Please Enter Country Code"
        case MobileNoEmpty = "Please Enter Mobile Number"
        case MobileNoInvalid = "Mobile Number is not valid."
        case EmailInvalid = "Email is not valid"
        case EmailEmpty = "Please Enter Email"
        case PasswordEmpty = "Please Enter Password "
        case CurrentPasswordEmpty = "Please Enter current Password "
        case PasswordMinimum = "Password can not be less then 8 characters."
        case ConfirmPasswordEmpty = "Please Enter Confirm password"
        case ConfirmPasswordInvalid = "Confirm password is not match with password."
        case CurrentPasswordInvalid = "Current password is mismatched."
        case newPasswordInvalid = "Please Enter New password"
        case EnterActivity = "Please select Activity"
        case EnterLocation = "Please select Location"
        case EnterVibes = "Please Enter Vibes"
        case TermsCond = "Please accept terms and condition"
        case AddressEmpty = "Please enter address"
        case Enterpromocode = "Please enter promo code"
        case SelectCategoryReplace = "Please select type for replacement cylinder"
        case SelectQtyReplace = "Please select quantity for replacement cylinder"
        case SelectCategoryNew = "Please select type for new cylinder"
        case SelectQtyNew = "Please select quantity for new cylinder"
        case CardNumberEmpty = "Please enter card number"
        case CardNumberValid = "Please enter a valid card number"
        case CardNameEmpty = "Please enter card name"
        case CVVEmpty = "Please enter CVV"
        case CVVvalid = "Please enter a valid CVV"
        
        
        case RequestAlreadySent = "Seems!! You have already sent request"
        case otpInvalid = "Please Enter Otp"
        case otpNotMatch = "Entered Otp is not correct"
        case networkError = "Error!! Check your internet connection"
        
        
    }
    
    
    
    
}
