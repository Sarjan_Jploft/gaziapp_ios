//
//  Library.swift
//  TaskbeGas
//
//  Created by Savita Kunjan Sharma  on 6/4/18.
//  Copyright © 2018 Orion. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ApiLibrary: NSObject {
    
    static let shared = ApiLibrary()
    
    // MARK:- Common API Method
    
    
    func APICalling(postDictionary: [String: Any]?, strApiUrl: String, in vc:UIViewController, showHud hud: Bool = true, completion: @escaping (_ responce : AnyObject?,_ message : String,_ status : Bool) -> ()) {
        
        do{
            print("\(Constants.AppLinks.API_BASE_URL)\(strApiUrl)")
            Alamofire.request("\(Constants.AppLinks.API_BASE_URL)\(strApiUrl)", method: .post, parameters:postDictionary, encoding: URLEncoding.init(destination: .httpBody), headers: nil).responseJSON { (response) in
                
                print(response)
                
                //                switch(response.result) {
                //
                //                case .success(_):
                //
                //                    if response.result.value != nil
                //                    {
                //                        let JSON = response.result.value! as AnyObject
                //
                //                        let responce = JSON as! NSDictionary
                //
                //                        let sucess = responce.value(forKey: "status") as? String
                //
                //                        let message = responce.value(forKey: "message") as? String ?? ""
                //
                //                        if sucess == "200"
                //                        {
                //                            let data = responce.value(forKey: "data") as? AnyObject
                //
                //                            completion(data, message, true)
                //                        }
                //                        else
                //                        {
                //
                //                            completion(nil, message, false)
                //                            self.presentAlertWithTitle(title: "", message: "", vc: vc)
                //                        }
                //
                //
                //                    }
                //
                //                    break
                //
                //                case .failure(_):
                //
                //                    print(response.result.error ?? "Fail")
                //                    completion(nil, (response.result.error?.localizedDescription)!, false)
                //                    self.presentAlertWithTitle(title: "", message: (response.result.error?.localizedDescription)!, vc: vc)
                //                    break
                //
                //                }
                
                
            }
            
            
            
        }
        
    }
    
    func APICallingWithParameters(postDictionary: [String: Any]?, strApiUrl: String, in vc:UIViewController, showHud hud: Bool = true, completion: @escaping (_ responce : AnyObject?,_ message : String,_ status : Bool) -> ()) {
        
        do{
            
            print("\(Constants.AppLinks.API_BASE_URL)\(strApiUrl)")
            Alamofire.request("\(Constants.AppLinks.API_BASE_URL)\(strApiUrl)", method: .post, parameters:postDictionary, encoding: URLEncoding.init(destination: .httpBody), headers: nil).responseJSON { (response) in
                
                
                print(response)
                
                switch(response.result) {
                    
                case .success(_):
                    
                    if response.result.value != nil
                    {
                        let JSON = response.result.value! as AnyObject
                        
                        let responce = JSON as! NSDictionary
                        
                        completion(responce, "", true)
                        
                    }
                    
                    break
                    
                case .failure(_):
                    //     CommonFile.shared.progressBarVisibility(false)
                    print(response.result.error ?? "Fail")
                    completion(nil, (response.result.error?.localizedDescription)!, false)
                    self.presentAlertWithTitle(title: "", message: (response.result.error?.localizedDescription)!, vc: vc)
                    
                    break
                    
                }
            }
            
        }
        
    }
    
    
    // image upload function
    
    func imageUpload(postDictionary: [String: Any?], strApiUrl: String, video:Data?,  image:Data?, imageKey:String, document:Data?, docKey:String, completion: @escaping (_ responce : AnyObject?,_ message : String,_ status : Bool) -> ()) {
        
        
        let urlString = "\(Constants.AppLinks.API_BASE_URL)\(strApiUrl)"
        
        let url = try! URLRequest(url: URL(string:urlString)!, method: .post, headers: nil)
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                for (key, value) in postDictionary {
                    if value is String
                    {
                        multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                    }else if value is [String]
                    {

                        for (_ ,value) in (value as! [String]).enumerated() {
                            print("key value", key)

                            multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key)
                        }
                        
                        
                    }
                   
                }
                
                let date = Date()
                let formatter = DateFormatter()
                formatter.dateFormat = "ddyyyy_hhmmss"
                let fileName = "profilePic".appending(formatter.string(from: date))
                
                if image != nil {
                    multipartFormData.append(image!, withName: imageKey, fileName: "\(fileName)_.png", mimeType: "image/png")
                }
                
                if document != nil {
                    multipartFormData.append(image!, withName: docKey, fileName: "\(fileName)_.png", mimeType: "image/png")
                }
                
                if video != nil{
                    multipartFormData.append(video!, withName: imageKey, fileName: "\(fileName)_.mp4", mimeType: "video/mp4")
                }
                
                
        }, with: url,
           encodingCompletion: { encodingResult in
            
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    print(response)
                    
                    if response.result.value != nil{
                        
                        let JSON = response.result.value! as AnyObject
                        
                        let responce = JSON as! NSDictionary
                        
                        completion(responce, "", true)
                    }
                  
                }
            case .failure( _):
                
                completion(nil, "Error", false)
                
                break
                
            }
        }
        )
        
    }
    
    
    func presentAlertWithTitle(title : String, message : String, vc : UIViewController){
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let okButton = UIAlertAction(title: "Ok", style: .default) { (_) in
            
        }
        alertController.addAction(okButton)
        vc.present(alertController, animated: true, completion: nil)
    }
    
    
    //Get Method
    
    
    func APICallingGet(strApiUrl: String, in vc:UIViewController, showHud hud: Bool = true, completion: @escaping (_ responce : AnyObject?,_ message : String,_ status : Bool) -> ()) {
        
        do{
            
            print("\(Constants.AppLinks.API_BASE_URL)\(strApiUrl)")
            
            Alamofire.request("\(Constants.AppLinks.API_BASE_URL)\(strApiUrl)", method: .get, encoding: JSONEncoding.default)
                .responseJSON { response in
                    
                    print(response)
                    
                    switch(response.result) {
                        
                    case .success(_):
                        
                        if response.result.value != nil
                        {
                            let JSON = response.result.value! as AnyObject
                            
                            let responce = JSON as! NSDictionary
                            
                            completion(responce, "", true)
                            
                        }
                        
                        break
                        
                    case .failure(_):
                        //     CommonFile.shared.progressBarVisibility(false)
                        print(response.result.error ?? "Fail")
                        completion(nil, (response.result.error?.localizedDescription)!, false)
                        self.presentAlertWithTitle(title: "", message: (response.result.error?.localizedDescription)!, vc: vc)
                        
                        break
                        
                    }
            }
            
        }
        
    }
    
    
    func APICall(param_dict: [String: Any]?, strApiUrl: String, in vc:UIViewController, showHud hud: Bool = true, completion: @escaping (_ responce : JSON, _ msg : String) -> ()){
        
        print("\(Constants.AppLinks.API_BASE_URL)\(strApiUrl)")
        
       
        Alamofire.request("\(Constants.AppLinks.API_BASE_URL)\(strApiUrl)" , method: .post, parameters: param_dict, headers:nil).responseJSON(completionHandler: { (response) in
            
            do {
                let json: JSON = try JSON(data: response.data!)
                
                let msg = json["message"].stringValue
                completion(json,msg)
                
                
            } catch{
              self.presentAlertWithTitle(title: "", message: "Please check network connection", vc: vc)
                
            }
        })
        
    }
    
    
}

