////
////  PushNotificationManager.swift
////  SocialHub
////
////  Created by Apple on 14/02/20.
////  Copyright © 2020 Mac. All rights reserved.
////
//
//import UIKit
//import Firebase
//import FirebaseFirestore
//import FirebaseMessaging
//import UserNotifications
//
//class PushNotificationManager: NSObject,MessagingDelegate,UNUserNotificationCenterDelegate {
//    
//    
//    let userID:String
//    
//    init(userId:String) {
//        self.userID = userId
//        super.init()
//    }
//    
//    func registerForPushNotifications() {
//        if #available(iOS 10.0, *) {
//            // For iOS 10 display notification (sent via APNS)
//            UNUserNotificationCenter.current().delegate = self
//            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
//            UNUserNotificationCenter.current().requestAuthorization(
//                options: authOptions,
//                completionHandler: {_, _ in })
//            // For iOS 10 data message (sent via FCM)
//            Messaging.messaging().delegate = self
//        } else {
//            let settings: UIUserNotificationSettings =
//                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
//            UIApplication.shared.registerUserNotificationSettings(settings)
//        }
//        UIApplication.shared.registerForRemoteNotifications()
//        updateFirestorePushTokenIfNeeded()
//    }
//    
//    
//    func updateFirestorePushTokenIfNeeded() {
//        if let token = Messaging.messaging().fcmToken {
//            global.shared.saveStringInUserDefult(data_str: token, key: "FCMToken")
//            let usersRef = Firestore.firestore().collection("users_table").document(userID)
////            usersRef.setData(["fcmToken": token], merge: true)
//            usersRef.setData(["fcmToken":token], merge: true) { (error) in
//                if let error = error{
//                    print(error)
//                }else{
//                    print("successfully updated")
//                }
//            }
//            
//        }
//    }
//    
//    
//    
//    
//    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
//        updateFirestorePushTokenIfNeeded()
//    }
//    
//    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
//        print(response)
//    }
//    
//    
//    
//   
//}
