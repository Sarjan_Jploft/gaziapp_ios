//
//  ApiKeys.swift
//  SocialHub
//
//  Created by Mac on 12/3/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import Foundation

enum ApiKey {
    
    static var status: String { return "status" }
    static var code: String { return "statusCode" }
    static var result: String { return "RESULT" }
    static var message: String { return "message" }
    
    //MARK:- User Data Api keys
    static var userId: String { return "user_id" }
    static var userDob: String { return "dob" }
    static var userEmail: String { return "email" }
    static var userFirstName: String { return "firstName" }
    static var userGender: String { return "gender" }
    static var userLastName: String { return "lastName" }
    static var userPhone: String { return "phone" }
    static var postId: String { return "postId" }
    static var id: String { return "id" }
    static var name: String { return "name" }
    static var rating: String { return "rating" }
    static var image: String { return "image" }
    static var lat: String { return "latitude" }
    static var long: String { return "longitude" }
    static var activity: String { return "activity" }
    static var requestSent: String { return "userinvitestatus" }
    static var profileImg: String { return "profileimage" } 
    static var question: String { return "question" }
    static var title: String { return "title" }
    static var option1: String { return "option1" }
    static var option2: String { return "option2" }
    
    
   
}
