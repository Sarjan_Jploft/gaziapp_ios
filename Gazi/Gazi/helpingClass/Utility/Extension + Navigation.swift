//
//  Extension + Navigation.swift
//  Gazi
//
//  Created by Apple on 09/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation
import UIKit



//MARK:- Flip Pop and Push

extension UINavigationController {

    func pushViewControllerWithFlipAnimation(viewController:UIViewController){
        self.pushViewController(viewController
        , animated: false)
        if let transitionView = self.view{
            UIView.transition(with:transitionView, duration: 0.8, options: .transitionFlipFromLeft, animations: nil, completion: nil)
        }
    }

    func popViewControllerWithCurlDownAnimation(){
        self.popViewController(animated: false)
        if let transitionView = self.view{
            UIView.transition(with:transitionView, duration: 0.8, options: .transitionCurlDown, animations: nil, completion: nil)
        }
    }
}
