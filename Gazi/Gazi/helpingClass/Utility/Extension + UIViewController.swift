//
//  Extension + UIViewController.swift
//  Gazi
//
//  Created by Apple on 09/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController{
    
    //MARK:- func For Add Padding in Textfiled
    
    func addPaddingAndBorder(to textfield: UITextField) {
        
        textfield.layer.cornerRadius =  Constants.textFiledCornerRadius
        textfield.layer.borderColor = Constants.textfieldBordercolor
        textfield.layer.borderWidth = 1
//        let leftView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: 12.0, height: 2.0))
////        textfield.leftView = leftView
//        textfield.rightView = leftView
////        textfield.leftViewMode = .always
//        textfield.rightViewMode = .always
    }
    
    
    //MARK:- func For Add Border And Gradients
    
    func addGreenGradientsAndBorder(to button: UIButton) {
        
        DispatchQueue.main.async {
            button.applyGradient(colours:[Constants.gradientGreen1,Constants.gradientGreen2])
            button.layer.borderWidth = 1
            button.layer.cornerRadius = Constants.buttonCornerRadius
            button.layer.borderColor = Constants.buttonBordercolor
            button.clipsToBounds = true
        }
        
        
    }
    
}


//MARK:- Change Status Bar Color

extension UIViewController{
    
    func changeStatusBarColor(){
        
        if #available(iOS 13.0, *) {
            let app = UIApplication.shared
            let statusBarHeight: CGFloat = app.statusBarFrame.size.height
            
            let statusbarView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: statusBarHeight))
            statusbarView.backgroundColor = Constants.headViewTopGradient//UIColor(named: "ThemeColor")
            view.addSubview(statusbarView)
            
        } else {
            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
            statusBar?.backgroundColor = Constants.headViewTopGradient//UIColor(named: "ThemeColor")
        }
    }
    
}


//MARK:-

extension UIViewController{
    
    
    func validateName(_ name : String) -> Bool {
        
        if(name == ""){
            
           HelpingMethod.shared.presentAlertWithTitle(title: "Error", message: Constants.errorMessage.CardNameEmpty.rawValue, vc: self)
            
            return false
        }
        
        return true
    }
    
    
    func validateCreditCardNumber(_ creditCardNumber : String) -> Bool {
           
           if(creditCardNumber == ""){
            
            HelpingMethod.shared.presentAlertWithTitle(title: "Error", message: Constants.errorMessage.CardNumberEmpty.rawValue, vc: self)
              
               return false
           }
           if(creditCardNumber.count != 19){
            
               HelpingMethod.shared.presentAlertWithTitle(title: "Error", message: Constants.errorMessage.CardNumberValid.rawValue, vc: self)
            
               return false
           }
           
           return true
       }
       
       
       
       func validateCVV(_ cvv : String) -> Bool {
           
           if(cvv == ""){
            
               HelpingMethod.shared.presentAlertWithTitle(title: "Error", message: Constants.errorMessage.CVVEmpty.rawValue, vc: self)
            
               return false
           }
           
           if(cvv.count != 3 && cvv.count != 4  ){
              
            HelpingMethod.shared.presentAlertWithTitle(title: "Error", message: Constants.errorMessage.CVVvalid.rawValue, vc: self)
            
               return false
           }
           
           return true
       }
       
}
