//
//  Extensions.swift
//  Infinite Cabs
//
//  Created by micro on 17/10/19.
//  Copyright © 2019 micro. All rights reserved.
//

import Foundation
import UIKit


// MARK:- Set Bottom line in Textfield

extension UITextField {
    
    //Set Border in bottom
    
    func setBottomBorder(borderColor: CGColor = UIColor.black.cgColor,
        backgroundColor: CGColor = UIColor.white.cgColor) {
        self.borderStyle = .none
        self.layer.backgroundColor = backgroundColor

        let border = CALayer()
        let width = CGFloat(2.0)
        border.borderColor = borderColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width, height: self.frame.size.height)
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
    
    
    // Set image in RightView
    func setRightViewImage(_ imgStr:String){
        
         let rightVw = UIView(frame: CGRect(x: 0, y: 0, width:16, height: 16))
        
        let imageView = UIImageView(frame: CGRect(x: 0, y: 6, width: 12, height: 8))
        imageView.image = UIImage(named: imgStr)
        rightVw.addSubview(imageView)
        self.rightView = rightVw
        self.rightViewMode = .always
        
    }
    
    // Set image in LeftView
    func setLeftViewImage(_ imgStr:String){
        
        let leftVw = UIView(frame: CGRect(x: 0, y: 0, width:40, height: 40))
        
        let imageView = UIImageView(frame: CGRect(x: 2, y: 2, width: 24, height: 24))
        imageView.image = UIImage(named: imgStr)
        leftVw.addSubview(imageView)
        self.leftView = leftVw
        self.leftViewMode = .always
        
    }
    
    
    
    
}

// MARK:- Set Bottom line in Textview

extension UITextView {
    func setBottomBorder(borderColor: CGColor = UIColor.black.cgColor,
        backgroundColor: CGColor = UIColor.white.cgColor) {
        
        self.layer.backgroundColor = backgroundColor
        let border = CALayer()
        let width = CGFloat(2.0)
        border.borderColor = borderColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width, height: self.frame.size.height)
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
}




// MARK:- viewController

extension UIViewController {
    
    
    // Hide KeyBoard when touch around
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    
    
    //MARK:- Redirect to setting if location denied
    
    
     func showPermissionAlert(){
        let alertController = UIAlertController(title: "Location Permission Required", message: "Please enable location permissions in settings.", preferredStyle: UIAlertController.Style.alert)

        let okAction = UIAlertAction(title: "Settings", style: .default, handler: {(cAlertAction) in
            //Redirect to Settings app
            UIApplication.shared.open(URL(string:UIApplication.openSettingsURLString)!)
        })

        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
        alertController.addAction(cancelAction)

        alertController.addAction(okAction)

        self.present(alertController, animated: true, completion: nil)
    }
    
    
}


// MARK:- UIApplication

extension UIApplication {
    
var statusBarUIView: UIView? {
    if #available(iOS 13.0, *) {
        let tag = 38482
        let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first

        if let statusBar = keyWindow?.viewWithTag(tag) {
            return statusBar
        } else {
            guard let statusBarFrame = keyWindow?.windowScene?.statusBarManager?.statusBarFrame else { return nil }
            let statusBarView = UIView(frame: statusBarFrame)
            statusBarView.tag = tag
            keyWindow?.addSubview(statusBarView)
            return statusBarView
        }
    } else if responds(to: Selector(("statusBar"))) {
        return value(forKey: "statusBar") as? UIView
    } else {
        return nil
    }
  }
    
}


// MARK:-Extension for Shadow

extension UIButton{
    
    func setShadow(){
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        self.layer.shadowRadius = 4
        self.layer.shadowOpacity = 0.5
    }
    
    
}

// MARK:- Border Color

extension UIView{
    
    func borderColorWidth(clr:CGColor,wdth:CGFloat = 1,cornerRadius:CGFloat = 4){
        
        self.layer.borderColor = clr
        self.layer.borderWidth = wdth
        self.layer.cornerRadius = cornerRadius
        
    }
    
}


//MARK:- Round Specific Corner
extension UIView {
   func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
}

//MARK:- Add SubView and Remove Subview

public extension UIViewController {

    /// Adds child view controller to the parent.
    ///
    /// - Parameter child: Child view controller.
    func addChld(_ child: UIViewController) {
        addChild(child)
        view.addSubview(child.view)
        child.didMove(toParent: self)
    }

    /// It removes the child view controller from the parent.
    func removeChld() {
        guard parent != nil else {
            return
        }
        willMove(toParent: nil)
        removeFromParent()
        view.removeFromSuperview()
    }
}


//MARK:- Gradients Color

extension UIView {
    
    // For Top to Bottom
    func applyGradient(colours: [UIColor]) -> Void {
        
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        //for left to right
//        gradient.startPoint = CGPoint(x : 0.0, y : 0.5)
//        gradient.endPoint = CGPoint(x :1.0, y: 0.5)
        //for top to bottom
        gradient.startPoint = CGPoint(x : 0.5, y : 0.0)
        gradient.endPoint = CGPoint(x :0.5, y: 1.0)
        self.layer.insertSublayer(gradient, at: 0)
        
    }
    
    //For Left To Right
     func applyGradientLToR(colours: [UIColor]) -> Void {
            
            let gradient: CAGradientLayer = CAGradientLayer()
            gradient.frame = self.bounds
            gradient.colors = colours.map { $0.cgColor }
            //for left to right
            gradient.startPoint = CGPoint(x : 0.0, y : 0.5)
            gradient.endPoint = CGPoint(x :1.0, y: 0.5)
            
            self.layer.insertSublayer(gradient, at: 0)
            
        }
    
}

extension CALayer {
    
    func applyGradient(colours: [UIColor]) -> Void {
        
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        //for left to right
//        gradient.startPoint = CGPoint(x : 0.0, y : 0.5)
//        gradient.endPoint = CGPoint(x :1.0, y: 0.5)
        //for top to bottom
        gradient.startPoint = CGPoint(x : 0.5, y : 0.0)
        gradient.endPoint = CGPoint(x :0.5, y: 1.0)
        CALayer.self().insertSublayer(gradient, at: 0)
        
    }
    
}



//MARK:- Extension For tableview if no data available

extension UITableView {
    
    //call this if no of lines is 0
    func setEmptyView(title: String, message: String) {
        
        let emptyView = UIView(frame: CGRect(x: self.center.x, y: self.center.y, width: self.bounds.size.width, height: self.bounds.size.height))
        let titleLabel = UILabel()
        let messageLabel = UILabel()
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        messageLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.textColor = UIColor.black
        titleLabel.font = UIFont(name: "Dubai-Bold", size: 18)
        messageLabel.textColor = UIColor.lightGray
        messageLabel.font = UIFont(name: "HelveticaNeue-Regular", size: 17)
        emptyView.addSubview(titleLabel)
        emptyView.addSubview(messageLabel)
        titleLabel.centerYAnchor.constraint(equalTo: emptyView.centerYAnchor).isActive = true
        titleLabel.centerXAnchor.constraint(equalTo: emptyView.centerXAnchor).isActive = true
        messageLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 20).isActive = true
        messageLabel.leftAnchor.constraint(equalTo: emptyView.leftAnchor, constant: 20).isActive = true
        messageLabel.rightAnchor.constraint(equalTo: emptyView.rightAnchor, constant: -20).isActive = true
        titleLabel.text = title
        messageLabel.text = message
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        // The only tricky part is here:
        self.backgroundView = emptyView
        self.separatorStyle = .none
        
    }
    
    
    //call this if data availabel
    func restore() {
        
        self.backgroundView = nil
        self.separatorStyle = .singleLine
        
    }
    
    
}


//MARK:- Extension For tableview if no data available

extension UICollectionView {
    
    //call this if no of lines is 0
    func setEmptyView(title: String, message: String) {
        
        let emptyView = UIView(frame: CGRect(x: self.center.x, y: self.center.y, width: self.bounds.size.width, height: self.bounds.size.height))
        let titleLabel = UILabel()
        let messageLabel = UILabel()
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        messageLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.textColor = UIColor.black
        titleLabel.font = UIFont(name: "HelveticaNeue-Bold", size: 18)
        messageLabel.textColor = UIColor.lightGray
        messageLabel.font = UIFont(name: "HelveticaNeue-Regular", size: 17)
        emptyView.addSubview(titleLabel)
        emptyView.addSubview(messageLabel)
        
        titleLabel.centerYAnchor.constraint(equalTo: emptyView.centerYAnchor).isActive = true
        titleLabel.centerXAnchor.constraint(equalTo: emptyView.centerXAnchor).isActive = true
        
        messageLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 20).isActive = true
        messageLabel.leftAnchor.constraint(equalTo: emptyView.leftAnchor, constant: 20).isActive = true
        messageLabel.rightAnchor.constraint(equalTo: emptyView.rightAnchor, constant: -20).isActive = true
        titleLabel.text = title
        messageLabel.text = message
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        // The only tricky part is here:
        self.backgroundView = emptyView
//        self.separatorStyle = .none
        
    }
    
    
    //call this if data availabel
    func restore() {
        
        self.backgroundView = nil
//        self.separatorStyle = .singleLine
        
    }
    
    
}



//MARK:- Date to String
extension Date
{
    func toString( dateFormat format  : String ) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
}


//MARK:- String to Date
extension String
{
    func toDate( dateFormat format  : String) -> Date
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        return dateFormatter.date(from: self)!
    }
    
}


extension UIViewController{
    
    //MARK: Toolbar method
    func methodAddToolbarEx() -> UIToolbar
    {
        let toolbar = UIToolbar(frame:CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        toolbar.barStyle = .default
        toolbar.items = [
            UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(clickOnCancelEx)),
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
            UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(clickOnDoneEx))]
        toolbar.sizeToFit()
        
        return toolbar
        
    }
    
    @objc func clickOnCancelEx()
    {
        
        self.view.endEditing(true)
    }
    
    @objc func clickOnDoneEx()
    {
        self.view.endEditing(true)
    }
    
}


//MARK:- Drop shadow with Rounded Corner
extension UIView{
    func dropShadow(shadowColor: UIColor = UIColor.black,
                    fillColor: UIColor = UIColor.white,
                    opacity: Float = 0.2,
                    offset: CGSize = CGSize(width: 0.0, height: 1.0),
                    radius: CGFloat = 10) -> CAShapeLayer {

        let shadowLayer = CAShapeLayer()
        shadowLayer.path = UIBezierPath(roundedRect: self.bounds, cornerRadius: radius).cgPath
        shadowLayer.fillColor = fillColor.cgColor
        shadowLayer.shadowColor = shadowColor.cgColor
        shadowLayer.shadowPath = shadowLayer.path
        shadowLayer.shadowOffset = offset
        shadowLayer.shadowOpacity = opacity
        shadowLayer.shadowRadius = radius
        layer.insertSublayer(shadowLayer, at: 0)
        return shadowLayer
    }
}

//MARK:- Reduce Size to less than 1 MB
extension UIImage {

func resized(withPercentage percentage: CGFloat) -> UIImage? {
    let canvasSize = CGSize(width: size.width * percentage, height: size.height * percentage)
    UIGraphicsBeginImageContextWithOptions(canvasSize, false, scale)
    defer { UIGraphicsEndImageContext() }
    draw(in: CGRect(origin: .zero, size: canvasSize))
    return UIGraphicsGetImageFromCurrentImageContext()
}

func resizedToLessThan1MB() -> UIImage? {
    guard let imageData = self.pngData() else { return nil }

    var resizingImage = self
    var imageSizeKB = Double(imageData.count) / 1000.0 // ! Or devide for 1024 if you need KB but not kB

    while imageSizeKB > 1000 { // ! Or use 1024 if you need KB but not kB
        guard let resizedImage = resizingImage.resized(withPercentage: 0.9),
            let imageData = resizedImage.pngData()
            else { return nil }

        resizingImage = resizedImage
        imageSizeKB = Double(imageData.count) / 1000.0 // ! Or devide for 1024 if you need KB but not kB
    }

    return resizingImage
}
}


extension Double {
    func removeZerosFromEnd() -> String {
        let formatter = NumberFormatter()
        let number = NSNumber(value: self)
        formatter.minimumFractionDigits = 0
        formatter.maximumFractionDigits = 2
        return String(formatter.string(from: number) ?? "")
    }
}
