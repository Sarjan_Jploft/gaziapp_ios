//
//  HelpingMethod.swift
//  SocialHub
//
//  Created by Mac on 12/2/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import Foundation
import UIKit

class HelpingMethod : NSObject
{
    static let shared = HelpingMethod()
    
    func VaildateTxtEmail(strEmail : String) -> Bool
    {
        let REGEX: String
        REGEX = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        return NSPredicate(format: "SELF MATCHES %@", REGEX).evaluate(with: strEmail)
    }
    
    
    // MARK: validate TextField
    
    class func validateTextFiled(textfiled: UITextField) -> Bool {
        
        textfiled.text = textfiled.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
        if textfiled.text==nil || textfiled.text == ""
        {
            return false
        }
        return true
    }
    
    // MARK: validate TextView
    class func validateTextView(textView: UITextView) -> Bool {
        
        textView.text = textView.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
        if textView.text==nil || textView.text == ""
        {
            return false
        }
        return true
    }
    
    
    
    func presentAlertWithTitle(title : String, message : String, vc : UIViewController){
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let okButton = UIAlertAction(title: Localization("Ok"), style: .default) { (_) in
            
        }
        alertController.addAction(okButton)
        vc.present(alertController, animated: true, completion: nil)
    }
    
}




