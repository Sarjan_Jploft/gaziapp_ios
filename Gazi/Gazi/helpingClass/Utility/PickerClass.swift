//
//  PickerClass.swift
//  TMA
//
//  Created by PineSucceed MacMini1 on 12/04/18.
//  Copyright © 2018 PineSucceed MacMini1. All rights reserved.
//




//protocol PickerClassDelegate:class
//{
//    func datePickerValue(_ datePicker: UIDatePicker?,datePickerKey:String)
//    func pickervalue(_ picker: UIPickerView?, selectedValue:String,selected_Id:String,pickerKey:String)
//}


import Foundation
import UIKit
//import SDWebImage



class PickerClass: NSObject,UIPickerViewDelegate, UIPickerViewDataSource
{
    
    var pickerArray = NSArray()
    
    var str_Pickerkey = String()
    var str_SelectedCountryCode = ""
    var str_SelectedCountryId = ""
    
    var picker = UIPickerView()
    
    
    static let sharedinstance = PickerClass()
    
    static let alertWindow: UIWindow = {
        let window = UIWindow(frame: UIScreen.main.bounds)
        window.windowLevel = UIWindow.Level.alert + 1
        return window
    }()
    
    //    MARK:- showDatePicker
    func showDatePicker(completionHandler:@escaping (UIDatePicker, Any)->()) -> UIDatePicker
    {
        let datePicker = UIDatePicker()
        // datePicker.maximumDate = Date()
        //datePicker.datePickerMode = .date
        
        let title = UIDevice.current.orientation.isLandscape ? "\n\n\n\n\n\n\n\n\n" : "\n\n\n\n\n\n\n\n\n\n\n\n"
        
        DispatchQueue.main.async(execute: {() -> Void in
            
            let datePickerContainer = UIAlertController(title: title, message: nil, preferredStyle: .actionSheet)
            datePickerContainer.view.addSubview(datePicker)
            datePickerContainer.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: .default, handler: {(_ action: UIAlertAction?) -> Void in
                
                DispatchQueue.main.async {
                    PickerClass.alertWindow.isHidden = true
                }
                
                completionHandler(datePicker,datePicker.date)
                
            }))
            
            datePickerContainer.addAction(UIAlertAction(title: NSLocalizedString("cancel", comment: ""), style: .default, handler: { (action) in
                
                DispatchQueue.main.async {
                    PickerClass.alertWindow.isHidden = true
                }
                
            }))
            
            
            //            let topWindow = UIWindow(frame: UIScreen.main.bounds)
            //            topWindow.rootViewController = UIViewController()
            //            topWindow.windowLevel = UIWindow.Level.alert + 0.8
            //
            //            topWindow.makeKeyAndVisible()
            //            DispatchQueue.main.async {
            //                topWindow.rootViewController?.present(datePickerContainer, animated: true, completion:nil)
            //            }
            
            
            
            
            
            // let topWindow = UIWindow(frame: UIScreen.main.bounds)
            
            PickerClass.alertWindow.rootViewController = UIViewController()
            PickerClass.alertWindow.windowLevel = UIWindow.Level.alert + 0.8
            
            PickerClass.alertWindow.makeKeyAndVisible()
            DispatchQueue.main.async {
                PickerClass.alertWindow.rootViewController?.present(datePickerContainer, animated: true, completion:nil)
            }
            
            
        });
        
        return datePicker;
    }
    
    //    MARK:- upPickerShow
    func upPickerShow(withArray array: NSArray, withkey key1: String, withTag tag: Int, completionHandler:@escaping (UIPickerView, Any)->())
    {
        
        if(array.count > 0)
        {
            pickerArray = array
            ////            print("value of pickerArray",pickerArray)
            str_Pickerkey = key1
            
            let datePickerContainer = UIAlertController(title: "\n\n\n\n\n\n\n\n\n\n\n\n", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
            
            let margin:CGFloat = 10.0
            
            
            var width = datePickerContainer.view.bounds.size.width - margin * 4.0
            if (UIDevice.current.userInterfaceIdiom == .pad){
                width = 270
            }
            
            
            let rect = CGRect(x: margin, y: margin, width:width, height: 200)
            
            picker = UIPickerView (frame:rect)
            picker.tag = tag
            picker.delegate = self
            datePickerContainer.view.addSubview(picker)
            
            datePickerContainer.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: .default, handler: {(_ action: UIAlertAction?) -> Void in
                
                DispatchQueue.main.async {
                    PickerClass.alertWindow.isHidden = true
                }
                completionHandler(self.picker,self.pickerArray[self.picker.selectedRow(inComponent: 0)])
                
                
            }))
            
            //        datePickerContainer.addAction(UIAlertAction(title: NSLocalizedString("cancel", comment: ""), style: .default, handler: nil))
            
            datePickerContainer.addAction(UIAlertAction(title: NSLocalizedString("cancel", comment: ""), style: .default, handler: { (action) in
                
                DispatchQueue.main.async {
                    PickerClass.alertWindow.isHidden = true
                }
                
            }))
            
            
            //        present(actionSheetController, animated: true, completion: nil)   // doesn't work for iPad
            
            //    actionSheetController.popoverPresentationController?.sourceView = yourSourceViewName // works for both iPhone & iPad
            //
            //    present(actionSheetController, animated: true) {
            //        //            print("option menu presented")
            //
            
            
            if array.count > 0
            {
                
                //            let topWindow = UIWindow(frame: UIScreen.main.bounds)
                //            topWindow.rootViewController = UIViewController()
                //            topWindow.windowLevel = UIWindow.Level.alert + 0.8
                //
                //            if let popoverController = datePickerContainer.popoverPresentationController {
                //
                //                popoverController.sourceView = topWindow
                //                popoverController.sourceRect =  picker.bounds
                //
                //            }
                //
                //            topWindow.makeKeyAndVisible()
                //            topWindow.rootViewController?.present(datePickerContainer, animated: true, completion: {})
                
                
                
                
                // let topWindow = UIWindow(frame: UIScreen.main.bounds)
                PickerClass.alertWindow.rootViewController = UIViewController()
                PickerClass.alertWindow.windowLevel = UIWindow.Level.alert + 0.8
                
                if let popoverController = datePickerContainer.popoverPresentationController {
                    
                    popoverController.sourceView = PickerClass.alertWindow
                    popoverController.sourceRect =  picker.bounds
                    
                }
                
                PickerClass.alertWindow.makeKeyAndVisible()
                
                DispatchQueue.main.async {
                    PickerClass.alertWindow.rootViewController?.present(datePickerContainer, animated: true, completion: {})
                }
                
                
            }
        }
    }
    
    
    
    //    //    MARK:- dictionaryOfNames
    //    func dictionaryOfNames(arr:UIView...) -> Dictionary<String,UIView>
    //    {
    //        var d = Dictionary<String,UIView>()
    //
    //        for (ix,v) in arr.enumerated()
    //        {
    //            d["v\(ix+1)"] = v
    //        }
    //        return d
    //    }
    //
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // returns the # of rows in each component..
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return pickerArray.count
        
    }
    
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        //            print("picker tag is:-",pickerView.tag)
        
        if (str_Pickerkey == "")
        {
            return pickerArray[row] as? String
        }
        else
        {
            // //            print("pickerArray is:-",pickerArray)
            
            let dicOfValue: NSDictionary = pickerArray[row] as! NSDictionary
            
            return dicOfValue[str_Pickerkey] as? String
        }
        
    }
    
    
    //    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
    //
    //
    //
    //        return 50
    //    }
    //
    //
    //    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
    //
    ////        var myView = UIView(frame: CGRect(x: 0, y: 0, width:pickerView.bounds.width - 30, height: 50))
    //        let myView = UIView(frame: CGRect(x: 50, y: 0, width:120, height: 50))
    //
    //        let myImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 120, height: 50))
    //
    //
    //        let dicOfValue: NSDictionary = pickerArray[row] as! NSDictionary
    //
    //        for _ in 1..<pickerArray.count {
    //
    //            myImageView.sd_setImage(with: URL.init(string: "\(ModelStaticApi.shared.paymentMethodImageBaseString)\(dicOfValue["apm_image"] as! String)"), placeholderImage: utilityObject.kPlaceholderImage, options: .refreshCached, completed: nil)
    //
    //
    //            myView.addSubview(myImageView)
    //
    //        }
    //
    //        return myView
    //
    //
    //    }
    
}




class PickerClassforView: NSObject,UIPickerViewDelegate, UIPickerViewDataSource
    
{
    
    var pickerArray = NSArray()
    var str_Pickerkey = String()
    var str_SelectedCountryCode = ""
    var str_SelectedCountryId = ""
    
    var picker = UIPickerView()
    
    
    static let sharedinstance = PickerClassforView()
    
    //    MARK:- showDatePicker
    func showDatePicker(completionHandler:@escaping (UIDatePicker, Any)->()) -> UIDatePicker
    {
        let datePicker = UIDatePicker()
        //datePicker.maximumDate = Date()
        datePicker.datePickerMode = .date
        
        let title = UIDevice.current.orientation.isLandscape ? "\n\n\n\n\n\n\n\n\n" : "\n\n\n\n\n\n\n\n\n\n\n\n"
        
        DispatchQueue.main.async(execute: {() -> Void in
            let datePickerContainer = UIAlertController(title: title, message: nil, preferredStyle: .actionSheet)
            datePickerContainer.view.addSubview(datePicker)
            datePickerContainer.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: .default, handler: {(_ action: UIAlertAction?) -> Void in
                
                completionHandler(datePicker,datePicker.date)
            }))
            
            datePickerContainer.addAction(UIAlertAction(title: NSLocalizedString("cancel", comment: ""), style: .default, handler: nil))
            
            let topWindow = UIWindow(frame: UIScreen.main.bounds)
            topWindow.rootViewController = UIViewController()
            topWindow.windowLevel = UIWindow.Level.alert + 0.8
            
            topWindow.makeKeyAndVisible()
            
            if let popoverController = datePickerContainer.popoverPresentationController {
                
                popoverController.sourceView = topWindow
                popoverController.sourceRect =  self.picker.bounds
                
            }
            topWindow.rootViewController?.present(datePickerContainer, animated: true, completion: {})
            
            
        });
        
        return datePicker;
    }
    
    //    MARK:- upPickerShow
    func upPickerShow(withArray array: NSArray, withkey key1: String, withTag tag: Int, completionHandler:@escaping (UIPickerView, Any)->())
    {
        
        if(array.count > 0)
        {
            pickerArray = array
            //            print("value of pickerArray",pickerArray)
            str_Pickerkey = key1
            
            let datePickerContainer = UIAlertController(title: "\n\n\n\n\n\n\n\n\n\n\n\n", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
            
            // if !DeviceType.IS_IPAD{
            
            // }
            
            let margin:CGFloat = 10.0
            // let rect = CGRect(x: margin, y: margin, width: datePickerContainer.view.bounds.size.width - margin * 4.0, height: 200)
            var width = datePickerContainer.view.bounds.size.width - margin * 4.0
            if (UIDevice.current.userInterfaceIdiom == .pad){
                width = 270
            }
            
            
            
            let rect = CGRect(x: margin, y: margin, width: width, height: 200)
            // CGRectMake(left), top, width, height) - left and top are like margins
            
            
            picker = UIPickerView (frame:rect)
            //            picker.center.x = ref.center.x
            
            picker.tag = tag
            picker.delegate = self
            datePickerContainer.view.addSubview(picker)
            
            datePickerContainer.addAction(UIAlertAction(title: NSLocalizedString("Select", comment: ""), style: .default, handler: {(_ action: UIAlertAction?) -> Void in
                completionHandler(self.picker,self.pickerArray[self.picker.selectedRow(inComponent: 0)])
                
                
            }))
            
            datePickerContainer.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .default, handler: nil))
            
            
            if array.count > 0
            {
                
                let topWindow = UIWindow(frame: UIScreen.main.bounds)
                topWindow.rootViewController = UIViewController()
                topWindow.windowLevel = UIWindow.Level.alert + 0.8
                
                topWindow.makeKeyAndVisible()
                
                if let popoverController = datePickerContainer.popoverPresentationController {
                    
                    popoverController.sourceView = topWindow
                    popoverController.sourceRect =  picker.bounds
                    
                }
                topWindow.rootViewController?.present(datePickerContainer, animated: true, completion: nil)
                
                //                topWindow.rootViewController?.popoverPresentationController?.sourceView = picker
                
                //                datePickerContainer.popoverPresentationController?.sourceView = self.picker
                
                //
                
                //                datePickerContainer.popoverPresentationController?.sourceRect = CGRect(x: 70, y: 100, width: 0, height: 0)
                //
                
                //                topWindow.rootViewController?.present(datePickerContainer, animated: true, completion: {
                //
                //
                //                })
                
                //                topWindow.rootViewController?.present(datePickerContainer, animated: true, completion: {})
                
                
                
            }
        }
    }
    
    
    
    //    //    MARK:- dictionaryOfNames
    //    func dictionaryOfNames(arr:UIView...) -> Dictionary<String,UIView>
    //    {
    //        var d = Dictionary<String,UIView>()
    //
    //        for (ix,v) in arr.enumerated()
    //        {
    //            d["v\(ix+1)"] = v
    //        }
    //        return d
    //    }
    //
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 1
    }
    
    // returns the # of rows in each component..
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return pickerArray.count
        
    }
    
    
    
    //    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    //    {
    //        //            print("picker tag is:-",pickerView.tag)
    //
    //        if (str_Pickerkey == "")
    //        {
    //            return pickerArray[row] as? String
    //        }
    //        else
    //        {
    //            //            print("pickerArray is:-",pickerArray)
    //
    //            //let dicOfValue: NSDictionary = pickerArray[row] as! NSDictionary
    //
    //            return "hello"  //dicOfValue[str_Pickerkey] as? String
    //        }
    //
    //    }
    //
    
    
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        
        
        return 50
    }
    
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        
        //        var myView = UIView(frame: CGRect(x: 0, y: 0, width:pickerView.bounds.width - 30, height: 50))
        let myView = UIView(frame: CGRect(x: 50, y: 0, width:120, height: 50))
        //        let myView = UIView(frame: CGRect(x: 10, y: 0, width:120, height: 50))
        
        let myImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 120, height: 50))
        
        
        let dicOfValue: NSDictionary = pickerArray[row] as! NSDictionary
        
        for _ in 0..<pickerArray.count {
            
            //change this
            //myImageView.sd_setImage(with: URL.init(string: "\(ModelStaticApi.shared.paymentMethodImageBaseString)\(dicOfValue["apm_image"] as! String)"), placeholderImage: utilityObject.kPlaceholderImage, options: .refreshCached, completed: nil)
            
            //comment this
            myImageView.image = UIImage(named: "camera")
            
            
            myView.addSubview(myImageView)
            
        }
        
        //        picker.reloadAllComponents()
        
        return myView
        
    }
    
}

//
//        var myView = UIView(frame: CGRectMake(0, 0, pickerView.bounds.width - 30, 60))
//
//        var myImageView = UIImageView(frame: CGRectMake(0, 0, 50, 50))
//
//        var rowString = String()
//
//        for _ in 1..<imageArray.count {
//            myImageView.image = UIImage(named: imageArray[row])
//
//            }
//        let myLabel = UILabel(frame: CGRectMake(60, 0, pickerView.bounds.width - 90, 60 ))
//        myLabel.font = UIFont(name:some font, size: 18)
//        myLabel.text = rowString
//
//        myView.addSubview(myLabel)
//        myView.addSubview(myImageView)
//
//        return myView
//
//
//    }
//

//
////        MARK: Profile Picker
//            if str_Pickerkey == "NATIONAL_ID_TYPE"
//            {
//                let arr_PickerArray = pickerArray as! [[String:Any]]
//                return arr_PickerArray[row]["idtype"] as? String
//            }
//            else if str_Pickerkey == "OWNER_CODE"
//            {
//                let arr_PickerArray = pickerArray as! [[String:Any]]
//                return arr_PickerArray[row]["ownercode"] as? String
//            }
//            else if str_Pickerkey == "OWNER_LOCATION"
//            {
//                //            print("pickerArray is:-",pickerArray)
//
//                let arr_PickerArray = pickerArray as! [[String:Any]]
//                return arr_PickerArray[row]["locationame"] as? String
//            }
//            else if str_Pickerkey == "OWNER_TYPE"
//            {
//                let arr_PickerArray = pickerArray as! [[String:Any]]
//                return arr_PickerArray[row]["ownertype"] as? String
//            }
//
////        MARK: Add Pet Picker
//            else if str_Pickerkey == "PET_BREEDS"
//            {
//                let arr_PickerArray = pickerArray as! [[String:Any]]
//                return arr_PickerArray[row]["breedname"] as? String
//            }
//            else if str_Pickerkey == "PET_LIVESWITH"
//            {
//                //            print("pickerArray is:-",pickerArray)
//
//                let arr_PickerArray = pickerArray as! [[String:Any]]
//                return arr_PickerArray[row]["petliveswith"] as? String
//            }
//            else if str_Pickerkey == "Gender" || str_Pickerkey == "Species"
//            {
//                let arr_PickerArray = pickerArray as! [String]
//                return arr_PickerArray[row]
//            }
//
//            else
//            {
//                return ""
//            }
//        }
// }

//    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
//    {
//        //            print("pickerView is:-",pickerView)
//
//        //        MARK: Profile Picker
//        if str_Pickerkey == "NATIONAL_ID_TYPE"
//        {
//            let arr_PickerArray = pickerArray as! [[String:Any]]
//            //            print("arr_PickerArray is:-",arr_PickerArray)
//
//            str_SelectedCountryCode = (arr_PickerArray[row]["idtype"] as? String)!
//            //            print("str_SelectedCountryCode is:-",str_SelectedCountryCode )
//
//            str_SelectedCountryId = (arr_PickerArray[row]["id"] as? String)!
//            //            print("str_SelectedCountryId is:-",str_SelectedCountryId)
//        }
//        else if str_Pickerkey == "OWNER_CODE"
//        {
//            let arr_PickerArray = pickerArray as! [[String:Any]]
//            //            print("arr_PickerArray is:-",arr_PickerArray)
//
//            str_SelectedCountryCode = (arr_PickerArray[row]["ownercode"] as? String)!
//            //            print("str_SelectedCountryCode is:-",str_SelectedCountryCode )
//
//            str_SelectedCountryId = (arr_PickerArray[row]["id"] as? String)!
//            //            print("str_SelectedCountryId is:-",str_SelectedCountryId )
//        }
//        else if str_Pickerkey == "OWNER_LOCATION"
//        {
//            let arr_PickerArray = pickerArray as! [[String:Any]]
//            //            print("arr_PickerArray is:-",arr_PickerArray)
//
//            str_SelectedCountryCode = (arr_PickerArray[row]["locationame"] as? String)!
//            //            print("str_SelectedCountryCode is:-",str_SelectedCountryCode )
//
//            str_SelectedCountryId = (arr_PickerArray[row]["id"] as? String)!
//            //            print("str_SelectedCountryId is:-",str_SelectedCountryId )
//        }
//        else if str_Pickerkey == "OWNER_TYPE"
//        {
//            let arr_PickerArray = pickerArray as! [[String:Any]]
//            //            print("arr_PickerArray is:-",arr_PickerArray)
//
//            str_SelectedCountryCode = (arr_PickerArray[row]["ownertype"] as? String)!
//            //            print("str_SelectedCountryCode is:-",str_SelectedCountryCode )
//
//            str_SelectedCountryId = (arr_PickerArray[row]["id"] as? String)!
//        }
//
//
//
//
////        MARK: Add Pet Picker
//        else if str_Pickerkey == "PET_BREEDS"
//        {
//            let arr_PickerArray = pickerArray as! [[String:Any]]
//
//            str_SelectedCountryCode = (arr_PickerArray[row]["breedname"] as? String)!
//
//            str_SelectedCountryId = (arr_PickerArray[row]["id"] as? String)!
//        }
//        else if str_Pickerkey == "PET_LIVESWITH"
//        {
//            let arr_PickerArray = pickerArray as! [[String:Any]]
//
//            str_SelectedCountryCode = (arr_PickerArray[row]["petliveswith"] as? String)!
//
//            str_SelectedCountryId = (arr_PickerArray[row]["id"] as? String)!
//        }
//        else if str_Pickerkey == "Gender"
//        {
//            let arr_PickerArray = pickerArray as! [String]
//
//            str_SelectedCountryCode = arr_PickerArray[row]
//
//            str_SelectedCountryId = ""
//        }
//        else if str_Pickerkey == "Species"
//        {
//            let arr_PickerArray = pickerArray as! [String]
//
//            str_SelectedCountryCode = arr_PickerArray[row]
//
//            str_SelectedCountryId = ""
//        }
//
//
//            /// list of picker
//        else if str_Pickerkey == "pets"
//        {
//            let arr_PickerArray = pickerArray as! [String]
//
//
//            str_SelectedCountryCode = arr_PickerArray[row]
//
//            str_SelectedCountryId = ""
//        }
//
//
//
//        else
//        {
//
//        }
//
//        self.delegate?.pickervalue(picker, selectedValue: self.str_SelectedCountryCode, selected_Id: str_SelectedCountryId, pickerKey: str_Pickerkey as String)
//    }

//}
