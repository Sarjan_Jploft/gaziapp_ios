//
//  Static.swift
//  Infinite Cabs
//
//  Created by Apple on 17/11/19.
//  Copyright © 2019 micro. All rights reserved.
//

import Foundation
import UIKit


class staticClass {
    
    static let shared = staticClass()
    
    let themeColor = UIColor(named: "ThemeColor")?.cgColor ?? UIColor.clear.cgColor
    
    
    
    class func getStoryboard_Main() -> UIStoryboard? {
        let str = "Main"
        //print("story board is:-\(UIStoryboard(name: str, bundle: nil))")
        return UIStoryboard(name: str, bundle: nil)
    }
    
    class func getStoryboard_Home() -> UIStoryboard? {
        let str = "Home"
        //print("story board is:-\(UIStoryboard(name: str, bundle: nil))")
        return UIStoryboard(name: str, bundle: nil)
    }
    
    class func getStoryboard_Order() -> UIStoryboard? {
        let str = "Order"
        //print("story board is:-\(UIStoryboard(name: str, bundle: nil))")
        return UIStoryboard(name: str, bundle: nil)
    }
    
    class func getStoryboard_Payment() -> UIStoryboard? {
        let str = "Payment"
        //print("story board is:-\(UIStoryboard(name: str, bundle: nil))")
        return UIStoryboard(name: str, bundle: nil)
    }
    
    class func getStoryboard_Chat() -> UIStoryboard? {
        let str = "Chat"
        //print("story board is:-\(UIStoryboard(name: str, bundle: nil))")
        return UIStoryboard(name: str, bundle: nil)
    }
    
    class func getStoryboard_Profile() -> UIStoryboard? {
        let str = "Profile"
        //print("story board is:-\(UIStoryboard(name: str, bundle: nil))")
        return UIStoryboard(name: str, bundle: nil)
    }
   
    class func getStoryboard_SideMenu() -> UIStoryboard? {
        let str = "SideMenu"
        //print("story board is:-\(UIStoryboard(name: str, bundle: nil))")
        return UIStoryboard(name: str, bundle: nil)
    }
   
}
