//
//  global.swift
//  SocialHub
//
//  Created by Mac on 12/5/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import Foundation
import SwiftyJSON

class global : NSObject
{
    static let shared = global()
    
    
     func saveJSON(json: JSON, key:String){
        
       if let jsonString = json.rawString() {
          UserDefaults.standard.setValue(jsonString, forKey: key)
        let userId = json[ApiKey.userId].stringValue
        UserDefaults.standard.set(userId, forKey: ApiKey.userId)
        let fname = json["fname"].stringValue
        let lastname = json["lname"].stringValue
        let name = fname + lastname
        UserDefaults.standard.set(name, forKey: "fullname")
       }
        
    }

        func getJSON(_ key: String)-> JSON? {
        var p = ""
        if let result = UserDefaults.standard.string(forKey: key) {
            p = result
        }
        if p != "" {
            if let json = p.data(using: String.Encoding.utf8, allowLossyConversion: false) {
                do {
                    return try JSON(data: json)
                } catch {
                    return nil
                }
            } else {
                return nil
            }
        } else {
            return nil
        }
    }

    
    /*
    func saveDataInUserDefult(dict:NSDictionary, key: String)
    {
        let encodedData = NSKeyedArchiver.archivedData(withRootObject: dict)
        UserDefaults.standard.set(encodedData, forKey: key)
//        UserDefaults.standard.set(dict, forKey: key)
        let userId = "\(dict[ApiKey.userId]!)"
        UserDefaults.standard.set(userId, forKey: ApiKey.userId)
        let fname = dict["fname"] as? String ?? ""
        let lastname = dict["lname"] as? String ?? ""
        let name = fname + lastname
        UserDefaults.standard.set(name, forKey: "fullname")
        UserDefaults.standard.synchronize()
    }*/
    
    func saveStringInUserDefult(data_str:  String, key: String)
    {
        UserDefaults.standard.set(data_str, forKey: key)
//        let userId = "\(dict[ApiKey.userId]!)"
//        UserDefaults.standard.set(userId, forKey: ApiKey.userId)
        UserDefaults.standard.synchronize()
    }
    
    /*
    func getUserDataInInUserDefult(key: String) -> NSDictionary
    {
        if(UserDefaults.standard.object(forKey: key) != nil)
        {
            guard let data = UserDefaults.standard.object(forKey: key) as? Data else { return NSDictionary() }
            let dict =  NSKeyedUnarchiver.unarchiveObject(with: data) as! NSDictionary
            
            //let dict = UserDefaults.standard.object(forKey: key) as! NSDictionary
            return dict
        }
        
//        if(UserDefaults.standard.object(forKey: key) != nil)
//        {
//            let dict = UserDefaults.standard.object(forKey: key) as! NSDictionary
//            return dict
//        }
        return NSDictionary()
        
    }*/
    
    func getUserDataValueByKey(strKey: String) -> String
    {
        if(UserDefaults.standard.object(forKey: strKey) != nil)
        {
            let value =
                "\(UserDefaults.standard.object(forKey: strKey)!)"
            return value
        }
        return ""
    }
    
    func removeDataFromUserDefult()
    {
        UserDefaults.standard.removeObject(forKey: Constants.UserDefult.SaveUserData.rawValue)
        UserDefaults.standard.removeObject(forKey: ApiKey.userId)
        UserDefaults.standard.removeObject(forKey: "fullname")
        
      //  UserDefaults.standard.synchronize()
    }
    
    
}
